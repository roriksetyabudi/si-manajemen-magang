<style>
    table.display thead th{
        font-size: 14px;text-align: center;font-weight: bold;
    }
    table.display {
        font-size:15px;
        border:black thin thin;
    }
</style>
<table class="table_data">
    <tr>
        <td align="centar" width="15%" class="header_logo" rowspan="4"><img src="{{public_path('template')}}/dist/img/pos-indonesia.jpg" width="75px"/></td>
        <td align="center" width="70%" class="header_title" style="letter-spacing: 3px;"><H2>PT Pos Indonesia (Persero) Madiun</H2></td>
        <td align="centar" width="15%" class="header_logo" rowspan="4"><H4></H4></td>
    </tr>
    <tr><td align="center" width="70%" class="header_title" colspan="3" style="letter-spacing: 2px;"><H3><b>Jalan Pahlawan Nomor 24 Madiun Kode Pos 63100</b></H3></td></tr>
    <tr><td align="center" width="70%" class="header_title" colspan="3" style="letter-spacing: 1px;"><H4>Website : <i>www.posindonesia.co.id</i>, Telepon : <i>0351-464454</i></H4></td></tr>

    <tr><td align="center" width="70%" class="header_title" colspan="3"></td></tr>
</table>
{{-- <h1 align="center">LAPORAN DATA AKTIFITAS PESERTA MAGANG<br>PERIODE GENERATE {{ date('d F Y', strtotime($tanggal_mulai)) }} s/d {{ date('d F Y', strtotime($tanggal_selesai)) }}</h1> --}}
<table width="100%" border="1" cellpadding="3" cellspacing="0">
    <thead>
    <tr style="background-color:#FF0000;color:#ffffff;">
        <td><b>NAMA</b></td>
        <td><b>KEGIATAN</b></td>
        <td><b>MULAI</b></td>
        <td><b>SELESAI</b></td>
        <td><b>WAKTU</b></td>
        <td><b>VOLUME</b></td>
    </tr>
    </thead>

    @foreach($detailAktifitasMagang as $value)
        <tr>
            <td> {{ $value->user->nama_lengkap }} </td>
            <td> {{ $value->nama_kegiatan }} </td>
            <td>{{ date('d F Y', strtotime($value->tanggal_mulai)) }}</td>
            <td>{{ date('d F Y', strtotime($value->tanggal_selesai)) }}</td>
            <td>{{ $value->interval_aktifitas }}</td>
            <td>{{ $value->volume_kegiatan }}</td>
            <td>{{ date('d F Y H:i:s', strtotime($value->updated_at_absens)) }}</td>
        </tr>
    @endforeach
</table>
