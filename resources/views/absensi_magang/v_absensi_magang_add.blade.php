@extends('template.v_template')
@section('title', 'Data Absensi Magang')
@push('head-css')
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/adminlte.min.css">
@endpush
@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Tambah Absensi</h3>
        </div>
        @if(session()->has('fail'))
            <div class="alert alert-danger" role="alert">
                {{ session('fail') }}
            </div>
        @endif

        <form method="post" action="/absensi-magang/post" enctype="multipart/form-data">
            @csrf
            <input type="text" class="form-control" id="id_user" name="id_user" placeholder="" value="{{ $id_users }}" readonly hidden>
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Peserta Magang</label>
                    <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" id="nama_lengkap" name="nama_lengkap" placeholder="" value="{{ $nama_lengkap }}" readonly>
                    @error('nama_lengkap')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tanggal dan Jam Absensi</label>
                    <input type="text" class="form-control @error('tanggal') is-invalid @enderror" id="tanggal" name="tanggal" placeholder="" value="{{ date('d F y H:i:s', strtotime(date('Y-m-d H:i:s'))) }}" readonly>
                    @error('tanggal')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Status</label>
                    <select class="form-control select2 status" name="status" id="status" style="width: 100%;">
                        <option value="Masuk" selected="selected">Masuk</option>
                        <option value="Pulang">Pulang</option>
                    </select>
                </div>
                <div class="form-group layout_bukti_absensi_masuk">
                    <label for="exampleInputFile">Bukti Absensi Masuk</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input file_bukti_absensi @error('file_bukti_absensi') is-invalid @enderror" id="file_bukti_absensi" name="file_bukti_absensi">
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Upload</span>
                        </div>

                    </div>
                    @error('file_bukti_absensi')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>


                <div class="form-group layout_bukti_absensi_pulang">
                    <label for="exampleInputFile">Bukti Absensi Pulang</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input file_bukti_absensi_pulang @error('file_bukti_absensi_pulang') is-invalid @enderror" id="file_bukti_absensi_pulang" name="file_bukti_absensi_pulang">
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Upload</span>
                        </div>

                    </div>
                    @error('file_bukti_absensi_pulang')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Proses</button>
            </div>
        </form>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Data Absensi Hari ini</h3>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Nama Peserta</th>
                        <th>Masuk</th>
                        <th>Pulang</th>
                        <th>Keterangan</th>
                        <th>Status</th>
                        <th>Updated</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($absens as $absen)
                    <tr>
                        <td>{{ $absen->user->nama_lengkap }}</td>
                        <td>
                            {{ date('d F Y, H:i:s', strtotime($absen->tanggal_masuk)) }}
                            @if(date('H:i:s', strtotime($absen->tanggal_masuk)) > '06:00:00' && date('H:i:s', strtotime($absen->tanggal_masuk)) < '08:05:00')
                                <span class="badge badge-success">Tepat</span>
                            @elseif(date('H:i:s', strtotime($absen->tanggal_masuk)) > '08:05:00' && date('H:i:s', strtotime($absen->tanggal_masuk)) < '09:05:00')
                                <span class="badge badge-warning">Terlambat</span>
                            @else
                                <span class="badge badge-danger">Tidak Absen</span>
                            @endif
                        </td>
                        <td>
                            @if($absen->tanggal_pulang == "" || $absen->tanggal_pulang == null)
                            @else
                                {{ date('d F Y, H:i:s', strtotime($absen->tanggal_pulang)) }}
                            @endif

                            @if(date('H:i:s', strtotime($absen->tanggal_pulang)) >= '15:00:00')
                                <span class="badge badge-success">Tepat</span>
                            @elseif(date('H:i:s', strtotime($absen->tanggal_pulang)) > '14:00:00' && date('H:i:s', strtotime($absen->tanggal_pulang)) <= '15:00:00')
                                <span class="badge badge-warning">Pulang Awal</span>
                            @else
                                <span class="badge badge-danger">Tidak Absen</span>
                            @endif
                        </td>
                        <td>
                            @if($absen->tanggal_masuk != ""  && $absen->tanggal_pulang != "")
                                <span class="badge badge-success">Hadir</span>
                            @else
                                <span class="badge badge-danger">Tidak Hadir</span>
                            @endif
                        </td>
                        <td>{{ $absen->status }}</td>
                        <td>{{ date('d F Y, H:i:s', strtotime($absen->updated_at)) }}</td>
                    </tr>
                    @endforeach



                    </tbody>
                </table>
            </div>
        </div>
    </div>




@endsection
@push('bottom-js')
    <script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- bs-custom-file-input -->
    <script src="{{asset('template')}}/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script type="text/javascript">
        $(document).ready(function() {
            $(".layout_bukti_absensi_pulang").hide();
            $('#status').change(function () {
                if($(this).val() == "Masuk") {
                    $(".layout_bukti_absensi_masuk").show();
                    $(".layout_bukti_absensi_pulang").hide();
                } else if($(this).val() == "Pulang") {
                    $(".layout_bukti_absensi_masuk").hide();
                    $(".layout_bukti_absensi_pulang").show();
                }
            });
        });

    </script>
@endpush

