<?php

namespace App\Http\Controllers;
use PDF;
use App\Models\NilaiTrans;
use Illuminate\Http\Request;

class NilaiMagangCetak extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $detailNilaiCount = NilaiTrans::where('id_users', $id)->get()->count();
        if($detailNilaiCount > 0) {
//            $detailNilai = NilaiTrans::leftJoin('magangs', 'magangs.id_users_magang', '=', 'nilai_trans.id_users')
//                ->leftJoin('users', 'nilai_trans.id_users', '=', 'users.id')
//                ->leftJoin('instansis', 'magangs.id_instansi', '=', 'instansis.id')
//                ->leftJoin('magangs as b', 'magangs.id_pembimbing_sekolah', '=', 'users.id')
//                ->select(['nilai_trans.id as id_nilai_trans','nilai_trans.id_users','nilai_trans.kedisiplinan','nilai_trans.sikap','nilai_trans.komunikasi',
//                    'nilai_trans.kerapian','nilai_trans.kerjasama','nilai_trans.motivasi','nilai_trans.penguasaan_terhadap_pekerjaan','nilai_trans.hasil_pekerjaan',
//                    'nilai_trans.ide_atau_gagasan','nilai_trans.deleted','nilai_trans.created_at','users.nama_lengkap',
//                    'nilai_trans.tanggung_jawab','nilai_trans.kejujuran','users.gender','instansis.nama_instansi','instansis.alamat_instansi',
//                    'instansis.telepon_instansi','b.id_pembimbing_sekolah as ids'])
//                ->whereNotNull('magangs.id_users_magang')
//                ->where('nilai_trans.id_users', $id)->get()->first();

            $detailNilai = NilaiTrans::where('id_users', $id)->get()->first();

            $view = view('laporan.nilai_magang.v_nilai_magang_cetak', [
                "detailNilai" => $detailNilai,
            ]);
            $html_content = $view->render();
            PDF::SetAuthor('Rorik Setya Budi');
            PDF::SetTitle('Laporan Data Sistem Informasi Manajemen Magang');
            PDF::SetSubject("Laporan Nilai Magang");
            PDF::SetKeywords('sistem informasi manajemen magang');
            PDF::setPrintHeader(false);
            PDF::setPrintFooter(false);
            PDF::SetMargins(7, 7, 7, 7);
            PDF::SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

            PDF::AddPage('P', 'F4');
            PDF::SetFont('helvetica', null, 11);
            PDF::writeHTML($html_content, true, false, true, false, '');
            PDF::Output('data_nilai.pdf');
        } else {
            echo "Data Tidak Tersedia";
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
