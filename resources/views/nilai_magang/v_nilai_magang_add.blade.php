@extends('template.v_template')
@section('title', 'Nilai Magang')
@push('head-css')
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template/')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template/')}}/dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('template/')}}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
@endpush
@section('content')

    <style>
        .scrollme {
            overflow-x: auto;
        }
    </style>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Tambah Nilai Magang</h3>

        </div>
        <div class="card-body">
            <form class="form-horizontal">

                <div class="col-sm-8">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">FILTER PEMBERIAN NILAI</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="inputPassword3" class="col-sm-3 col-form-label">Instansi</label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2" style="width: 100%;">
                                                    <option selected="selected">Semua</option>
                                                    <option>Rahardian</option>
                                                    <option>Dion Purnama</option>
                                                    <option>Fitri Nur Wijayanti</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="inputPassword3" class="col-sm-3 col-form-label">Bidang Keahlian</label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2" style="width: 100%;">
                                                    <option selected="selected">Semua</option>
                                                    <option>Teknik Komputer dan Jaringan</option>
                                                    <option>Akuntansi</option>
                                                    <option>Perkantoran</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="inputPassword3" class="col-sm-3 col-form-label">Pembimbing Magang</label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2" style="width: 100%;">
                                                    <option selected="selected">Semua</option>
                                                    <option>SMK Negeri 1 Madiun</option>
                                                    <option>SMK Negeri 2 Madiun</option>
                                                    <option>SMK Negeri 3 Madiun</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

            </form>

            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr class="bg-info">
                        <td rowspan="2" class="align-middle  text-center">NO</td>
                        <td rowspan="2" class="align-middle text-center">NAMA PESERTA MAGANG</td>
                        <td colspan="8" class="align-middle text-center">ASPEK PERILAKU</td>
                        <td colspan="4" class="align-middle text-center">ASPEK PENUGASAN TUGAS</td>
                        <td colspan="4" class="align-middle text-center">ASPEK TEKNIS</td>
                        <td rowspan="2" class="align-middle text-center">AKSI</td>
                    </tr>
                    <tr class="bg-info">
                        <td class="align-middle text-center">Kesetiaan</td>
                        <td class="align-middle text-center">Ketaatan</td>
                        <td class="align-middle text-center">Kepemimpinan</td>
                        <td class="align-middle text-center">Kerjasama</td>
                        <td class="align-middle text-center">Komunikasi</td>
                        <td class="align-middle text-center">Kemauan Belajar</td>
                        <td class="align-middle text-center">Tanggung Jawab</td>
                        <td class="align-middle text-center">Hubungan Sosial</td>
                        <td class="align-middle text-center">Pemahaman Tugas</td>
                        <td class="align-middle text-center">Pelaksanaan Tugas</td>
                        <td class="align-middle text-center">Kualitas Kerja</td>
                        <td class="align-middle text-center">Volume Hasil Kerja</td>
                        <td class="align-middle text-center">Ketelitian Kerja</td>
                        <td class="align-middle text-center">Tempo Kerja</td>
                        <td class="align-middle text-center">Keparan</td>
                        <td class="align-middle text-center">Ketekunan Kerja</td>
                    </tr>
                    <tr class="table-striped">
                        <td class="align-middle text-center">1</td>
                        <td>Rinang Sibandoro</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center">80</td>
                        <td class="align-middle text-center"><button type="button" class="btn btn-warning btn-sm" >Edit</button></td></td>
                    </tr>
                    <tr class="table-striped">
                        <td class="align-middle text-center">2</td>
                        <td>Danang Wicaksono</td>
                        <td class="align-middle text-center"><input type="text" class="form-control" placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value="80"></td>
                        <td class="align-middle text-center"><button type="button" class="btn btn-warning btn-sm" >Edit</button></td></td>
                    </tr>
                    <tr class="table-striped">
                        <td class="align-middle text-center">2</td>
                        <td>Andi Darma Sasongko</td>
                        <td class="align-middle text-center"><input type="text" class="form-control" placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><input type="text" class="form-control"  placeholder="" value=""></td>
                        <td class="align-middle text-center"><button type="button" class="btn btn-success btn-sm" >Save</button></td></td>
                    </tr>

                </table>
            </div>


        </div>
        <!-- /.card-body -->

        <!-- /.card-footer-->
    </div>
@endsection
@push('bottom-js')
    <!-- jQuery -->
    <script src="{{asset('template/')}}/plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('template/')}}/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template/')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="{{asset('template/')}}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template/')}}/dist/js/adminlte.js"></script>
@endpush
