@extends('template.v_template')
@section('title', 'Data Absensi Magang')
@push('head-css')
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/adminlte.min.css">

    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/summernote/summernote-bs4.min.css">
    <!-- CodeMirror -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/codemirror/codemirror.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/codemirror/theme/monokai.css">
    <!-- SimpleMDE -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/simplemde/simplemde.min.css">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

@endpush
@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Tambah Pemberian Tugas Baru</h3>
        </div>
        <form method="post" action="/pemberian-tugas/post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Peserta Magang Yang Akan Diberi Tugas</label>
                    <select class="form-control @error('id_users_diberi_tugas') is-invalid @enderror select2" name="id_users_diberi_tugas" id="id_users_diberi_tugas" style="width: 100%;">
                        <option value="">Pilih</option>
                        @foreach($magangs as $valueMagangs)
                            <option value="{{ $valueMagangs->id_users_magang }}">{{ $valueMagangs->userMagang->nama_lengkap }}</option>
                        @endforeach
                    </select>
                    @error('id_users_diberi_tugas')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Tugas</label>
                    <input type="text" class="form-control @error('nama_tugas') is-invalid @enderror" id="nama_tugas" name="nama_tugas" placeholder="Nama Tugas" value="">
                    @error('nama_tugas')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>



                <div class="form-group">
                    <label for="exampleInputEmail1">Keterangan Tugas Yang Diberikan</label>
                    <textarea id="keterangan_summernote" name="keterangan" class="@error('keterangan') is-invalid @enderror">

                    </textarea>
                        @error('keterangan')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">File Lampiran Tugas</label>
                    <input type="file" class="form-control @error('id_file_tugas') is-invalid @enderror" id="id_file_tugas" name="id_file_tugas" placeholder="Pemberian Tugas" value="">
                    @error('id_file_tugas')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Target Yang Diharapkan</label>
                    <input type="number" class="form-control @error('target') is-invalid @enderror" id="target" name="target" placeholder="Target Yang Diharapkan" value="">
                    @error('target')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Proses</button>
            </div>
        </form>
    </div>





@endsection
@push('bottom-js')
    <script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

    <!-- bs-custom-file-input -->
    <script src="{{asset('template')}}/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

    <!-- AdminLTE App -->
    <script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>

    <!-- Summernote -->
    <script src="{{asset('template')}}/plugins/summernote/summernote-bs4.min.js"></script>
    <!-- CodeMirror -->
    <script src="{{asset('template')}}/plugins/codemirror/codemirror.js"></script>
    <script src="{{asset('template')}}/plugins/codemirror/mode/css/css.js"></script>
    <script src="{{asset('template')}}/plugins/codemirror/mode/xml/xml.js"></script>
    <script src="{{asset('template')}}/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>


    <script>
        $(function () {

            // Summernote
            $('#keterangan_summernote').summernote()

            // CodeMirror
            CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
                mode: "htmlmixed",
                theme: "monokai"
            });


        })
    </script>
@endpush

