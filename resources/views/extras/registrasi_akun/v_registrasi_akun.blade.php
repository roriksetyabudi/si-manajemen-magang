<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>anajemen Magang | Registrasi Akun</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/adminlte.min.css">
</head>
<body class="hold-transition register-page">
<div class="register-box">
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a href="/home" class="h2"><b>AKTIVASI</b>AKUN</a>
        </div>
        <div class="card-body">
            @if(session()->has('fail'))
                <div class="alert alert-danger" role="alert">
                    {{ session('fail') }}
                </div>
            @endif
            @if(session()->has('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            <p class="login-box-msg">Aktivasi Akun Mu</p>

            <form action="/registrasi-akun/post" method="post">
                @csrf
                <div class="input-group mb-3">
                    <input type="text" name="kode_verifikasi" id="kode_verifikasi" class="form-control @error('kode_verifikasi') is-invalid @enderror" placeholder="Kode Verifikasi Dari Email">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-code"></span>
                        </div>
                    </div>
                    @error('kode_verifikasi')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
{{--                <div class="input-group mb-3">--}}
{{--                    <input type="text" name="username" id="username" class="form-control @error('username') is-invalid @enderror" placeholder="Username">--}}
{{--                    <div class="input-group-append">--}}
{{--                        <div class="input-group-text">--}}
{{--                            <span class="fas fa-user"></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    @error('username')--}}
{{--                    <div class="invalid-feedback">--}}
{{--                        {{ $message }}--}}
{{--                    </div>--}}
{{--                    @enderror--}}
{{--                </div>--}}
{{--                <div class="input-group mb-3">--}}
{{--                    <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">--}}
{{--                    <div class="input-group-append">--}}
{{--                        <div class="input-group-text">--}}
{{--                            <span class="fas fa-lock"></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    @error('kode_verifikasi')--}}
{{--                    <div class="invalid-feedback">--}}
{{--                        {{ $message }}--}}
{{--                    </div>--}}
{{--                    @enderror--}}
{{--                </div>--}}
{{--                <div class="input-group mb-3">--}}
{{--                    <input type="password" name="ulangi_password" id="ulangi_password" class="form-control @error('ulangi_password') is-invalid @enderror" placeholder="Ulangi Password">--}}
{{--                    <div class="input-group-append">--}}
{{--                        <div class="input-group-text">--}}
{{--                            <span class="fas fa-lock"></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    @error('ulangi_password')--}}
{{--                    <div class="invalid-feedback">--}}
{{--                        {{ $message }}--}}
{{--                    </div>--}}
{{--                    @enderror--}}
{{--                </div>--}}
                <div class="input-group mb-3">
                    <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                    @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
{{--                <div class="input-group mb-3">--}}
{{--                    <input type="telepon" name="telepon" id="telepon" class="form-control @error('telepon') is-invalid @enderror" placeholder="Telepon">--}}
{{--                    <div class="input-group-append">--}}
{{--                        <div class="input-group-text">--}}
{{--                            <span class="fas fa-phone"></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    @error('telepon')--}}
{{--                    <div class="invalid-feedback">--}}
{{--                        {{ $message }}--}}
{{--                    </div>--}}
{{--                    @enderror--}}
{{--                </div>--}}
                <div class="row">
                    <div class="col-8">

                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Aktivasi</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <p class="mb-0">
                <a href="/login" class="text-center">Masuk Login</a>
            </p>
            <p class="mb-0">
                <a href="/registrasi-magang/create" class="text-center">Registrasi Magang</a>
            </p>
        </div>
        <!-- /.form-box -->
    </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>
</body>
</html>
