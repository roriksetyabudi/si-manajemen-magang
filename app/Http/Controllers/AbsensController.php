<?php

namespace App\Http\Controllers;

use App\Models\FileUploads;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Absens;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;

class AbsensController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {


            $dataAbsens = Absens::leftJoin('users','absens.id_users', '=', 'users.id')
                ->leftJoin('magangs', 'magangs.id_users_magang', '=', 'absens.id_users')
                ->leftJoin('instansis', 'instansis.id', '=', 'magangs.id_instansi')
                ->select(['absens.id as id_absens','absens.id_users as id_users_absens','absens.tanggal_masuk','absens.tanggal_pulang',
                    'absens.status','absens.file_bukti_absensi','absens.file_bukti_absensi_pulang',
                    'absens.approval','absens.deleted','users.nama_lengkap'])
                ->whereNotNull('magangs.id_users_magang')
                ->whereDate('absens.created_at', '>=', date('Y-m-d', strtotime($request->get('tanggal_mulai'))))
                ->whereDate('absens.created_at', '<=', date('Y-m-d', strtotime($request->get('tanggal_selesai'))))
                ->where('absens.deleted',0)
                ->where(function ($query) use ($request){
                    $keterangan = $request->get('keterangan');
                    if($keterangan == "Hadir") {
                        $query->whereNotNull('tanggal_masuk')->whereNotNull('tanggal_pulang');
                    } else if($keterangan == "Tidak Hadir") {
                        $query->whereNull('tanggal_masuk')->orWhereNull('tanggal_pulang');
                    }
                })
                ->where(function ($qurty){
                    if(auth()->user()->privileges == "USERS MAGANG") {
                        $qurty->where('absens.id_users', auth()->user()->id);
                    } else if(auth()->user()->privileges == "PEMBIMBING SEKOLAH") {
                        $qurty->where('magangs.id_pembimbing_sekolah', auth()->user()->id);
                    } else if(auth()->user()->privileges == "PEMBIMBING MAGANG") {
                        $qurty->where('magangs.id_users_pembimbing_magang', auth()->user()->id);
                    }
                })->get();


            //$dataAbsens = Absens::with('user')->select('absens.*');
            return DataTables::of($dataAbsens)
                ->addIndexColumn('absens')
                ->filter(function ($instance) use ($request) {

                    if(!empty($request->get('status'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains(Str::lower($row['status']), Str::lower($request->get('status'))) ? true : false;
                        });
                    }

                    if(!empty($request->get('user'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains(Str::lower($row['nama_lengkap']), Str::lower($request->get('user'))) ? true : false;
                        });
                    }

                    if(!empty($request->get('search'))){
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            if (Str::contains(Str::lower($row['nama_lengkap']), Str::lower($request->get('search')))){
                                return true;
                            }

                            return false;
                        });
                    }

                })

                ->addColumn('user', function (Absens $absens){
                    return $absens->nama_lengkap;
                })
                ->addColumn('tanggal_masuk', function (Absens $absens) {
                    $label_tanggal_masuk = '';
                    $tanggal_masuk = '';
                    if($absens->tanggal_masuk == "" || $absens->tanggal_masuk == null) {
                        $tanggal_masuk = '';
                    } else {
                        $tanggal_masuk = date('d F y H:i:s', strtotime($absens->tanggal_masuk));
                    }
                    if(date('H:i:s', strtotime($absens->tanggal_masuk)) > '06:00:00' && date('H:i:s', strtotime($absens->tanggal_masuk)) < '08:05:00') {
                        $label_tanggal_masuk .= $tanggal_masuk. ' <span class="badge badge-success">Tepat</span>';
                    } else if(date('H:i:s', strtotime($absens->tanggal_masuk)) > '08:05:00' && date('H:i:s', strtotime($absens->tanggal_masuk)) < '09:05:00') {
                        $label_tanggal_masuk .= $tanggal_masuk. ' <span class="badge badge-warning">Terlambat</span>';
                    } else {
                        $label_tanggal_masuk .= $tanggal_masuk. ' <span class="badge badge-danger">Tidak Absen</span>';
                    }
                    return $label_tanggal_masuk;
                })
                ->addColumn('tanggal_pulang', function (Absens $absens){
                    $label_tanggal_pulang = '';
                    $tanggal_pulang = '';
                    if($absens->tanggal_pulang == "" || $absens->tanggal_pulang == null) {
                        $tanggal_pulang = '';
                    } else {
                        $tanggal_pulang = date('d F y H:i:s', strtotime($absens->tanggal_pulang));
                    }

                    if(date('H:i:s', strtotime($absens->tanggal_pulang)) >= '15:00:00') {
                        $label_tanggal_pulang .= $tanggal_pulang. ' <span class="badge badge-success">Tepat</span>';
                    } else if(date('H:i:s', strtotime($absens->tanggal_pulang)) > '14:00:00' && date('H:i:s', strtotime($absens->tanggal_pulang)) <= '15:00:00') {
                        $label_tanggal_pulang .= $tanggal_pulang. ' <span class="badge badge-warning">Pulang Awal</span>';
                    } else {
                        $label_tanggal_pulang .= $tanggal_pulang. ' <span class="badge badge-danger">Tidak Absen</span>';
                    }
                    return $label_tanggal_pulang;
                })
                ->addColumn('status', function (Absens $absens) {
                    return $absens->status;
                })
                ->addColumn('keterangan', function (Absens $absens){
                    $status = '';
                    if($absens->tanggal_masuk != ""  && $absens->tanggal_pulang != "") {
                        $status = ' <span class="badge badge-success">Hadir</span>';
                    } else {
                        $status = ' <span class="badge badge-danger">Tidak Hadir</span>';
                    }
                    return $status;
                })
                ->addColumn('approval', function (Absens $absens) {
                    if(auth()->user()->privileges == "ADMIN" || auth()->user()->privileges == "PEMBIMBING MAGANG") {
                        if($absens->approval == "N") {
                            $approval = '<a href="/absensi-magang/set-approval/'.$absens->id_absens.'"><span class="badge badge-danger">Not Approval</span></a> ';
                        } else if($absens->approval == "Y") {
                            $approval = '<a href="/absensi-magang/set-approval/'.$absens->id_absens.'"><span class="badge badge-success">Approval</span></a> ';
                        }
                    } else {
                        if($absens->approval == "N") {
                            $approval = '<span class="badge badge-danger">Not Approval</span>';
                        } else if($absens->approval == "Y") {
                            $approval = '<span class="badge badge-success">Approval</span> ';
                        }
                    }

                    return $approval;
                })
                ->addColumn('action', function (Absens $absens) {
                    if($absens->file_bukti_absensi != 0 && $absens->file_bukti_absensi_pulang == 0) {
                        $btn = '<a href="./storage/'.$absens->fileuploads->path.'" target="_blank" class="btn btn-info" path="'.$absens->fileuploads->path.'">Masuk</a>';
                    } else if($absens->file_bukti_absensi_pulang != 0 && $absens->file_bukti_absensi == 0) {
                        $btn = '<a href="./storage/'.$absens->fileuploadsPulang->path.'" target="_blank" class="btn btn-warning" path="'.$absens->fileuploadsPulang->path.'"> Pulang</a>';
                    } else if($absens->file_bukti_absensi != 0 && $absens->file_bukti_absensi_pulang != 0) {
                        $btn = '<a href="./storage/'.$absens->fileuploads->path.'" target="_blank" class="btn btn-info" path="'.$absens->fileuploads->path.'">Masuk</a>
                                <a href="./storage/'.$absens->fileuploadsPulang->path.'" target="_blank" class="btn btn-warning" path="'.$absens->fileuploadsPulang->path.'"> Pulang</a>';
                    } else {
                        $btn = '';
                    }
                    return $btn;
                })
                ->rawColumns(['action','tanggal_masuk','tanggal_pulang','keterangan','approval'])
                ->make(true);
        }
        return view('absensi_magang.v_absensi_magang', [
            "id_users" => auth()->user()->id,
            "privileges" => auth()->user()->privileges
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('absensi_magang.v_absensi_magang_add',[
            "id_users" => auth()->user()->id,
            "nama_lengkap" => auth()->user()->nama_lengkap,
            "privileges" => auth()->user()->privileges,
            "absens" => Absens::where('id_users', auth()->user()->id)->whereDate('created_at', date('Y-m-d'))->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_user' => 'required',
            'tanggal' => 'required',
            'nama_lengkap' => 'required',
            'status' => 'required',
        ]);

        $resultCountCheckTanggalMasuk = Absens::whereNotNull('tanggal_masuk')->whereDate('tanggal_masuk', date("Y-m-d", strtotime($request->tanggal)))->where('id_users', $request->id_user)->where('deleted', 0)->get()->count();
        $resultCountCheckTanggalPulang = Absens::whereNotNull('tanggal_pulang')->whereDate('tanggal_pulang', date("Y-m-d", strtotime($request->tanggal)))->where('id_users', $request->id_user)->where('deleted', 0)->get()->count();
        $resultFirstCheckTanggalPulang = Absens::whereNotNull('tanggal_masuk')->whereDate('tanggal_masuk', date("Y-m-d", strtotime($request->tanggal)))->where('id_users', $request->id_user)->where('deleted', 0)->orderBy('id', "DESC")->get()->first();


        //FILE UPLOAD IMAGES PHOTO USER
        $insert['absens']['id_users'] = $request->id_user;
        if($request->status == "Masuk") {
            $request->validate([
                'file_bukti_absensi' => 'required|image|file|max:1000',
            ]);
            if($resultCountCheckTanggalMasuk > 0) {
                return redirect('/absensi-magang/create')->with('fail', 'Anda Sudah Melakukan Absensi Masuk Hari ini');
            }
            $insert['file_uploads']['title'] = "Buakti Absen Masuk ". $request->nama_lengkap;
            $insert['file_uploads']['filename'] = $request->file('file_bukti_absensi')->hashName();
            $insert['file_uploads']['path'] = $request->file('file_bukti_absensi')->store('file-uploads/images');
            $insert['file_uploads']['type'] = $request->file('file_bukti_absensi')->extension();
            $insert['file_uploads']['size'] = $request->file('file_bukti_absensi')->getSize();
            $queryInsertBerkasUploads = FileUploads::create($insert['file_uploads']);
            $resultInsertBerkasUpload = $queryInsertBerkasUploads->id;
            $insert['absens']['tanggal_masuk'] = date('Y-m-d H:i:s', strtotime($request->tanggal));
            $insert['absens']['file_bukti_absensi'] = $resultInsertBerkasUpload;
        } else if($request->status == "Pulang") {
            $request->validate([
                'file_bukti_absensi_pulang' => 'required|image|file|max:1000',
            ]);
            if($resultCountCheckTanggalPulang > 0) {
                return redirect('/absensi-magang/create')->with('fail', 'Anda Sudah Melakukan Absensi Pulang Hari ini');
            }
            if($resultCountCheckTanggalMasuk > 0) {
                $insert['file_uploads']['title'] = "Bukti Absen Pulang ". $request->nama_lengkap;
                $insert['file_uploads']['filename'] = $request->file('file_bukti_absensi_pulang')->hashName();
                $insert['file_uploads']['path'] = $request->file('file_bukti_absensi_pulang')->store('file-uploads/images');
                $insert['file_uploads']['type'] = $request->file('file_bukti_absensi_pulang')->extension();
                $insert['file_uploads']['size'] = $request->file('file_bukti_absensi_pulang')->getSize();
                $queryInsertBerkasUploadsPulang = FileUploads::create($insert['file_uploads']);
                $resultInsertBerkasUploadPulang = $queryInsertBerkasUploadsPulang->id;

                $update['absens']['tanggal_pulang'] = date('Y-m-d H:i:s', strtotime($request->tanggal));
                $update['absens']['file_bukti_absensi_pulang'] = $resultInsertBerkasUploadPulang;
                Absens::where('id', $resultFirstCheckTanggalPulang->id)->update($update['absens']);
                return redirect('/absensi-magang')->with('success', 'Sukses Melakukan Absensi Pulang');
            }
            $insert['absens']['tanggal_pulang'] = date('Y-m-d H:i:s', strtotime($request->tanggal));
        }
        $insert['absens']['status'] = $request->status;
        $queryInsertAbsens = Absens::create($insert['absens']);
        return redirect('/absensi-magang')->with('success', 'Sukses Melakukan Absensi');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function set_approval($id){
        $detailAbsenCount = Absens::where('id', $id)->get()->count();
        if($detailAbsenCount > 0) {
            $detailAbsens = Absens::find($id);
            if($detailAbsens->approval == "N") {
                $update['absens']['approval'] = "Y";
            } else if($detailAbsens->approval == "Y") {
                $update['absens']['approval'] = "N";
            }
            Absens::where('id', $id)->update($update['absens']);
            return redirect('/absensi-magang')->with('success', 'Sukses Melakukan Proses Approval');
        } else {
            return redirect('/absensi-magang')->with('fail', 'Data Tidak Tersedia');
        }
    }
}
