<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Mail\SendMailLinkResetPassword;
use App\Mail\SendMailPasswordRandom;

class LupaPasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('extras.lupa_password.v_lupa_password');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'email' => 'required|email',
        ]);
        //check email
        $checkEmail = User::where("email", $request->email)->get()->count();
        if($checkEmail < 1) {
            return redirect('/lupa-password')->with('fail','Email '.$request->email.' Tidak Tersedia');
        }
        $checkEmail = User::where("email", $request->email)->first();
        if($checkEmail->reset_hash) {
            $then = $checkEmail->reset_hash;
            if(is_numeric($then) && (time() - $then) > 3600 || !is_numeric($then)) { //expired link..
                $reset_data['reset_hash'] = '';
                User::where('email', $request->email)->update($reset_data);
            }
            if(is_numeric($then) && (time() - $then) < 300) { //very early to re-generate! < 5 mins.
                $deficit = 300 - (time() - $then);
                return redirect('/lupa-password')->with('fail','Link Reset sudah dibuat dan dikirim ke email, silahkan coba kembali ' . $this->secondsToTime($deficit));
            }
        }
        $reset_hash = time();
        $reset_data['reset_hash'] = $reset_hash;
        User::where('email', $request->email)->update($reset_data);

        $reset_link = url('/') . "/lupa-password/reset_hash?reset_hash=".rawurlencode($this->mjencode($reset_hash.'|'.$checkEmail->username, Env::get('SALT')));
        $linkResetPassword['reset_link'] = $reset_link;
        Mail::to($request->email)->send(new SendMailLinkResetPassword($linkResetPassword));

        return redirect('/lupa-password')->with('success','Link Lupa Password Sudah Dikirimkan Ke Email '. $request->email.' Silahkan Periksa Pada Folder Inbox Atau Spam Email Anda');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reset_hash(Request $request) {
        if($request->get('reset_hash') !== null) {
            $get_reset = $request->get('reset_hash');
            $reset_hash = $this->mjdecode($get_reset, Env::get('SALT'));
            $data = explode('|', $reset_hash);
            $then = $data[0];

            if(is_numeric($then) && (time() - $then) > 3600 || !is_numeric($then)) {
                return redirect('/lupa-password')->with('fail','Link Lupa Password Sudah Kadaluarsa, Silahkan Melakukan Generate Ulang');
            }
            $found_users = User::where('reset_hash', $then)->where("username", $data[1])->first();
            if($found_users) {
                $password = Str::random(10);
                $updateUsers['password'] = bcrypt($password);
                User::where('id', $found_users->id)->update($updateUsers);
                $passwordRandom['password'] = $password;
                Mail::to($found_users->email)->send(new SendMailPasswordRandom($passwordRandom));

                return redirect('/login')->with('success','Sebuah Password Baru Sudah Dikirimkan Ke Email Anda, Silahkan Periksa Pada FOlder Inbox atau Spam Email Anda');
            } else {
                return redirect('/lupa-password')->with('fail','Data Users Tidak Tersedia');
            }
        }
    }

    function secondsToTime($seconds) {
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$seconds");
        $dtF->diff($dtT)->format('%a days, %h hours, %i minutes, %s seconds');
        $return = '';
        if($dtF->diff($dtT)->format('%a') > 0 ) {
            $return .= $dtF->diff($dtT)->format('%a Days, ');
        }
        if($dtF->diff($dtT)->format('%h') > 0 ) {
            $return .= $dtF->diff($dtT)->format('%h Hours, ');
        }
        if($dtF->diff($dtT)->format('%i') > 0 ) {
            $return .= $dtF->diff($dtT)->format('%i Minutes, ');
        }

        if($dtF->diff($dtT)->format('%s') > 0 ) {
            $return .= $dtF->diff($dtT)->format('%s Seconds');
        }

        return $return;

    }
    function mjencode ($plainText, $cryptKey) {
        $length   = 8;
        $cstrong  = true;
        $cipher   = 'aes-128-cbc';
        if (in_array($cipher, openssl_get_cipher_methods()))  {
            $ivlen = openssl_cipher_iv_length($cipher);
            $iv = openssl_random_pseudo_bytes($ivlen);
            $ciphertext_raw = openssl_encrypt(
                $plainText, $cipher, $cryptKey, $options=OPENSSL_RAW_DATA, $iv);
            $hmac = hash_hmac('sha256', $ciphertext_raw, $cryptKey, $as_binary=true);
            $encodedText = base64_encode( $iv.$hmac.$ciphertext_raw );
        }
        return $encodedText;
    }
    function mjdecode ($encodedText, $cryptKey) {
        $c = base64_decode($encodedText);
        $cipher   = 'aes-128-cbc';
        if (in_array($cipher, openssl_get_cipher_methods()))  {
            $ivlen = openssl_cipher_iv_length($cipher);
            $iv = substr($c, 0, $ivlen);
            $hmac = substr($c, $ivlen, $sha2len=32);
            $ivlenSha2len = $ivlen+$sha2len;
            $ciphertext_raw = substr($c, $ivlen+$sha2len);
            try {
                $plainText = openssl_decrypt(
                    $ciphertext_raw, $cipher, $cryptKey, $options=OPENSSL_RAW_DATA, $iv);
            } catch(Exception $e) {
                $plainText = NULL;
            }
        }
        return $plainText;
    }
}
