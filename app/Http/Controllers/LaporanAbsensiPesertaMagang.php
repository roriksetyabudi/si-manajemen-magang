<?php

namespace App\Http\Controllers;
use App\Models\Absens;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class LaporanAbsensiPesertaMagang extends Controller
{
    public function index(){
        $dataUsersAktifMagangs = DB::select("SELECT a.id_instansi, b.nama_instansi FROM management_magang.magangs a
	                            LEFT JOIN management_magang.instansis b ON a.id_instansi=b.id GROUP BY b.nama_instansi");
        $data['instansi'] = $dataUsersAktifMagangs;
        return view('laporan.absensi_magang.v_laporan_absensi_magang_peserta_magang', $data);
    }

    public function generate(Request $request){
//        $view = view('laporan.absensi_magang.cetak_absensi_magang_peserta', [
//            "detailAbsens" => 1,
//            "tanggal_mulai" => $request->tanggal_mulai,
//            "tanggal_selesai" => $request->tanggal_selesai
//        ]);

        $html = '<style>
    table.display thead th{
        font-size: 14px;text-align: center;font-weight: bold;
    }
    table.display {
        font-size:15px;
        border:black thin;
    }

</style>';
    $html .= '<table class="table_data">
    <tr>
        <td align="centar" width="15%" class="header_logo" rowspan="4"><img src="./template/dist/img/pos-indonesia.jpg" width="75px"/></td>
        <td align="center" width="70%" class="header_title" style="letter-spacing: 3px;"><H2>PT Pos Indonesia (Persero) Madiun</H2></td>
        <td align="centar" width="15%" class="header_logo" rowspan="4"><H4></H4></td>
    </tr>
    <tr><td align="center" width="70%" class="header_title" colspan="3" style="letter-spacing: 2px;"><H3><b>Jalan Pahlawan Nomor 24 Madiun Kode Pos 63100</b></H3></td></tr>
    <tr><td align="center" width="70%" class="header_title" colspan="3" style="letter-spacing: 1px;"><H4>Website : <i>www.posindonesia.co.id</i>, Telepon : <i>0351-464454</i></H4></td></tr>

    <tr><td align="center" width="70%" class="header_title" colspan="3"></td></tr>
</table>';
        //$html .= '<h1 align="center">LAPORAN DATA ABSENSI PESERTA MAGANG<br>PERIODE GENERATE '.date('F Y', strtotime($request->tanggal)).'</h1>';
        $html .= '<table width="100%" border="1" cellpadding="3" cellspacing="0">
    <thead>
    <tr style="background-color:#FF0000;color:#ffffff;">
        <td width="2%" rowspan="2" align="center"><b>NO</b></td>
        <td width="5%" rowspan="2" align="center"><b>NAMA</b></td>
        <td width="93%" colspan="31" align="center"><b>TANGGAL dan JAM KERJA HARIAN</b></td>
    </tr>
    <tr style="background-color:#FF0000;color:#ffffff;">
        <td align="center" width="3%"><b>01</b></td>
        <td align="center" width="3%"><b>02</b></td>
        <td align="center" width="3%"><b>03</b></td>
        <td align="center" width="3%"><b>04</b></td>
        <td align="center" width="3%"><b>05</b></td>
        <td align="center" width="3%"><b>06</b></td>
        <td align="center" width="3%"><b>07</b></td>
        <td align="center" width="3%"><b>08</b></td>
        <td align="center" width="3%"><b>09</b></td>
        <td align="center" width="3%"><b>10</b></td>
        <td align="center" width="3%"><b>11</b></td>
        <td align="center" width="3%"><b>12</b></td>
        <td align="center" width="3%"><b>13</b></td>
        <td align="center" width="3%"><b>14</b></td>
        <td align="center" width="3%"><b>15</b></td>
        <td align="center" width="3%"><b>16</b></td>
        <td align="center" width="3%"><b>17</b></td>
        <td align="center" width="3%"><b>18</b></td>
        <td align="center" width="3%"><b>19</b></td>
        <td align="center" width="3%"><b>20</b></td>
        <td align="center" width="3%"><b>21</b></td>
        <td align="center" width="3%"><b>22</b></td>
        <td align="center" width="3%"><b>23</b></td>
        <td align="center" width="3%"><b>24</b></td>
        <td align="center" width="3%"><b>25</b></td>
        <td align="center" width="3%"><b>26</b></td>
        <td align="center" width="3%"><b>27</b></td>
        <td align="center" width="3%"><b>28</b></td>
        <td align="center" width="3%"><b>29</b></td>
        <td align="center" width="3%"><b>30</b></td>
        <td align="center" width="3%"><b>31</b></td>

    </tr>
    </thead>';
        $requestAbsensi = $this->sql_absensi($request->id_instansi, $request->tanggal);

        $no = 1;
        $tanggal_plus = date("Y")."-".date("m")."-1";
        foreach ($requestAbsensi as $item) {
//            $tanggaldaysPlus = strtotime("Y-m-d", strtotime($tanggal_plus."+ ".$no." days"));
//            $checkIzins = $this->check_izin($item->id_users_magang);
//            $status_izin = "";
//            if($checkIzins > 0) {
//                $status_izin = "Izin";
//            }

            $html .= '<tr >
        <td align="center" width="2%">'.$no.'</td>
        <td align="left" width="5%">'.$item->nama_lengkap .'</td>
        <td align="center" width="3%">'.$item->absen_1.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-1").'</td>
        <td align="center" width="3%">'.$item->absen_2.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-2").'</td>
        <td align="center" width="3%">'.$item->absen_3.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-3").'</td>
        <td align="center" width="3%">'.$item->absen_4.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-4").'</td>
        <td align="center" width="3%">'.$item->absen_5.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-5").'</td>
        <td align="center" width="3%">'.$item->absen_6.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-6").'</td>
        <td align="center" width="3%">'.$item->absen_7.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-7").'</td>
        <td align="center" width="3%">'.$item->absen_8.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-8").'</td>
        <td align="center" width="3%">'.$item->absen_9.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-9").'</td>
        <td align="center" width="3%">'.$item->absen_10.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-10").'</td>
        <td align="center" width="3%">'.$item->absen_11.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-11").'</td>
        <td align="center" width="3%">'.$item->absen_12.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-12").'</td>
        <td align="center" width="3%">'.$item->absen_13.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-13").'</td>
        <td align="center" width="3%">'.$item->absen_14.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-14").'</td>
        <td align="center" width="3%">'.$item->absen_15.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-15").'</td>
        <td align="center" width="3%">'.$item->absen_16.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-16").'</td>
        <td align="center" width="3%">'.$item->absen_17.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-17").'</td>
        <td align="center" width="3%">'.$item->absen_18.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-18").'</td>
        <td align="center" width="3%">'.$item->absen_19.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-19").'</td>
        <td align="center" width="3%">'.$item->absen_20.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-20").'</td>
        <td align="center" width="3%">'.$item->absen_21.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-21").'</td>
        <td align="center" width="3%">'.$item->absen_22.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-22").'</td>
        <td align="center" width="3%">'.$item->absen_23.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-23").'</td>
        <td align="center" width="3%">'.$item->absen_24.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-24").'</td>
        <td align="center" width="3%">'.$item->absen_25.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-25").'</td>
        <td align="center" width="3%">'.$item->absen_26.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-26").'</td>
        <td align="center" width="3%">'.$item->absen_27.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-27").'</td>
        <td align="center" width="3%">'.$item->absen_28.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-28").'</td>
        <td align="center" width="3%">'.$item->absen_29.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-29").'</td>
        <td align="center" width="3%">'.$item->absen_30.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-30").'</td>
        <td align="center" width="3%">'.$item->absen_31.' '.$this->check_izin($item->id_users_magang, date("Y")."-".date("m")."-31").'</td>';
            $html .= '
    </tr>';
            $no++;
        }



    $html .='</table>';

        //$html_content = $this->r;
        PDF::SetAuthor('Rorik Setya Budi');
        PDF::SetTitle('Laporan Data Sistem Informasi Manajemen Magang');
        PDF::SetSubject("Laporan Absensi");
        PDF::SetKeywords('sistem informasi manajemen magang');
        PDF::setPrintHeader(false);
        PDF::setPrintFooter(false);
        PDF::SetMargins(7, 7, 7, 7);
        PDF::SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

        PDF::AddPage('L', 'F4');
        PDF::SetFont('helvetica', null, 8);
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output('data_absensi.pdf');
    }

    public function sql_absensi($instansi, $tanggalBulanTahunRekap){
        $tahun = date("Y", strtotime($tanggalBulanTahunRekap));
        $bulan = date("m", strtotime($tanggalBulanTahunRekap));
        $sql = DB::select("SELECT b.nama_lengkap,a.id_users_magang,
                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='01' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_1,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='02' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_2,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='03' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_3,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='04' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_4,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='05' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_5,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='06' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_6,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='07' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_7,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='08' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_8,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='09' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_9,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='10' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_10,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='11' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_11,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='12' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_12,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='13' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_13,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='14' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_14,

                        (SELECT COALESCE(CONCAT(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='15' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_15,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='16' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_16,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='17' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_17,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='18' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_18,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='19' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_19,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='20' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_20,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='21' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_21,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='22' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_22,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='23' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_23,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='24' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_24,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='25' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_25,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='26' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_26,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='27' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_27,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk), '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='28' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_28,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='29' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_29,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='30' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_30,

                        (SELECT CONCAT(COALESCE(date_format(TIME(aa.tanggal_masuk) , '%H:%i'),''),'<br>', COALESCE(date_format(TIME(aa.tanggal_pulang) , '%H:%i'),'')) FROM management_magang.absens aa
                            WHERE YEAR(aa.tanggal_masuk)='$tahun' AND MONTH(aa.tanggal_masuk)='$bulan' AND DAY(aa.tanggal_masuk)='31' AND aa.id_users=a.id_users_magang ORDER BY aa.id DESC LIMIT 1) AS absen_31

                        FROM management_magang.magangs a
                        LEFT JOIN management_magang.users b ON a.id_users_magang=b.id
                        LEFT JOIN management_magang.instansis c ON a.id_instansi=c.id
                        WHERE a.id_instansi IN ('$instansi') and a.deleted = '0'");
        return $sql;
    }
    public function check_izin($id_users, $tanggal){
        $sql1  = DB::table("izins")
            ->where('tanggal_izin', '<=', $tanggal)
            ->where('tanggal_selesai_izin', '>=', $tanggal)
            ->where('id_users', '=', $id_users)
            ->where('status', '=', 'Disetujui')
            ->orderBy('id',  'DESC')
            ->limit(1)
            ->count();

//        $sql = DB::select("SELECT * FROM management_magang.izins a
//	            WHERE '.$tanggal.' >= a.tanggal_izin AND '.$tanggal.' <= a.tanggal_selesai_izin
//					and a.id_users = '$id_users' AND a.`status` = 'Disetujui'  ORDER BY a.id DESC LIMIT 1");

        return ($sql1 > 0) ? 'Izin' : "";
    }


}
