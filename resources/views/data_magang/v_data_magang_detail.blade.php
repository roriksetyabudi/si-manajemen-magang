@extends('template.v_template')
@section('title', 'Dashboard')
@push('head-css')
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template/')}}/plugins/fontawesome-free/css/all.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">


    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('template/')}}/plugins/fontawesome-free/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template/')}}/dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('template/')}}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{asset('template/')}}/plugins/daterangepicker/daterangepicker.css">
@endpush
@section('content')
    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
        </symbol>
        <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
        </symbol>
        <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
        </symbol>
    </svg>
    <div class="container-fluid">

        @if(session()->has('success'))
            <div class="alert alert-success d-flex align-items-center" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                <div>
                    {{ session('success') }}
                </div>
            </div>

        @elseif(session()->has('fail'))
            <div class="alert alert-danger" role="alert">
                {{ session('fail') }}
            </div>
        @endif
        <div class="row">

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header border-transparent">
                        <h3 class="card-title">Data Absensi Peserta Magang</h3>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table m-0" id="table_data_absensi">
                                <thead>
                                <tr>
                                    <th>Nama Peserta</th>
                                    <th>Masuk</th>
                                    <th>Pulang</th>
                                    <th>Status</th>
                                    <th>Updated</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($absens as $absen)
                                    <tr>
                                        <td>{{ $absen->user->nama_lengkap }}</td>
                                        <td>
                                            {{ date('d F Y, H:i:s', strtotime($absen->tanggal_masuk)) }}
                                            @if(date('H:i:s', strtotime($absen->tanggal_masuk)) > '06:00:00' && date('H:i:s', strtotime($absen->tanggal_masuk)) < '08:05:00')
                                                <span class="badge badge-success">Tepat</span>
                                            @elseif(date('H:i:s', strtotime($absen->tanggal_masuk)) > '08:05:00' && date('H:i:s', strtotime($absen->tanggal_masuk)) < '09:05:00')
                                                <span class="badge badge-warning">Terlambat</span>
                                            @else
                                                <span class="badge badge-danger">Tidak Absen</span>
                                            @endif
                                        </td>
                                        <td>

                                            @if($absen->tanggal_pulang == "" || $absen->tanggal_pulang == null)
                                            @else
                                                {{ date('d F Y, H:i:s', strtotime($absen->tanggal_pulang)) }}
                                            @endif

                                            @if(date('H:i:s', strtotime($absen->tanggal_pulang)) >= '15:00:00')
                                                <span class="badge badge-success">Tepat</span>
                                            @elseif(date('H:i:s', strtotime($absen->tanggal_pulang)) > '14:00:00' && date('H:i:s', strtotime($absen->tanggal_pulang)) <= '15:00:00')
                                                <span class="badge badge-warning">Pulang Awal</span>
                                            @else
                                                <span class="badge badge-danger">Tidak Absen</span>
                                        @endif

                                        <td>
                                            @if($absen->tanggal_masuk != ""  && $absen->tanggal_pulang != "")
                                                <span class="badge badge-success">Hadir</span>
                                            @else
                                                <span class="badge badge-danger">Tidak Hadir</span>
                                            @endif

                                        </td>
                                        <td>{{ date('d F Y, H:i:s', strtotime($absen->updated_at)) }}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.card-body -->
                    <!-- /.card-footer -->
                </div>

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="ion ion-clipboard mr-1"></i>
                            Aktifitas Kegiatan
                        </h3>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <ul class="todo-list" data-widget="todo-list">
                            @foreach($aktifitasMagangs as $aktifitasMagang)
                                <li>
                                <span class="handle">
                                  <i class="fas fa-ellipsis-v"></i>
                                  <i class="fas fa-ellipsis-v"></i>
                                </span>
                                    <span class="text">{{ $aktifitasMagang->nama_kegiatan }} <small class="badge badge-info">{{ $aktifitasMagang->user->nama_lengkap }}</small></span>
                                    <small class="badge badge-success"><i class="far fa-clock"></i> {{ $aktifitasMagang->interval_aktifitas }}</small>
                                    <small class="badge badge-info"><i class="far fa-clock"></i> {{ date('d F y H:i:s', strtotime($aktifitasMagang->created_at)) }}</small>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                    <!-- /.card-body -->
                </div>

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="ion ion-clipboard mr-1"></i>
                            Unggah Tugas Akhir Magang
                        </h3>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">

                        <form method="post" action="/upload-file-berkas-tugas-akhir-magang" enctype="multipart/form-data">
                            @csrf
                            <input type="text" class="form-control @error('id_magang') is-invalid @enderror" id="id_magang" name="id_magang" placeholder="id_magang" value="{{ $id }}" readonly hidden>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tugas Akhir Magang <label class="text-red">*</label> </label>
                                    <p class="text-muted h6">Upload Tugas Akhir Magang format .pdf</p>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input @error('file_berkas_tugas_akhir_magang') is-invalid @enderror" id="file_berkas_tugas_akhir_magang" name="file_berkas_tugas_akhir_magang">
                                        @error('file_berkas_tugas_akhir_magang')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                        <label class="custom-file-label" for="exampleInputFile" required>Upload Tugas Akhir Magang</label>
                                    </div>

                                </div>

                            </div>
                            @if($detailMagang->file_berkas_tugas_akhir_magang != 0)
                                <div class="card-footer bg-white">
                                    <ul class="mailbox-attachments d-flex align-items-stretch clearfix">
                                        <li>
                                            <span class="mailbox-attachment-icon"><i class="far fa-file-pdf"></i></span>

                                            <div class="mailbox-attachment-info">
                                                <a href="#" class="mailbox-attachment-name"><i class="fas fa-paperclip"></i> {{ $detailMagang->fileUploadsBerkasTugasAkhirMagang->filename }}</a>
                                                <span class="mailbox-attachment-size clearfix mt-1">
                                              <span>{{ $detailMagang->fileUploadsBerkasTugasAkhirMagang->size }}</span>
                                              <a href="/storage/{{ $detailMagang->fileUploadsBerkasTugasAkhirMagang->path }}" target="_blank" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                                            </span>
                                            </div>
                                        </li>


                                    </ul>
                                </div>
                            @endif
                            @if(auth()->user()->privileges == "USERS MAGANG")
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Upload Tugas Akhir Magangs</button>
                                </div>
                            @endif

                        </form>

                    </div>
                    <!-- /.card-body -->
                </div>

            </div>
            <div class="col-md-4">

                <div class="card">
                    <div class="card-header border-transparent">
                        <h3 class="card-title">Data Nilai Peserta Magang</h3>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table m-0">
                                <thead>
                                <tr>
                                    <th>Penilaian</th>
                                    <th>Nilai </th>


                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Kedisiplinan</td>
                                    <td>{{ empty($dataNilai->kedisiplinan) ? 0 : $dataNilai->kedisiplinan }}</td>
                                </tr>
                                <tr>
                                    <td>Sikap</td>
                                    <td>{{ empty($dataNilai->sikap) ? 0 : $dataNilai->sikap }}</td>
                                </tr>
                                <tr>
                                    <td>Komunikasi</td>
                                    <td>{{ empty($dataNilai->komunikasi) ? 0 : $dataNilai->komunikasi }}</td>
                                </tr>
                                <tr>
                                    <td>Kerapian</td>
                                    <td>{{ empty($dataNilai->kerapian) ? 0 : $dataNilai->kerapian }}</td>
                                </tr>
                                <tr>
                                    <td>Kerjasama</td>
                                    <td>{{ empty($dataNilai->kerjasama) ? 0 : $dataNilai->kerjasama }}</td>
                                </tr>
                                <tr>
                                    <td>Motivasi</td>
                                    <td>{{ empty($dataNilai->motivasi) ? 0 : $dataNilai->motivasi  }}</td>
                                </tr>
                                <tr>
                                    <td>Penguasaan Terhadap Pekerjaan</td>
                                    <td>{{ empty($dataNilai->penguasaan_terhadap_pekerjaan) ? 0 : $dataNilai->penguasaan_terhadap_pekerjaan }}</td>
                                </tr>
                                <tr>
                                    <td>Hasil Pekerjaan</td>
                                    <td>{{ empty($dataNilai->hasil_pekerjaan) ? 0 : $dataNilai->hasil_pekerjaan }}</td>
                                </tr>
                                <tr>
                                    <td>Ide Gagasan</td>
                                    <td>{{ empty($dataNilai->ide_atau_gagasan) ? 0 : $dataNilai->ide_atau_gagasan }}</td>
                                </tr>
                                <tr>
                                    <td>Tangung Jawab</td>
                                    <td>{{ empty($dataNilai->tanggung_jawab) ? 0 : $dataNilai->tanggung_jawab }}</td>
                                </tr>
                                <tr>
                                    <td>Kejujuran</td>
                                    <td>{{ empty($dataNilai->kejujuran) ? 0 : $dataNilai->kejujuran }}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.card-body -->
                    <!-- /.card-footer -->
                </div>

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Izin</h3>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <ul class="products-list product-list-in-card pl-2 pr-2">
                            @foreach($dataIzinMagangs as $dataIzinMagang)
                                <li class="item">
                                    <div class="product-img">
                                        @if($dataIzinMagang->user->photo == "")
                                            <img src="{{asset('template')}}/dist/img/avatar.png" alt="Product Image" class="img-size-50">
                                        @else
                                            <img src="{{asset('storage/'.$dataIzinMagang->user->fileuploads->path )}}" alt="Product Image" class="img-size-50">
                                        @endif



                                    </div>
                                    <div class="product-info">
                                        <a href="data-izin-detail/{{ $dataIzinMagang->id }}" class="product-title">{{ $dataIzinMagang->user->nama_lengkap }}
                                            <span class="badge badge-warning float-right">Detail</span></a>
                                        <span class="product-description">
                                        {{ $dataIzinMagang->alasan_izin }}
                                    </span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /.card-body -->
                    <!-- /.card-footer -->
                </div>
            </div>
        </div>
    </div>


@endsection
@push('bottom-js')
    <!-- jQuery -->
    <script src="{{asset('template/')}}/plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('template/')}}/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template/')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- DataTables  & Plugins -->
    <script src="{{asset('template')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/jszip/jszip.min.js"></script>
    <script src="{{asset('template')}}/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{asset('template')}}/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


    <!-- overlayScrollbars -->
    <script src="{{asset('template/')}}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>

    <!-- date-range-picker -->
    <script src="{{asset('template/')}}/plugins/daterangepicker/daterangepicker.js"></script>


    <!-- AdminLTE App -->
    <script src="{{asset('template/')}}/dist/js/adminlte.js"></script>
    <script>
        $(function () {

            //Date picker
            $('#reservationdate').datetimepicker({
                format: 'L'
            });
        });
    </script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#table_data_absensi').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>

@endpush

