@extends('template.v_template')
@section('title', 'Data Absensi Magang')
@push('head-css')
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/adminlte.min.css">

    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />


@endpush
@section('content')
    <div class="card">
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
            </symbol>
            <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
            </symbol>
            <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
            </symbol>
        </svg>
        <div class="card-header">
            <h3 class="card-title">Data Kegiatan / Aktifitas Magang</h3>
            @if($privileges == "PEMBIMBING MAGANG")
                <div class="card-tools">
                    <a href="/pemberian-tugas/create" class="btn btn-info btn-sm">Tambah Tugas Baru</a>
                </div>
            @endif

        </div>
        <div class="card-body">
            @if(session()->has('success'))
                <div class="alert alert-success d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                    <div>
                        {{ session('success') }}
                    </div>
                </div>
            @endif

            @if(session()->has('fail'))
                <div class="alert alert-danger" role="alert">
                    {{ session('fail') }}
                </div>
            @endif
            <form class="form-horizontal">



            </form>

            <table class="table table-bordered table_list_data_pemberian_tugas" id="table_list_data_pemberian_tugas">
                <thead>
                <tr class="bg-info">
                    <th>Nama Peserta Magang</th>
                    <th>Nama Tugas</th>
                    <th>Keterangan</th>
                    <th>Lampiran</th>
                    <th>Tanggal Dibuat</th>
                    <th>Target</th>
                    <th>Realisasi</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>


                </tbody>
            </table>

        </div>


    </div>
    <!-- Modal -->


@endsection
@push('bottom-js')
    <script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{asset('template')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/jszip/jszip.min.js"></script>
    <script src="{{asset('template')}}/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{asset('template')}}/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->

    <script type="text/javascript">

        $(function () {

            var table = $('.table_list_data_pemberian_tugas').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('pemberian_tugas.index') }}",
                    data: function (d){
                            d.search = $('input[type="search"]').val()
                    },
                },

                columns: [
                    {data: 'user', name: 'users_diberi_tugas'},
                    {data: 'nama_tugas', name: 'nama_tugas'},
                    {data: 'keterangan', name: 'keterangan'},
                    {data: 'lampiran', name: 'lampiran'},
                    {data: 'tanggal_dibuat', name: 'tanggal_dibuat'},
                    {data: 'target', name: 'target'},
                    {data: 'realisasi', name: 'realisasi'},
                    {data: 'action', name: 'action', orderable: false, searchable: false, className: "text-center"},
                ]
            });
            // $("#tanggal_mulai, #nama_peserta, #tanggal_selesai, #nama_kegiatan").change(function(e){
            //     //table.column($(this).data('column')).search($(this).val()).draw();
            //     table.draw();
            //     e.preventDefault();
            // });
        });

    </script>


@endpush

