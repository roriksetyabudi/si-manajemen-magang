<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manajemen Magang | Registrasi Magang</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/adminlte.min.css">
</head>
<body>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">

        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
        </symbol>
        <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
        </symbol>
        <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
        </symbol>
    </svg>

    <div class="container">
        <div class="row align-items-center">
            <!-- /.col -->
            <div class="col-md-12">
                <form method="post" action="/registrasi-magang/post" enctype="multipart/form-data">
                    @csrf
                    <div class="card card-primary card-outline">
                        <div class="card-header text-center">
                            <a href="/home" class="h3"><b>PENDAFTARAN PESERTA </b> MAGANG</a>
                        </div>

                        <!-- /.card-header -->
                        <div class="card-body p-2">
                            @if(session()->has('success'))
                                <div class="alert alert-success d-flex align-items-center" role="alert">
                                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                                    <div>
                                        {{ session('success') }}
                                    </div>
                                </div>

                            @elseif(session()->has('fail'))
                                <div class="alert alert-danger" role="alert">
                                    {{ session('fail') }}
                                </div>
                            @endif
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Username <label class="text-red">*</label> </label>
                                            <input type="text" class="form-control @error('username_peserta_magang') is-invalid @enderror" id="username_peserta_magang" name="username_peserta_magang" placeholder="Username Peserta Magang">
                                            @error('username_peserta_magang')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nama Lengkap <label class="text-red">*</label> </label>
                                            <input type="text" class="form-control @error('nama_lengkap_peserta_magang') is-invalid @enderror" id="nama_lengkap_peserta_magang" name="nama_lengkap_peserta_magang" placeholder="Nama Lengkap Peserta Magang">
                                            @error('nama_lengkap_peserta_magang')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Alamat Lengkap <label class="text-red">*</label> </label>
                                            <input type="text" class="form-control @error('alamat_lengkap_peserta_magang') is-invalid @enderror" id="alamat_lengkap_peserta_magang" name="alamat_lengkap_peserta_magang" placeholder="Alamat Lengkap Peserta">
                                            @error('alamat_lengkap_peserta_magang')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Jenis Kelamin <label class="text-red">*</label> </label>
                                            <div class="input-group mb-3">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input @error('jenis_kelamin_peserta_magang') is-invalid @enderror" type="radio" name="jenis_kelamin_peserta_magang" id="jenis_kelamin_peserta_magang" value="L">
                                                    <label class="form-check-label" for="inlineRadio1">Laki-Laki</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input @error('jenis_kelamin_peserta_magang') is-invalid @enderror" type="radio" name="jenis_kelamin_peserta_magang" id="jenis_kelamin_peserta_magang" value="P">
                                                    <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">No Telepon/HP <label class="text-red">*</label> </label>
                                            <input type="number" class="form-control @error('telepon_peserta_magang') is-invalid @enderror" id="telepon_peserta_magang" name="telepon_peserta_magang" placeholder="Telepon / HP">
                                            @error('telepon_peserta_magang')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email Aktif <label class="text-red">*</label> </label>
                                            <input type="text" class="form-control @error('email_peserta_magang') is-invalid @enderror" id="email_peserta_magang" name="email_peserta_magang" placeholder="Email Aktif">
                                            @error('email_peserta_magang')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Upload Berkas Magang <label class="text-red">*</label> </label>
                                            <p class="text-muted h6">Kartu Identitas dan Surat Keterangan Magang dari Instansi, Digabung Jadi 1 File .pdf maksimal 2Mb</p>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input @error('fupload_berkas_peserta_magang') is-invalid @enderror" id="fupload_berkas_peserta_magang" name="fupload_berkas_peserta_magang">
                                                @error('fupload_berkas_peserta_magang')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                                @enderror
                                                <label class="custom-file-label" for="exampleInputFile" required>Upload Berkas</label>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Tanggal Ajuan Mulai Magang <label class="text-red">*</label> </label>
                                            <input type="date" class="form-control @error('tanggal_ajuan_mulai_magang') is-invalid @enderror" id="tanggal_ajuan_mulai_magang" name="tanggal_ajuan_mulai_magang" placeholder="Tanggal Ajuan Mulai Magang">
                                            @error('tanggal_ajuan_mulai_magang')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="col-sm-6">

                                        <div class="form-group ">
                                            <label for="exampleInputEmail1">Password <label class="text-red">*</label> </label>
                                            <div class="input-group mb-3">
                                                <input type="password" class="form-control @error('password_peserta_magang') is-invalid @enderror" id="password_peserta_magang" name="password_peserta_magang" placeholder="Password">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" onclick="password_show_hide();">
                                                      <i class="fas fa-eye" id="show_eye"></i>
                                                      <i class="fas fa-eye-slash d-none" id="hide_eye"></i>
                                                    </span>
                                                </div>
                                            </div>

                                            @error('password_peserta_magang')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group ">
                                            <label for="exampleInputEmail1">Ulangi Password <label class="text-red">*</label> </label>
                                            <div class="input-group mb-3">
                                                <input type="password" class="form-control @error('ulangi_password_peserta_magang') is-invalid @enderror" id="ulangi_password_peserta_magang" name="ulangi_password_peserta_magang" placeholder="Ulangi Password">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" onclick="password_show_hide_ulangi();">
                                                      <i class="fas fa-eye" id="show_eye_ulangi"></i>
                                                      <i class="fas fa-eye-slash d-none" id="hide_eye_ulangi"></i>
                                                    </span>
                                                </div>
                                            </div>

                                            @error('ulangi_password_peserta_magang')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nama Instansi <label class="text-red">*</label> </label>
                                            <input type="text" class="form-control @error('nama_instansi') is-invalid @enderror" id="nama_instansi" name="nama_instansi" placeholder="Nama Instansi">
                                            @error('nama_instansi')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Alamat Lengkap Instansi <label class="text-red">*</label> </label>
                                            <input type="text" class="form-control @error('alamat_lengkap_instansi') is-invalid @enderror" id="alamat_lengkap_instansi" name="alamat_lengkap_instansi" placeholder="Alamat Instansi">
                                            @error('alamat_lengkap_instansi')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Telepon Instansi <label class="text-red">*</label> </label>
                                            <input type="number" class="form-control @error('telepon_instansi') is-invalid @enderror" id="telepon_instansi" name="telepon_instansi" placeholder="Telepon Instansi">
                                            @error('telepon_instansi')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Jurusan / Bidang Keahlian <label class="text-red">*</label> </label>
                                            <input type="text" class="form-control @error('bidang_keahlian_peserta_magang') is-invalid @enderror" id="bidang_keahlian_peserta_magang" name="bidang_keahlian_peserta_magang" placeholder="Jurusan / Bidang Keahlian">
                                            @error('bidang_keahlian_peserta_magang')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Foto Peserta <label class="text-red">*</label> </label>
                                            <p class="text-muted h6">Pas Foto Terbaru, Maksimal 1Mb</p>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input @error('fupload_berkas_foto_peserta_magang') is-invalid @enderror" id="fupload_berkas_foto_peserta_magang" name="fupload_berkas_foto_peserta_magang">
                                                @error('fupload_berkas_foto_peserta_magang')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                                @enderror
                                                <label class="custom-file-label" for="exampleInputFile" required>Upload Foto</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nama Pembimbing Sekolah <label class="text-red">*</label> </label>
                                            <input type="text" class="form-control @error('nama_pembimbing_sekolah_peserta_magang') is-invalid @enderror" id="nama_pembimbing_sekolah_peserta_magang" name="nama_pembimbing_sekolah_peserta_magang" placeholder="Pembimbing Sekolah">
                                            @error('nama_pembimbing_sekolah_peserta_magang')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Tanggal Ajuan Selesai Magang <label class="text-red">*</label> </label>
                                            <input type="date" class="form-control @error('tanggal_ajuan_selesai_magang') is-invalid @enderror" id="tanggal_ajuan_selesai_magang" name="tanggal_ajuan_selesai_magang" placeholder="Tanggal Ajuan Selesai Magang">
                                            @error('tanggal_ajuan_selesai_magang')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <!-- /.card-footer -->
                        <div class="card-footer">
                            <div class="float-right">

                                <a href="/registrasi-akun"  class="btn btn-default"><i class="fas fa-share"></i> Aktivasi Akun</a>
                                <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Proses</button>

                            </div>
                            <a href="/login" class="btn btn-warning"><i class="fas fa-lock"></i> Goto Login</a>

                        </div>
                        <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->
                </form>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.register-box -->



<!-- jQuery -->
<script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>

<script>
    function password_show_hide() {
        var x = document.getElementById("password_peserta_magang");
        var show_eye = document.getElementById("show_eye");
        var hide_eye = document.getElementById("hide_eye");
        hide_eye.classList.remove("d-none");
        if (x.type === "password") {
            x.type = "text";
            show_eye.style.display = "none";
            hide_eye.style.display = "block";
        } else {
            x.type = "password";
            show_eye.style.display = "block";
            hide_eye.style.display = "none";
        }
    }
    function password_show_hide_ulangi() {
        var x = document.getElementById("ulangi_password_peserta_magang");
        var show_eye = document.getElementById("show_eye_ulangi");
        var hide_eye = document.getElementById("hide_eye_ulangi");
        hide_eye.classList.remove("d-none");
        if (x.type === "password") {
            x.type = "text";
            show_eye.style.display = "none";
            hide_eye.style.display = "block";
        } else {
            x.type = "password";
            show_eye.style.display = "block";
            hide_eye.style.display = "none";
        }
    }
</script>
</body>
</html>

