<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PDF;
use App\Models\NilaiTrans;
class LaporanNilai extends Controller
{
    public function index(){
        return view('laporan.nilai_magang.v_laporan_nilai_magang');
    }
    public function generate_pdf(Request $request) {
        $request->validate([
            'tanggal_mulai' => 'required|min:3',
            'tanggal_selesai' => 'required|min:3',
        ]);
        if(auth()->user()->privileges == "PEMBIMBING SEKOLAH") {
            $nilai = NilaiTrans::leftJoin('magangs', 'magangs.id_users_magang', '=', 'nilai_trans.id_users')
                ->leftJoin('users', 'nilai_trans.id_users', '=', 'users.id')
                ->select(['nilai_trans.id as id_nilai_trans','nilai_trans.id_users','nilai_trans.kedisiplinan','nilai_trans.sikap','nilai_trans.komunikasi',
                    'nilai_trans.kerapian','nilai_trans.kerjasama','nilai_trans.motivasi','nilai_trans.penguasaan_terhadap_pekerjaan','nilai_trans.hasil_pekerjaan',
                    'nilai_trans.ide_atau_gagasan','nilai_trans.deleted','nilai_trans.created_at','users.nama_lengkap',
                    'nilai_trans.tanggung_jawab','nilai_trans.kejujuran'])
                ->whereNotNull('magangs.id_users_magang')
                ->where('id_pembimbing_sekolah', auth()->user()->id)
                ->whereDate('nilai_trans.created_at', '>=', date('Y-m-d', strtotime($request->post('tanggal_mulai'))))
                ->whereDate('nilai_trans.created_at', '<=', date('Y-m-d', strtotime($request->post('tanggal_selesai'))))->get();
        } else if(auth()->user()->privileges == "PEMBIMBING MAGANG") {
            $nilai = NilaiTrans::leftJoin('magangs', 'magangs.id_users_magang', '=', 'nilai_trans.id_users')
                ->leftJoin('users', 'nilai_trans.id_users', '=', 'users.id')
                ->select(['nilai_trans.id as id_nilai_trans','nilai_trans.id_users','nilai_trans.kedisiplinan','nilai_trans.sikap','nilai_trans.komunikasi',
                    'nilai_trans.kerapian','nilai_trans.kerjasama','nilai_trans.motivasi','nilai_trans.penguasaan_terhadap_pekerjaan','nilai_trans.hasil_pekerjaan',
                    'nilai_trans.ide_atau_gagasan','nilai_trans.deleted','nilai_trans.created_at','users.nama_lengkap',
                    'nilai_trans.tanggung_jawab','nilai_trans.kejujuran'])
                ->whereNotNull('magangs.id_users_magang')
                ->where('id_users_pembimbing_magang', auth()->user()->id)
                ->whereDate('nilai_trans.created_at', '>=', date('Y-m-d', strtotime($request->post('tanggal_mulai'))))
                ->whereDate('nilai_trans.created_at', '<=', date('Y-m-d', strtotime($request->post('tanggal_selesai'))))->get();
        } else if(auth()->user()->privileges == "ADMIN") {
            $nilai = NilaiTrans::leftJoin('magangs', 'magangs.id_users_magang', '=', 'nilai_trans.id_users')
                ->leftJoin('users', 'nilai_trans.id_users', '=', 'users.id')
                ->select(['nilai_trans.id as id_nilai_trans','nilai_trans.id_users','nilai_trans.kedisiplinan','nilai_trans.sikap','nilai_trans.komunikasi',
                    'nilai_trans.kerapian','nilai_trans.kerjasama','nilai_trans.motivasi','nilai_trans.penguasaan_terhadap_pekerjaan','nilai_trans.hasil_pekerjaan',
                    'nilai_trans.ide_atau_gagasan','nilai_trans.deleted','nilai_trans.created_at','users.nama_lengkap',
                    'nilai_trans.tanggung_jawab','nilai_trans.kejujuran'])
                ->whereNotNull('magangs.id_users_magang')
                ->whereDate('nilai_trans.created_at', '>=', date('Y-m-d', strtotime($request->post('tanggal_mulai'))))
                ->whereDate('nilai_trans.created_at', '<=', date('Y-m-d', strtotime($request->post('tanggal_selesai'))))->get();
        }
        $view = view('laporan.nilai_magang.cetak', [
            "detailNilai" => $nilai,
            "tanggal_mulai" => $request->tanggal_mulai,
            "tanggal_selesai" => $request->tanggal_selesai
        ]);
        $html_content = $view->render();
        PDF::SetAuthor('Rorik Setya Budi');
        PDF::SetTitle('Laporan Data Sistem Informasi Manajemen Magang');
        PDF::SetSubject("Laporan Nilai Magang");
        PDF::SetKeywords('sistem informasi manajemen magang');
        PDF::setPrintHeader(false);
        PDF::setPrintFooter(false);
        PDF::SetMargins(7, 7, 7, 7);
        PDF::SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

        PDF::AddPage('L', 'F4');
        PDF::SetFont('helvetica', null, 9);
        PDF::writeHTML($html_content, true, false, true, false, '');
        PDF::Output('data_nilai.pdf');
    }
}
