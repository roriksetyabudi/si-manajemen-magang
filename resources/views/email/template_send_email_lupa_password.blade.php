@component('mail::message')
    # Permintaan Lupa Password
    Berikut Kami Kirimkan Link Permintaan Lupa Password
    {{ $linkResetPassword['reset_link'] }}

    Terima Kasih
    {{ config('app.name') }}
@endcomponent
