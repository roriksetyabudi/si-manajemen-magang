@extends('template.v_template')
@section('title', 'Data Absensi Magang')
@push('head-css')
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/adminlte.min.css">

    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/summernote/summernote-bs4.min.css">
    <!-- CodeMirror -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/codemirror/codemirror.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/codemirror/theme/monokai.css">
    <!-- SimpleMDE -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/simplemde/simplemde.min.css">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

@endpush
@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Tambah Kegiatan Aktifitas Magang</h3>
        </div>
        <form method="post" action="/aktifitas-magang/post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Kegiatan</label>
                    <input type="text" class="form-control @error('nama_kegiatan') is-invalid @enderror" id="nama_kegiatan" name="nama_kegiatan" placeholder="Nama Kegiatan" value="">
                    @error('nama_kegiatan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tanggal Jam Mulai Aktifitas</label>
                    <input type="text" class="form-control @error('tanggal_mulai') is-invalid @enderror" id="tanggal_mulai" name="tanggal_mulai" placeholder="" value="{{ date('m/d/Y', strtotime(date('Y-m-d'))) }}" readonly>
                    @error('tanggal_mulai')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tanggal Jam Selesai Aktifitas</label>
                    <input type="text" class="form-control @error('tanggal_selesai') is-invalid @enderror" id="tanggal_selesai" name="tanggal_selesai" placeholder="" value="{{ date('m/d/Y', strtotime(date('Y-m-d'))) }}" readonly>
                    @error('tanggal_selesai')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Waktu Melakukan Kegiatan</label>
                    <input type="text" class="form-control @error('interval_aktifitas') is-invalid @enderror" id="interval_aktifitas" name="interval_aktifitas" placeholder="1 Menit, 2 Menit" value="">
                    @error('interval_aktifitas')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Volume Kegiatan</label>
                    <input type="text" class="form-control @error('volume_kegiatan') is-invalid @enderror" id="volume_kegiatan" name="volume_kegiatan" placeholder="Misal 1 Document, dll" value="">
                    @error('volume_kegiatan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Deskripsi Kegiatan</label>
                    <textarea id="summernote" name="keterangan_kegiatan" class="@error('keterangan_kegiatan') is-invalid @enderror">
                        Silahkan Memasukan Detail Deskripsi Kegiatan Anda
              </textarea>
                    @error('keterangan_kegiatan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Proses</button>
            </div>
        </form>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Data Kegiatan Hari ini</h3>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Nama Peserta</th>
                        <th>Kegiatan</th>
                        <th>Mulai</th>
                        <th>Selesai</th>
                        <th>Waktu</th>
                        <th>Volume</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($aktifitasMagangs as $aktifitasMagang)
                    <tr>
                        <td>{{ $aktifitasMagang->user->nama_lengkap }}<br></td>
                        <td>{{ $aktifitasMagang->nama_kegiatan }}</td>
                        <td>{{ date('d F y H:i:s', strtotime($aktifitasMagang->tanggal_mulai)) }}</td>
                        <td>{{ date('d F y H:i:s', strtotime($aktifitasMagang->tanggal_selesai)) }}</td>
                        <td><small class="badge badge-success"><i class="far fa-clock"></i> {{ $aktifitasMagang->interval_aktifitas }}</small></td>
                        <td>{{ $aktifitasMagang->volume_kegiatan }}</td>

                    </tr>
                    @endforeach


                    </tbody>
                </table>
            </div>
        </div>
    </div>




@endsection
@push('bottom-js')
    <script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

    <!-- bs-custom-file-input -->
    <script src="{{asset('template')}}/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

    <!-- AdminLTE App -->
    <script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>

    <!-- Summernote -->
    <script src="{{asset('template')}}/plugins/summernote/summernote-bs4.min.js"></script>
    <!-- CodeMirror -->
    <script src="{{asset('template')}}/plugins/codemirror/codemirror.js"></script>
    <script src="{{asset('template')}}/plugins/codemirror/mode/css/css.js"></script>
    <script src="{{asset('template')}}/plugins/codemirror/mode/xml/xml.js"></script>
    <script src="{{asset('template')}}/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>


    <script>
        $(function () {
            $('#tanggal_mulai').datepicker({
                uiLibrary: 'bootstrap4'
            });
            $('#tanggal_selesai').datepicker({
                uiLibrary: 'bootstrap4'
            });
            // Summernote
            $('#summernote').summernote()

            // CodeMirror
            CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
                mode: "htmlmixed",
                theme: "monokai"
            });


        })
    </script>
@endpush

