<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NilaiTrans;
use App\Models\Instansis;
use App\Models\User;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;

class NilaiTransController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $dataNilaiUsers = User::leftJoin('nilai_trans', 'nilai_trans.id_users', '=', 'users.id')
                ->leftJoin('magangs', 'magangs.id_users_magang', '=', 'users.id')
                ->leftJoin('instansis', 'instansis.id', '=', 'magangs.id_instansi')
                ->select(['nilai_trans.id as id_nilai_trans', 'users.id as idusers', 'users.nama_lengkap as nama_lengkap','nilai_trans.kedisiplinan',
                    'nilai_trans.sikap','nilai_trans.komunikasi','nilai_trans.kerapian','nilai_trans.kerjasama','nilai_trans.motivasi',
                    'nilai_trans.penguasaan_terhadap_pekerjaan','nilai_trans.hasil_pekerjaan',
                    'nilai_trans.ide_atau_gagasan','magangs.id_instansi','instansis.nama_instansi','magangs.tanggal_selesai_magang',
                    'nilai_trans.tanggung_jawab','nilai_trans.kejujuran','nilai_trans.rata_rata','nilai_trans.no_sertifikat'])
                ->where(function ($query){
                    if(auth()->user()->privileges == "USERS MAGANG") {
                        $query->where('nilai_trans.id_users', auth()->user()->id);
                    } else if(auth()->user()->privileges == "PEMBIMBING SEKOLAH") {
                        $query->where('magangs.id_pembimbing_sekolah', auth()->user()->id);
                    } else if(auth()->user()->privileges == "PEMBIMBING MAGANG") {
                        $query->where('magangs.id_users_pembimbing_magang', auth()->user()->id);
                    }
                })
                //->where('magangs.tanggal_selesai_magang', '>=', date('Y-m-d'))
                ->whereNotNull('magangs.id_users_magang')->orderBy('nilai_trans.id', "DESC")->get();
            return DataTables::of($dataNilaiUsers)
                ->addIndexColumn()
                ->filter(function ($instance) use ($request) {

                    if(!empty($request->get('nama_peserta'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['nama_lengkap'], $request->get('nama_peserta')) ? true : false;
                        });
                    }
                    if(!empty($request->get('search'))){
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            if (Str::contains(Str::lower($row['nama_lengkap']), Str::lower($request->get('search')))){
                                return true;
                            }

                            return false;
                        });
                    }
                    if(!empty($request->get('id_instansis'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['id_instansi'], $request->get('id_instansis')) ? true : false;
                        });
                    }

                })
                ->addColumn('user', function (User $user){
                    $status_magang = "";
                    if($user->tanggal_selesai_magang == date('Y-m-d')) {
                        $status_magang = '<small class="badge badge-warning">Hampir Selesai</small>';
                    } else if(date('Y-m-d') > $user->tanggal_selesai_magang) {
                        $status_magang = '<small class="badge badge-danger">Selesai</small>';
                    } else {
                        $status_magang = '<small class="badge badge-success">Aktif</small>';
                    }

                    return '<b>'.$user->nama_lengkap.'</b><br><small class="badge badge-primary" >'.$user->nama_instansi.'</small> '.$status_magang;
                })
                ->addColumn('action', function ($row){
                    return '<button type="button"
                    id="'.$row->id_nilai_trans.'"
                    id_users="'.$row->idusers.'"
                    nama_lengkap="'.$row->nama_lengkap.'"
                    no_sertifikat="'.$row->no_sertifikat.'"
                    kedisiplinan="'.$row->kedisiplinan.'"
                    sikap="'.$row->sikap.'"
                    komunikasi="'.$row->komunikasi.'"
                    kerapian="'.$row->kerapian.'"
                    kerjasama="'.$row->kerjasama.'"
                    motivasi="'.$row->motivasi.'"
                    penguasaan_terhadap_pekerjaan="'.$row->penguasaan_terhadap_pekerjaan.'"
                    ide_atau_gagasan="'.$row->ide_atau_gagasan.'"
                    hasil_pekerjaan="'.$row->hasil_pekerjaan.'"
                    tanggung_jawab="'.$row->tanggung_jawab.'"
                    kejujuran="'.$row->kejujuran.'"
                    class="btn btn-info click_pilih_nilai_magangs">Pilih</button>';
                })
                ->rawColumns(['action','user'])
                ->make(true);
        }
        return view('nilai_magang.v_nilai_magang',[
            'instansis' => Instansis::where('deleted', 0)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function set_nilai_with_ajax(Request $request){
        $detailNilaiTrans = NilaiTrans::where('id', $request->id_nilai_magangs)->where('id_users', $request->id_users)->get()->count();
        $action = "";
        if($detailNilaiTrans > 0) {
            $action = "update";
        } else {
            $action = "create";
            $set['nilai_trans']['id_users'] = $request->id_users;
        }

        $set['nilai_trans']['no_sertifikat'] = ($request->no_sertifikat != '') ? $request->no_sertifikat : "";;
        $set['nilai_trans']['kedisiplinan'] = ($request->kedisiplinan != '') ? $request->kedisiplinan : 0;;
        $set['nilai_trans']['sikap'] = ($request->sikap != '') ? $request->sikap : 0;;
        $set['nilai_trans']['komunikasi'] = ($request->komunikasi != '') ? $request->komunikasi : 0;;
        $set['nilai_trans']['kerapian'] = ($request->kerapian != '') ? $request->kerapian : 0;;
        $set['nilai_trans']['kerjasama'] = ($request->kerjasama != '') ? $request->kerjasama : 0;;
        $set['nilai_trans']['motivasi'] = ($request->motivasi != '') ? $request->motivasi : 0;;
        $set['nilai_trans']['penguasaan_terhadap_pekerjaan'] = ($request->penguasaan_terhadap_pekerjaan != '') ? $request->penguasaan_terhadap_pekerjaan : 0;;
        $set['nilai_trans']['hasil_pekerjaan'] = ($request->hasil_pekerjaan != '') ? $request->hasil_pekerjaan : 0;;
        $set['nilai_trans']['ide_atau_gagasan'] = ($request->ide_atau_gagasan != '') ? $request->ide_atau_gagasan : 0;
        $set['nilai_trans']['tanggung_jawab'] = ($request->tanggung_jawab != '') ? $request->tanggung_jawab : 0;;
        $set['nilai_trans']['kejujuran'] = ($request->kejujuran != '') ? $request->kejujuran : 0;;

        $sumTotalNilai = $set['nilai_trans']['kedisiplinan'] + $set['nilai_trans']['sikap'] + $set['nilai_trans']['komunikasi'] + $set['nilai_trans']['kerapian']
                            + $set['nilai_trans']['kerjasama'] + $set['nilai_trans']['motivasi'] + $set['nilai_trans']['penguasaan_terhadap_pekerjaan']
                            + $set['nilai_trans']['hasil_pekerjaan'] + $set['nilai_trans']['ide_atau_gagasan'] + $set['nilai_trans']['tanggung_jawab']
                            + $set['nilai_trans']['kejujuran'];
        $rata_rata = $sumTotalNilai / 11;
        $set['nilai_trans']['rata_rata'] = $rata_rata;

        if($action == "create") {
            $resultQueryNilaiTrans = NilaiTrans::create($set['nilai_trans']);
        } else {
            $resultQueryNilaiTrans = NilaiTrans::where('id', $request->id_nilai_magangs)->update($set['nilai_trans']);
        }
        if($resultQueryNilaiTrans) {
            return response()->json([
                'metaData' => [
                    "code" => 200,
                    "message" => "Sukses Memproses Nilai"
                ]
            ]);
        } else {
            return response()->json([
                'metaData' => [
                    "code" => 201,
                    "message" => "Gagal Memproses Nilai"
                ]
            ]);
        }



    }
}
