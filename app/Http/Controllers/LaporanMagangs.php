<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Models\Magangs;
class LaporanMagangs extends Controller
{
    public function index(){
        return view('laporan.data_magang.v_laporan_data_magang');
    }
    public function generate_pdf(Request $request) {
        $request->validate([
            'tanggal_mulai' => 'required|min:3',
            'tanggal_selesai' => 'required|min:3',
        ]);
        if(auth()->user()->privileges == "PEMBIMBING SEKOLAH") {
            $magans = Magangs::where('deleted', 0)->where('id_pembimbing_sekolah', auth()->user()->id)->where('tanggal_mulai_magang', '>=', date('Y-m-d', strtotime($request->tanggal_mulai)))->where('tanggal_mulai_magang', '<=', date('Y-m-d', strtotime($request->tanggal_selesai)))->get();
        } else if(auth()->user()->privileges == "PEMBIMBING MAGANG") {
            $magans = Magangs::where('deleted', 0)->where('id_users_pembimbing_magang', auth()->user()->id)->where('tanggal_mulai_magang', '>=', date('Y-m-d', strtotime($request->tanggal_mulai)))->where('tanggal_mulai_magang', '<=', date('Y-m-d', strtotime($request->tanggal_selesai)))->get();
        } else if(auth()->user()->privileges == "ADMIN") {
            $magans = Magangs::where('deleted', 0)->where('tanggal_mulai_magang', '>=', date('Y-m-d', strtotime($request->tanggal_mulai)))->where('tanggal_mulai_magang', '<=', date('Y-m-d', strtotime($request->tanggal_selesai)))->get();
        }
        $view = view('laporan.data_magang.cetak', [
            "detailMagans" => $magans,
            "tanggal_mulai" => $request->tanggal_mulai,
            "tanggal_selesai" => $request->tanggal_selesai
        ]);
        $html_content = $view->render();
        PDF::SetAuthor('Rorik Setya Budi');
        PDF::SetTitle('Laporan Data Sistem Informasi Manajemen Magang');
        PDF::SetSubject("Laporan Magangs");
        PDF::SetKeywords('sistem informasi manajemen magang');
        PDF::setPrintHeader(false);
        PDF::setPrintFooter(false);
        PDF::SetMargins(7, 7, 7, 7);
        PDF::SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

        PDF::AddPage('L', 'A4');
        PDF::SetFont('helvetica', null, 11);
        PDF::writeHTML($html_content, true, false, true, false, '');
        PDF::Output('data_magang.pdf');
    }
}
