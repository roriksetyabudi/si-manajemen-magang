@extends('template.v_template')
@section('title', 'Informasi Akun')
@push('head-css')
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template/')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template/')}}/dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('template/')}}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
@endpush
@section('content')
    <div class="container-fluid">
        <form method="post" action="/ubah-akun/post/{{ auth()->user()->id }}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                </symbol>
                <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
                </symbol>
                <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                </symbol>
            </svg>
            @if(session()->has('success'))
                <div class="alert alert-success d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                    <div>
                        {{ session('success') }}
                    </div>
                </div>

            @elseif(session()->has('fail'))
                <div class="alert alert-danger" role="alert">
                    {{ session('fail') }}
                </div>
            @endif
            <div class="row">

                <div class="col-md-3">
                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                @if($detailUsers->photo == 0)
                                    <img class="profile-user-img img-fluid img-circle"
                                         src="{{asset('template')}}/dist/img/avatar.png"
                                         alt="User profile picture">

                                @else
                                    <img class="profile-user-img img-fluid img-circle"
                                         src="{{asset('storage')}}/{{ $detailUsers->fileuploads->path }}"
                                         alt="User profile picture">
                                @endif

                            </div>

                            <h3 class="profile-username text-center">{{ $detailUsers->nama_lengkap }}</h3>
                            @if(auth()->user()->privileges == "USERS MAGANG")
                                <p class="text-muted text-center">{{ $detailUsers->magangs->instansis->nama_instansi }}</p>
                            @endif


                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>Aktifitas Magang</b> <a class="float-right">{{ $countAktifitasMagangs }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Izin Magang</b> <a class="float-right">{{ $countIzins }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Absensi Magang</b> <a class="float-right">{{ $countAbsens }}</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- About Me Box -->

                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header p-2">
                            Ubah Akun
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" id="nama_lengkap" name="nama_lengkap" value="{{ $detailUsers->nama_lengkap }}" placeholder="Nama Lengkap">
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Telepon</label>
                                        <input type="text" class="form-control @error('telepon') is-invalid @enderror" id="telepon" name="telepon" placeholder="Telepon" value="{{ $detailUsers->telepon }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <input type="text" class="form-control @error('alamat') is-invalid @enderror" placeholder="Alamat" id="alamat" name="alamat" value="{{ $detailUsers->alamat }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Jenis Kelamin</label>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input @error('gender') is-invalid @enderror" type="radio" name="gender" id="genderL" value="L" @if($detailUsers->gender == "L") checked @endif>
                                            <label class="form-check-label" for="inlineRadio1">Laki - Laki</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input @error('gender') is-invalid @enderror" type="radio" name="gender" id="genderP" value="P" @if($detailUsers->gender == "P") checked @endif>
                                            <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" id="email" value="{{ $detailUsers->email }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Privileges</label>
                                        <select class="form-control select2 @error('privileges') is-invalid @enderror" name="privileges" id="privileges" style="width: 100%;" @if($detailUsers->privileges != "ADMIN") readonly="" @endif>
                                            @if($detailUsers->privileges == "ADMIN")
                                                <option value="USERS MAGANG" @if($detailUsers->privileges == "USERS MAGANG") selected @endif>USERS MAGANG</option>
                                                <option value="PEMBIMBING SEKOLAH" @if($detailUsers->privileges == "PEMBIMBING SEKOLAH") selected @endif>PEMBIMBING SEKOLAH</option>
                                                <option value="PEMBIMBING MAGANG" @if($detailUsers->privileges == "PEMBIMBING MAGANG") selected @endif>PEMBIMBING MAGANG</option>
                                                <option value="ADMIN" @if($detailUsers->privileges == "ADMIN") selected @endif>ADMIN</option>
                                            @else
                                                <option value="{{ $detailUsers->privileges }}">{{ $detailUsers->privileges }}</option>
                                            @endif
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" id="username" placeholder="Username" value="{{ $detailUsers->username }}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Password Lama</label>
                                        <div class="input-group">
                                            <input type="password" class="form-control @error('password_lama') is-invalid @enderror" name="password_lama" id="password_lama" placeholder="Password Lama" value="">
                                            <div class="input-group-append">
                                                <span class="input-group-text" onclick="password_show_hide();">
                                                  <i class="fas fa-eye" id="show_eye"></i>
                                                  <i class="fas fa-eye-slash d-none" id="hide_eye"></i>
                                                </span>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label>Password Baru</label>
                                        <div class="input-group">
                                            <input type="password" class="form-control @error('password_baru') is-invalid @enderror" name="password_baru" id="password_baru" placeholder="Password Baru">
                                            <div class="input-group-append">
                                                <span class="input-group-text" onclick="password_show_hide_baru();">
                                                  <i class="fas fa-eye" id="show_eye_baru"></i>
                                                  <i class="fas fa-eye-slash d-none" id="hide_eye_baru"></i>
                                                </span>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label>Foto Peserta</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input @error('photo') is-invalid @enderror" id="photo" name="photo" onchange="previewImage()">
                                            @error('photo')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                            <label class="custom-file-label" for="exampleInputFile" required>Upload Foto</label>
                                        </div>
                                        <hr>
                                        @if($detailUsers->photo != 0)
                                            <img src="{{asset('storage/'.$detailUsers->fileuploads->path )}}" class="image-preview img-fluid col-sm-6">
                                        @else
                                            <img class="image-preview img-fluid col-sm-6">
                                        @endif

                                    </div>

                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn bg-gradient-primary">Proses</button>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->

            </div>
        </form>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
@endsection
@push('bottom-js')
    <!-- jQuery -->
    <script src="{{asset('template/')}}/plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('template/')}}/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template/')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="{{asset('template/')}}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template/')}}/dist/js/adminlte.js"></script>

    <script type="text/javascript">
        function previewImage(){
            const image = document.querySelector('#photo');
            const imagePreview = document.querySelector('.image-preview');

            imagePreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function (oFREvent) {
                imagePreview.src = oFREvent.target.result;
            }
        }

        function password_show_hide() {
            var x = document.getElementById("password_lama");
            var show_eye = document.getElementById("show_eye");
            var hide_eye = document.getElementById("hide_eye");
            hide_eye.classList.remove("d-none");
            if (x.type === "password") {
                x.type = "text";
                show_eye.style.display = "none";
                hide_eye.style.display = "block";
            } else {
                x.type = "password";
                show_eye.style.display = "block";
                hide_eye.style.display = "none";
            }
        }

        function password_show_hide_baru() {
            var x = document.getElementById("password_baru");
            var show_eye = document.getElementById("show_eye_baru");
            var hide_eye = document.getElementById("hide_eye_baru");
            hide_eye.classList.remove("d-none");
            if (x.type === "password") {
                x.type = "text";
                show_eye.style.display = "none";
                hide_eye.style.display = "block";
            } else {
                x.type = "password";
                show_eye.style.display = "block";
                hide_eye.style.display = "none";
            }
        }
    </script>
@endpush
