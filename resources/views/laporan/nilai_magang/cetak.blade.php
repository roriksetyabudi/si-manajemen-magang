<style>
    table.display thead th{
        font-size: 14px;text-align: center;font-weight: bold;
    }
    table.display {
        font-size:15px;
        border:black thin thin;
    }
</style>
<table class="table_data">
    <tr>
        <td align="centar" width="15%" class="header_logo" rowspan="4"><img src="{{public_path('template')}}/dist/img/pos-indonesia.jpg" width="75px"/></td>
        <td align="center" width="70%" class="header_title" style="letter-spacing: 3px;"><H2>PT Pos Indonesia (Persero) Madiun</H2></td>
        <td align="centar" width="15%" class="header_logo" rowspan="4"><H4></H4></td>
    </tr>
    <tr><td align="center" width="70%" class="header_title" colspan="3" style="letter-spacing: 2px;"><H3><b>Jalan Pahlawan Nomor 24 Madiun Kode Pos 63100</b></H3></td></tr>
    <tr><td align="center" width="70%" class="header_title" colspan="3" style="letter-spacing: 1px;"><H4>Website : <i>www.posindonesia.co.id</i>, Telepon : <i>0351-464454</i></H4></td></tr>

    <tr><td align="center" width="70%" class="header_title" colspan="3"></td></tr>
</table>
{{-- <h1 align="center">LAPORAN DATA NILAI PESERTA MAGANG<br>PERIODE GENERATE {{ date('d F Y', strtotime($tanggal_mulai)) }} s/d {{ date('d F Y', strtotime($tanggal_selesai)) }}</h1> --}}
<table width="100%" border="1" cellpadding="3" cellspacing="0">
    <thead>
    <tr style="background-color:#FF0000;color:#ffffff;">
        <td><b>NAMA</b></td>
        <td><b>DISIPLIN</b></td>
        <td><b>SIKAP</b></td>
        <td><b>KOMUNIKASI</b></td>
        <td><b>KERAPIAN</b></td>
        <td><b>KERJASAMA</b></td>
        <td><b>MOTIVASI</b></td>
        <td><b>P.PEKERJAAN</b></td>
        <td><b>H.PEKERJAAN</b></td>
        <td><b>IDE</b></td>
        <td><b>T.JAWAB</b></td>
        <td><b>JUJUR</b></td>
    </tr>
    </thead>

    @foreach($detailNilai as $value)
        <tr>
            <td> {{ $value->user->nama_lengkap }} </td>
            <td> {{ $value->kedisiplinan }} </td>
            <td> {{ $value->sikap }} </td>
            <td> {{ $value->komunikasi }} </td>
            <td> {{ $value->kerapian }} </td>
            <td> {{ $value->kerjasama }} </td>
            <td> {{ $value->motivasi }} </td>
            <td> {{ $value->penguasaan_terhadap_pekerjaan }} </td>
            <td> {{ $value->hasil_pekerjaan }} </td>
            <td> {{ $value->ide_atau_gagasan }} </td>
            <td> {{ $value->tanggung_jawab }} </td>
            <td> {{ $value->kejujuran }} </td>
        </tr>
    @endforeach
</table>
