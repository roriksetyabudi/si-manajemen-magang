<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Untitled Document</title>
    <style type="text/css">
        body {
            font-family: Arial, Helvetica, sans-serif;
        }
        .table_data {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 13px;
            height: auto;
            width: 100%;
            border: thin none #333;
            padding-top: 5px;
        }
        .header_table_data {
            width: 15%;
            height: auto;
            text-align: center;
            border: thin none #666;
        }
        .header_table_data_content {
            width: 85%;
            height: auto;
            text-align: center;
            vertical-align: top;
            border: thin none #666;
            line-height: 3px;
        }
        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-top-width: 2px;
            border-right-width: 2px;
            border-bottom-width: 2px;
            border-left-width: 2px;
            border-top-style: ridge;
            border-right-style: ridge;
            border-bottom-style: ridge;
            border-left-style: ridge;
        }
        .line_table {
            height: auto;
            width: 100%;
            border: thin none #000;
            padding-left: 5px;
        }
        .label_surat_keterangan_berobat {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 16px;
            font-weight: bold;
            color: #000;
            text-align: center;
            vertical-align: top;
            height: auto;
            width: 100%;
            text-decoration: underline;

        }
        .label_nomor_surat_keterangan_berobat {
            height: auto;
            width: 100%;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            text-align: center;
            vertical-align: top;
            padding-top: 10px;
        }

        .header_kepada {
            height: auto;
            width: 60%;
            border: thin none #333;
        }
        .footer_kepada {
            width: 40%;
            border: thin none #000;
        }
        .p_text {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            line-height: 16px;
        }
        .isi_label{
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            height: auto;
            width: 25%;
            padding-left: 25px;
            padding-top: 5px;
            padding-bottom: 5px;
            border: thin none #000;
        }
        .isi_content{
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            height: auto;
            width: 30%;
            padding-top: 5px;
            padding-bottom: 5px;
            border: thin none #000;
        }
        .table_isi_1 {
            height: auto;
            width: 100%;
            padding-top: 5px;
            padding-bottom: 5px;
        }
    </style>
</head>

<body>

<table class="table_data">
    <tr>
        <td align="centar" width="15%" class="header_logo" rowspan="4"><img src="{{public_path('template')}}/dist/img/pos-indonesia.jpg" width="75px"/></td>
        <td align="center" width="85%" class="header_title" style="letter-spacing: 3px;"><H2>PT Pos Indonesia (Persero) Madiun<br>NILAI MAGANG KERJA</H2></td>

        <td align="centar" width="0%" class="header_logo" rowspan="4"><H4></H4></td>
    </tr>
    <tr><td align="center" width="85%" class="header_title" colspan="3" style="letter-spacing: 2px;"><H5><b>Jalan Pahlawan Nomor 24 Madiun Kode Pos 63100</b></H5></td></tr>
    <tr><td align="center" width="85%" class="header_title" colspan="3" style="letter-spacing: 1px;"><H6>Website : <i>www.posindonesia.co.id</i>, Telepon : <i>0351-464454</i></H6></td></tr>

    <tr><td align="center" width="85%" class="header_title" colspan="3"></td></tr>
</table>

{{-- <table class="table_data">
    <tr>
        <td align="centar" width="5%" class="header_logo" rowspan="4"></td>
        <td align="center" width="90%" class="header_title" style="letter-spacing: 3px;"><H3>FORM PENILAIAN LAPORAN MAGANG KERJA</H3></td>
        <td align="centar" width="5%" class="header_logo" rowspan="4"></td>
    </tr>
    <tr><td align="center" width="90%" class="header_title" colspan="3" style="letter-spacing: 2px;"><H2><b>POLITEKNIK NEGERI MADIUN</b></H2></td></tr>
    <tr><td align="center" width="90%" class="header_title" colspan="3" style="letter-spacing: 2px;"><H2><b>TEKNOLOGI INFORMASI</b></H2></td></tr>
</table> --}}
<hr/>
<table class="table_isi_1">
    <tr>
        <td class="isi_label">Nama Mahasiswa</td>
        <td class="isi_content">: <b>{{ $detailNilai->user->nama_lengkap }}</b></td>
        <td class="isi_label">&nbsp;</td>
        <td class="isi_content">&nbsp;</td>
    </tr>
    <tr>
        <td class="isi_label">Jenis Kelamin</td>
        <td class="isi_content">: @if($detailNilai->user->gender == "L") Laki-Laki @else Perempuan @endif</td>
        <td class="isi_label">&nbsp;</td>
        <td class="isi_content">&nbsp;</td>
    </tr>
    <tr>
        <td class="isi_label">Nama Instansi</td>
        <td class="isi_content">: {{ $detailNilai->magangs->instansis->nama_instansi }}</td>
        <td class="isi_label">&nbsp;</td>
        <td class="isi_content">&nbsp;</td>
    </tr>
    <tr>
        <td class="isi_label">Alamat Instansi</td>
        <td class="isi_content">: {{ $detailNilai->magangs->instansis->alamat_instansi }}</td>
        <td class="isi_label">&nbsp;</td>
        <td class="isi_content">&nbsp;</td>
    </tr>
    <tr>
        <td class="isi_label">Pembimbing Magang</td>
        <td class="isi_content">: {{ $detailNilai->magangs->usersPembimbingmagang->nama_lengkap }}</td>
        <td class="isi_label">&nbsp;</td>
        <td class="isi_content">&nbsp;</td>
    </tr>
    <tr>
        <td class="isi_label">Pembimbing Instansi</td>
        <td class="isi_content">: {{ $detailNilai->magangs->usersPembimbingSekolah->nama_lengkap }}</td>
        <td class="isi_label">&nbsp;</td>
        <td class="isi_content">&nbsp;</td>
    </tr>
    <tr>
        <td class="isi_label">Nilai Rata Rata</td>
        <td class="isi_content">: <b>{{ $detailNilai->rata_rata }}</b></td>
        <td class="isi_label">&nbsp;</td>
        <td class="isi_content">&nbsp;</td>
    </tr>

</table>

<table class="table_data">
    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"><b></b></td>
    </tr>

</table>

<table width="100%" border="1" cellpadding="3" cellspacing="0">
    <thead>
    <tr style="background-color:#c4c1c1;color:#090808;">
        <td width="10%" align="center"><b>NO.</b></td>
        <td width="40%" align="center"><b>ASPEK PENILAIAN</b></td>
        <td width="20%" align="center"><b>NILAI (1 - 100)</b></td>
        <td width="30%" align="center"><b>KET</b></td>
    </tr>
    </thead>
        <tr>
            <td width="10%" align="center">1</td>
            <td width="40%">Kedisiplinan</td>
            <td width="20%" align="center">{{ empty($detailNilai->kedisiplinan) ? 0 : $detailNilai->kedisiplinan }}</td>
            <td width="30%" align="center">{{ ($detailNilai->kedisiplinan >= 90) ? "Baik" : "Cukup" }}</td>
        </tr>
    <tr>
        <td width="10%" align="center">2</td>
        <td width="40%">Sikap</td>
        <td width="20%" align="center">{{ empty($detailNilai->sikap) ? 0 : $detailNilai->sikap }}</td>
        <td width="30%" align="center">{{ ($detailNilai->sikap >= 90) ? "Baik" : "Cukup" }}</td>
    </tr>
    <tr>
        <td width="10%" align="center">3</td>
        <td width="40%">Komunikasi</td>
        <td width="20%" align="center">{{ empty($detailNilai->komunikasi) ? 0 : $detailNilai->komunikasi }}</td>
        <td width="30%" align="center">{{ ($detailNilai->komunikasi >= 90) ? "Baik" : "Cukup" }}</td>
    </tr>
    <tr>
        <td width="10%" align="center">4</td>
        <td width="40%">Kerapian</td>
        <td width="20%" align="center">{{ empty($detailNilai->kerapian) ? 0 : $detailNilai->kerapian }}</td>
        <td width="30%" align="center">{{ ($detailNilai->kerapian >= 90) ? "Baik" : "Cukup" }}</td>
    </tr>
    <tr>
        <td width="10%" align="center">5</td>
        <td width="40%">Kerjasama</td>
        <td width="20%" align="center">{{ empty($detailNilai->kerjasama) ? 0 : $detailNilai->kerjasama }}</td>
        <td width="30%" align="center">{{ ($detailNilai->kerjasama >= 90) ? "Baik" : "Cukup" }}</td>
    </tr>
    <tr>
        <td width="10%" align="center">6</td>
        <td width="40%">Motivasi</td>
        <td width="20%" align="center">{{ empty($detailNilai->motivasi) ? 0 : $detailNilai->motivasi  }}</td>
        <td width="30%" align="center">{{ ($detailNilai->motivasi >= 90) ? "Baik" : "Cukup" }}</td>
    </tr>
    <tr>
        <td width="10%" align="center">7</td>
        <td width="40%">Penguasaan Terhadap Pekerjaan</td>
        <td width="20%" align="center">{{ empty($detailNilai->penguasaan_terhadap_pekerjaan) ? 0 : $detailNilai->penguasaan_terhadap_pekerjaan }}</td>
        <td width="30%" align="center">{{ ($detailNilai->penguasaan_terhadap_pekerjaan >= 90) ? "Baik" : "Cukup" }}</td>
    </tr>
    <tr>
        <td width="10%" align="center">8</td>
        <td width="40%">Hasil Pekerjaan</td>
        <td width="20%" align="center">{{ empty($detailNilai->hasil_pekerjaan) ? 0 : $detailNilai->hasil_pekerjaan }}</td>
        <td width="30%" align="center">{{ ($detailNilai->hasil_pekerjaan >= 90) ? "Baik" : "Cukup" }}</td>
    </tr>
    <tr>
        <td width="10%" align="center">9</td>
        <td width="40%">Ide Atau Gagasan</td>
        <td width="20%" align="center">{{ empty($detailNilai->ide_atau_gagasan) ? 0 : $detailNilai->ide_atau_gagasan }}</td>
        <td width="30%" align="center">{{ ($detailNilai->ide_atau_gagasan >= 90) ? "Baik" : "Cukup" }}</td>
    </tr>
    <tr>
        <td width="10%" align="center">10</td>
        <td width="40%">Tanggung Jawab</td>
        <td width="20%" align="center">{{ empty($detailNilai->tanggung_jawab) ? 0 : $detailNilai->tanggung_jawab }}</td>
        <td width="30%" align="center">{{ ($detailNilai->tanggung_jawab >= 90) ? "Baik" : "Cukup" }}</td>
    </tr>
    <tr>
        <td width="10%" align="center">11</td>
        <td width="40%">Kejujuran</td>
        <td width="20%" align="center">{{ empty($detailNilai->kejujuran) ? 0 : $detailNilai->kejujuran }}</td>
        <td width="30%" align="center">{{ ($detailNilai->kejujuran >= 90) ? "Baik" : "Cukup" }}</td>
    </tr>
</table>
<table class="table_data">
    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"><b></b></td>
    </tr>

</table>
<table class="table_data">

    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;">Pembimbing Magang</td>
    </tr>
    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"></td>
    </tr>
    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"></td>
    </tr>
    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"></td>
    </tr>

    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"><b><u>{{ $detailNilai->magangs->usersPembimbingmagang->nama_lengkap }}</u><br>Nip Pos : {{ $detailNilai->magangs->usersPembimbingmagang->nippos }}</b></td>
    </tr>
</table>
</body>
</html>
