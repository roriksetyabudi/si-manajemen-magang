@extends('template.v_template')
@section('title', 'Data Pendaftaran Magang')
@push('head-css')
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/adminlte.min.css">

@endpush
@section('content')
    <div class="card">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4>Data Pendaftaran Magang</h4>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session()->has('success'))
                <div class="alert alert-success d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                    <div>
                        {{ session('success') }}
                    </div>
                </div>
            @endif
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr class="bg-info">
                    <th>Nama</th>
                    <th>Instansi</th>
                    <th>Jurusan</th>
                    <th>Contact</th>
                    <th>Berkas</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pendaftaranMagans as $pendaftaranMagan)
                    <tr>
                        <td>
                            <p><i class="fas fa-user mr-1"></i> {{ $pendaftaranMagan->nama_lengkap_peserta_magang }}</p>
                            <p class="fw-lighter">{{ $pendaftaranMagan->alamat_lengkap_peserta_magang }}</p><span class="badge bg-info float-left">{{ $pendaftaranMagan->status }}</span>
                        </td>
                        <td><b>{{ $pendaftaranMagan->nama_instansi }}</b><br>
                            <p class="fw-lighter">{{ $pendaftaranMagan->alamat_lengkap_instansi }}</p></td>
                        <td>{{ $pendaftaranMagan->bidang_keahlian_peserta_magang }}</td>

                        <td>
                            <p><i class="fas fa-phone mr-1"></i> {{ $pendaftaranMagan->telepon_peserta_magang }}</p>
                            <p><i class="fas fa-envelope mr-1"></i> {{ $pendaftaranMagan->email_peserta_magang }}</p>
                        </td>
                        <td>
                            <a href="{{ asset('storage/'.$pendaftaranMagan->fileuploads->path) }}" target="_blank"><p><i class="fas fa-paperclip mr-1"></i>{{ $pendaftaranMagan->fileuploads->title}} </p></a>
                        </td>
                        <td class="text-center">
                            <a href="/pendaftaran-magang/{{ $pendaftaranMagan->id }}/edit" class="btn btn-outline-info btn-sm">Detail</a>
                        </td>
                    </tr>
                @endforeach

                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection
@push('bottom-js')
    <script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{asset('template')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/jszip/jszip.min.js"></script>
    <script src="{{asset('template')}}/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{asset('template')}}/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endpush
