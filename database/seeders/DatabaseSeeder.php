<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\FileUploads;
use App\Models\MagangRegistration;
use App\Models\Absens;
use App\Models\AktifitasMagangs;
use App\Models\Izins;
use App\Models\Instansis;
use App\Models\GeneralSettings;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::create([
            'username' => "admin",
            'password' => bcrypt('123456'),
            'nama_lengkap' => 'Administrator',
            'alamat' => 'Jl. Raya Melati Punung',
            'gender' => 'L',
            'email' => 'roriksetya@gmail.com',
            'telepon' => '087751948827',
            'photo' => 0,
            'privileges' => 'ADMIN',
            'pending' => 1,
            'disabled' => 1,
            'kode_verifikasi' => 123456,
            'kode_verifikasi_at' => now(),
            'deleted' => 0
        ]);
        User::create([
            'username' => "usermagang",
            'password' => bcrypt('123456'),
            'nama_lengkap' => 'Tester User Magang',
            'alamat' => 'Jl. Raya Melati Punung',
            'gender' => 'L',
            'email' => 'roriksbb@gmail.com',
            'telepon' => '085932114771',
            'photo' => 0,
            'privileges' => 'USERS MAGANG',
            'pending' => 1,
            'disabled' => 1,
            'kode_verifikasi' => 741852,
            'kode_verifikasi_at' => now(),
            'deleted' => 0
        ]);
        User::create([
            'username' => "pembimbingsekolah",
            'password' => bcrypt('123456'),
            'nama_lengkap' => 'Tester Pembimbing Sekolah',
            'alamat' => 'Jl. Raya Melati Punung',
            'gender' => 'L',
            'email' => 'kahyangdev@gmail.com',
            'telepon' => '085932114772',
            'photo' => 0,
            'privileges' => 'PEMBIMBING SEKOLAH',
            'pending' => 1,
            'disabled' => 1,
            'kode_verifikasi' => 963852,
            'kode_verifikasi_at' => now(),
            'deleted' => 0
        ]);
        User::create([
            'username' => "pembimbingmagang",
            'password' => bcrypt('123456'),
            'nama_lengkap' => 'Tester Pembimbing Magang',
            'alamat' => 'Jl. Raya Melati Punung',
            'gender' => 'L',
            'email' => 'rorikdocument@gmail.com',
            'telepon' => '085932114773',
            'photo' => 0,
            'privileges' => 'PEMBIMBING MAGANG',
            'pending' => 1,
            'disabled' => 1,
            'kode_verifikasi' => 258639,
            'kode_verifikasi_at' => now(),
            'deleted' => 0
        ]);

//        FileUploads::create([
//           'title' => 'Image Default',
//            'filename' => 'avatar.png',
//            'type' => 'image/png',
//            'size' => '100.7'
//        ]);

//        MagangRegistration::create([
//           'nama_lengkap_peserta_magang' => 'Rorik Setya Budi',
//            'alamat_lengkap_peserta_magang' => 'Arjosari Mlati',
//            'jenis_kelamin_peserta_magang' => 'L',
//            'telepon_peserta_magang' => '085932114771',
//            'email_peserta_magang' => 'kahyangsite@gmail.com',
//            'fupload_berkas_peserta_magang' => 'file-uploads/documents/oJdO22C4fXckmRaV6sj1vcEPWLnmi6aXguomPgT0.pdf',
//            'fupload_berkas_foto_peserta_magang' => 'file-uploads/images/d3eoJSS5TvCZ1ove7lHHu55QCWVmRddvDCKYUa6i.png',
//            'nama_instansi' => 'RSUD dr. Darsono',
//            'alamat_lengkap_instansi' => 'JL. Ahmad Yani 50A Pacitan',
//            'telepon_instansi' => '03578859',
//            'bidang_keahlian_peserta_magang' => 'Rekayasa Perangkat Lunak',
//            'nama_pembimbing_sekolah_peserta_magang' => 'Setyobudi',
//            'telepon_pembimbing_sekolah_peserta_magang' => '087751948827',
//            'email_pembimbing_sekolah_peserta_magang' => 'roriksb@gmail.com',
//            'deleted' => 0
//        ]);

        Absens::create([
           'id_users' => 2,
            'tanggal_masuk' => '2022-03-24 08:00:00',
            'status' => 'Masuk',
            'file_bukti_absensi' => 0,
            'deleted' => 0
        ]);
        Absens::create([
            'id_users' => 2,
            'tanggal_masuk' => '2022-03-24 08:00:00',
            'tanggal_pulang' => '2022-03-24 15:00:00',
            'status' => 'Pulang',
            'file_bukti_absensi' => 0,
            'deleted' => 0
        ]);
        Absens::create([
            'id_users' => 2,
            'tanggal_masuk' => '2022-03-25 08:00:00',
            'status' => 'Masuk',
            'file_bukti_absensi' => 0,
            'deleted' => 0
        ]);
        Absens::create([
            'id_users' => 2,
            'tanggal_masuk' => '2022-03-25 08:00:00',
            'tanggal_pulang' => '2022-03-25 15:00:00',
            'status' => 'Pulang',
            'file_bukti_absensi' => 0,
            'deleted' => 0
        ]);
        Absens::create([
            'id_users' => 2,
            'tanggal_masuk' => '2022-03-26 08:00:00',
            'status' => 'Masuk',
            'file_bukti_absensi' => 0,
            'deleted' => 0
        ]);

        AktifitasMagangs::create([
           'id_users' => 2,
            'nama_kegiatan' => 'Menulis Surat',
            'tanggal_mulai' => '2022-03-24 07:30:56',
            'tanggal_selesai' => '2022-03-24 08:00:00',
            'interval_aktifitas' => '30 Menit',
            'volume_kegiatan' => 'Document',
            'keterangan_kegiatan' => 'Menulis surat',
            'deleted' => 0
        ]);
        AktifitasMagangs::create([
            'id_users' => 2,
            'nama_kegiatan' => 'Mengantarkan Surat Ke Luar',
            'tanggal_mulai' => '2022-03-24 08:37:56',
            'tanggal_selesai' => '2022-03-24 09:30:00',
            'interval_aktifitas' => '60 Menit',
            'volume_kegiatan' => 'Paket',
            'keterangan_kegiatan' => 'Mengantar Surat',
            'deleted' => 0
        ]);
        AktifitasMagangs::create([
            'id_users' => 2,
            'nama_kegiatan' => 'Mengantarkan Surat Ke Beberapa Kantor',
            'tanggal_mulai' => '2022-03-24 09:37:56',
            'tanggal_selesai' => '2022-03-24 10:30:00',
            'interval_aktifitas' => '60 Menit',
            'volume_kegiatan' => 'Paket',
            'keterangan_kegiatan' => 'Dinas A, Dinas B, Dinas C',
            'deleted' => 0
        ]);
        AktifitasMagangs::create([
            'id_users' => 2,
            'nama_kegiatan' => 'Rapat kerja dengan pimpinan',
            'tanggal_mulai' => '2022-03-24 12:00:56',
            'tanggal_selesai' => '2022-03-24 13:30:00',
            'interval_aktifitas' => '90 Menit',
            'volume_kegiatan' => 'Paket',
            'keterangan_kegiatan' => 'Rapat Rutin Bulanan',
            'deleted' => 0
        ]);
        AktifitasMagangs::create([
            'id_users' => 2,
            'nama_kegiatan' => 'Melakukan verifikasi surat masuk',
            'tanggal_mulai' => '2022-03-24 07:30:56',
            'tanggal_selesai' => '2022-03-24 07:45:00',
            'interval_aktifitas' => '15 Menit',
            'volume_kegiatan' => 'Document',
            'keterangan_kegiatan' => 'Melakukan verifikasi surat masuk',
            'deleted' => 0
        ]);
        $tanggal_izin1 = array(
            "2022-03-24","2022-03-25","2022-04-26","2022-03-27");
        $tanggal_izin2 = array(
            "2022-03-28"
        );
        $tanggal_izin3 = array(
            "2022-03-29","2022-03-30"
        );
//        Izins::create([
//            'id_users' => 2,
//            'tanggal_izin' => json_encode($tanggal_izin1),
//            'alasan_izin' => 'Membantu Orang Tua Dirumah',
//            'file_bukti_izin' => 0,
//            'id_users_approval' => 1,
//            'is_approval' => 'Y',
//            'is_approval_at' => '2022-03-24 13:45:10',
//            'keterangan_approval' => 'Harap datang tepat waktu jika selesai ijin',
//            'status' => 'Disetujui',
//            'deleted' => 0
//        ]);
//        Izins::create([
//            'id_users' => 2,
//            'tanggal_izin' => json_encode($tanggal_izin2),
//            'alasan_izin' => 'Jalan Jalan',
//            'file_bukti_izin' => 0,
//            'id_users_approval' => 0,
//            'is_approval' => 'Y',
//            'is_approval_at' => '2022-03-24 13:45:10',
//            'keterangan_approval' => 'Tidak Boleh Izin Lagi',
//            'status' => 'Ditolak',
//            'deleted' => 0
//        ]);
//        Izins::create([
//            'id_users' => 2,
//            'tanggal_izin' => json_encode($tanggal_izin3),
//            'alasan_izin' => 'Belanja Ke Jogja',
//            'file_bukti_izin' => 0,
//            'id_users_approval' => 0,
//            'is_approval' => 'N',
//            'is_approval_at' => null,
//            'keterangan_approval' => '',
//            'status' => 'Pengajuan',
//            'deleted' => 0
//        ]);

        Instansis::create([
            'nama_instansi' => "RSUD dr. Darsono",
            'alamat_instansi' => "Jl Ahmad Yani 50A Pacitan",
            'telepon_instansi' => "03578859"
        ]);
        Instansis::create([
            'nama_instansi' => "KPRI Among Husada Sejaahtera",
            'alamat_instansi' => "Jl Ahmad Yani 50A Pacitan",
            'telepon_instansi' => "03578829"
        ]);
        Instansis::create([
            'nama_instansi' => "SMKN 2 Pacitan",
            'alamat_instansi' => "JL. Dewi Sartika 50B Sidoharjo Pacitan",
            'telepon_instansi' => "087751948827"
        ]);

        GeneralSettings::create([
           'name' => 'Sistem Informasi Manajemen Magang',
            'description' => 'SIstem Informasi Manajemen Magang Adalah xx',
            'kuota_magang' => 10
        ]);

    }

}
