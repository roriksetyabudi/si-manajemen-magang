<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username', 50)->unique();
            $table->string('password');
            $table->string('reset_hash', 50)->nullable(true);
            $table->string('nippos', 50)->comment('Nip Pegawai Pos')->nullable(true);
            $table->string('nama_lengkap', 50)->comment('Nama lengkap users');
            $table->text('alamat');
            $table->string('gender', 2)->comment("L=Laki-Laki, P=Perempuan");
            $table->string('email', 50)->unique();
            $table->string('telepon', 20)->unique();
            $table->smallInteger("photo")->default(0);
            $table->enum('privileges', ['USERS MAGANG','PEMBIMBING SEKOLAH','PEMBIMBING MAGANG','ADMIN'])->default("USERS MAGANG");
            $table->enum('is_peserta_magang', ['Y','N'])->default("N");
            $table->tinyInteger("pending")->default(0)->comment("0=Status Pending, 1=Approval");
            $table->tinyInteger("disabled")->default(0)->comment("0=disabled/blokir/non aktif, 1=aktif");
            $table->string("kode_verifikasi", 50)->comment("didapat dari verifikasi pendaftaran magang/dari pendaftaran magang")->nullable(true);
            $table->dateTime("kode_verifikasi_at")->nullable(true);
            $table->tinyInteger("deleted")->default(0)->comment("0=Not Deleted, 1=deleted");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
