<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MagangRegistration extends Model
{
    use HasFactory;

    protected $table = "magang_registrations";
    protected $guarded = ['id'];

    public function fileuploads(){
        return $this->belongsTo(FileUploads::class, 'fupload_berkas_peserta_magang', 'id');
    }
    public function fileuploadsPhotoPendaftaranMagang(){
        return $this->belongsTo(FileUploads::class, 'fupload_berkas_foto_peserta_magang', 'id');
    }
}
