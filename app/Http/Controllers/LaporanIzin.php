<?php

namespace App\Http\Controllers;

use App\Models\Absens;
use Illuminate\Http\Request;
use PDF;
use App\Models\Izins;
class LaporanIzin extends Controller
{
    public function index(){
        return view('laporan.izin_magang.v_laporan_izin_magang');
    }
    public function generate_pdf(Request $request) {
        $request->validate([
            'tanggal_mulai' => 'required|min:3',
            'tanggal_selesai' => 'required|min:3',
        ]);
        if(auth()->user()->privileges == "PEMBIMBING SEKOLAH") {
            $izinMagangs = Izins::leftJoin('magangs', 'magangs.id_users_magang', '=', 'izins.id_users')
                ->leftJoin('users', 'izins.id_users', '=', 'users.id')
                ->select(['izins.id as id_izin','izins.id_users','izins.tanggal_izin','izins.tanggal_selesai_izin','izins.alasan_izin','izins.file_bukti_izin','izins.id_users_approval',
                    'izins.is_approval','izins.is_approval_at','izins.keterangan_approval','izins.status','izins.deleted','izins.created_at',
                    'users.nama_lengkap'])
                ->whereNotNull('magangs.id_users_magang')
                ->where('id_pembimbing_sekolah', auth()->user()->id)
                ->whereDate('izins.tanggal_izin', '>=', date('Y-m-d', strtotime($request->post('tanggal_mulai'))))
                ->whereDate('izins.tanggal_izin', '<=', date('Y-m-d', strtotime($request->post('tanggal_selesai'))))->get();
        } else if(auth()->user()->privileges == "PEMBIMBING MAGANG") {
            $izinMagangs = Izins::leftJoin('magangs', 'magangs.id_users_magang', '=', 'izins.id_users')
                ->leftJoin('users', 'izins.id_users', '=', 'users.id')
                ->select(['izins.id as id_izin','izins.id_users','izins.tanggal_izin','izins.tanggal_selesai_izin','izins.alasan_izin','izins.file_bukti_izin','izins.id_users_approval',
                    'izins.is_approval','izins.is_approval_at','izins.keterangan_approval','izins.status','izins.deleted','izins.created_at',
                    'users.nama_lengkap'])
                ->whereNotNull('magangs.id_users_magang')
                ->where('id_users_pembimbing_magang', auth()->user()->id)
                ->whereDate('izins.tanggal_izin', '>=', date('Y-m-d', strtotime($request->post('tanggal_mulai'))))
                ->whereDate('izins.tanggal_izin', '<=', date('Y-m-d', strtotime($request->post('tanggal_selesai'))))->get();
        } else if(auth()->user()->privileges == "ADMIN") {
            $izinMagangs = Izins::leftJoin('magangs', 'magangs.id_users_magang', '=', 'izins.id_users')
                ->leftJoin('users', 'izins.id_users', '=', 'users.id')
                ->select(['izins.id as id_izin','izins.id_users','izins.tanggal_izin','izins.tanggal_selesai_izin','izins.alasan_izin','izins.file_bukti_izin','izins.id_users_approval',
                    'izins.is_approval','izins.is_approval_at','izins.keterangan_approval','izins.status','izins.deleted','izins.created_at',
                    'users.nama_lengkap'])
                ->whereNotNull('magangs.id_users_magang')
                ->whereDate('izins.tanggal_izin', '>=', date('Y-m-d', strtotime($request->post('tanggal_mulai'))))
                ->whereDate('izins.tanggal_izin', '<=', date('Y-m-d', strtotime($request->post('tanggal_selesai'))))->get();
        }
        $view = view('laporan.izin_magang.cetak', [
            "detailIzin" => $izinMagangs,
            "tanggal_mulai" => $request->tanggal_mulai,
            "tanggal_selesai" => $request->tanggal_selesai
        ]);
        $html_content = $view->render();
        PDF::SetAuthor('Rorik Setya Budi');
        PDF::SetTitle('Laporan Data Sistem Informasi Manajemen Magang');
        PDF::SetSubject("Laporan Izin Magang");
        PDF::SetKeywords('sistem informasi manajemen magang');
        PDF::setPrintHeader(false);
        PDF::setPrintFooter(false);
        PDF::SetMargins(7, 7, 7, 7);
        PDF::SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

        PDF::AddPage('L', 'A4');
        PDF::SetFont('helvetica', null, 11);
        PDF::writeHTML($html_content, true, false, true, false, '');
        PDF::Output('data_izins_magang.pdf');
    }
}
