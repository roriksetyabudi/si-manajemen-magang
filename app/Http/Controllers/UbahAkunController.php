<?php

namespace App\Http\Controllers;

use App\Models\FileUploads;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\AktifitasMagangs;
use App\Models\Izins;
use App\Models\Absens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UbahAkunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $detailUsers = User::find(auth()->user()->id);
        $countAktifitasMagangs = AktifitasMagangs::where('id_users', auth()->user()->id)->where('deleted', 0)->get()->count();
        $countIzins = Izins::where('id_users', auth()->user()->id)->where('deleted', 0)->get()->count();
        $countAbsens = Absens::where('id_users', auth()->user()->id)->where('deleted', 0)->get()->count();
        return view('akun.v_akun',[
            "id_users" => auth()->user()->id,
            "privileges" => auth()->user()->privileges,
            "detailUsers" => $detailUsers,
            "countAktifitasMagangs" => $countAktifitasMagangs,
            "countIzins" => $countIzins,
            "countAbsens" => $countAbsens,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_lengkap' => 'required|min:3',
            'telepon' => 'required|min:3',
            'alamat' => 'required',
            'gender' => 'required',
            'privileges' => 'required',
            'email' => 'required',
        ]);
        //detail users
        $detailusers = User::find($id);



        if($detailusers->photo != 0) {
            $detailFileUploads = FileUploads::find($detailusers->photo);
            if($request->file('photo')) {
                //delete image old
                Storage::delete($detailFileUploads->path);
                //FILE UPLOAD IMAGES PHOTO USER
                $update['file_uploads']['title'] = "Photo Users ". $request->nama_lengkap;
                $update['file_uploads']['filename'] = $request->file('photo')->hashName();
                $update['file_uploads']['path'] = $request->file('photo')->store('file-uploads/images');
                $update['file_uploads']['type'] = $request->file('photo')->extension();
                $update['file_uploads']['size'] = $request->file('photo')->getSize();
                FileUploads::where('id', $detailusers->photo)->update($update['file_uploads']);
                $update['users']['photo'] = $detailusers->photo;
            }
        }

        if($request->password_baru) {
            if(Auth::attempt(['username' => $request->username, 'password' => $request->password_lama])) {
                $request->validate([
                    'password_baru' => 'required|min:3',
                ]);
            } else {
                return redirect('/ubah-akun')->with('fail', 'Password Lama Tidak Sesuai Dengan Password Saat ini');
            }

        }
        $update['users']['password'] = bcrypt($request->password_baru);
        //UPDATE USERS
        $update['users']['nama_lengkap'] = $request->nama_lengkap;
        $update['users']['alamat'] = $request->alamat;
        $update['users']['gender'] = $request->gender;
        $update['users']['email'] = $request->email;
        $update['users']['telepon'] = $request->telepon;
        $update['users']['privileges'] = $request->privileges;

        User::where('id', $detailusers->id)->update($update['users']);
        return redirect('/ubah-akun')->with('success', 'Sukses Melakukan Update Akun');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
