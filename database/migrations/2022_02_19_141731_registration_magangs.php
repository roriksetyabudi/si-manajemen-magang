<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegistrationMagangs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magangs', function (Blueprint $table) {
            $table->id();
            $table->string("kode_pendaftaran_magang", 50)->unique()->comment("digunakan sebagao kode verifikasi");
            $table->smallInteger("id_users_magang")->index();
            $table->smallInteger("id_instansi")->index();
            $table->smallInteger("id_users_pembimbing_magang")->index();
            $table->smallInteger("photo")->default(0)->index();
            $table->smallInteger("file_berkas")->default(0)->index();
            $table->smallInteger("file_berkas_tugas_akhir_magang")->default(0)->index();
            $table->string("jurusan_bidang_keahlihan", 50)->nullable(true);
            $table->smallInteger("id_pembimbing_sekolah")->index();
            $table->date("tanggal_mulai_magang")->comment("rencana tanggal mulai magang");
            $table->date("tanggal_selesai_magang")->comment("rencana tanggal selesai magang");
            $table->enum("is_approval", ["Y","N"])->comment("Y=Approval, N=Not Approval")->default("N");
            $table->dateTime("is_approval_at");
            $table->text("keterangan")->nullable(true);
            $table->string("user_approval", 50)->index();
            $table->enum("status", ["MAGANG"])->default("MAGANG");
            $table->tinyInteger("deleted")->default(0)->comment("0=Not Deleted, 1=deleted");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('magangs');
    }
}
