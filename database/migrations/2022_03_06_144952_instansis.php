<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Instansis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instansis', function (Blueprint $table) {
            $table->id();
            $table->string("nama_instansi", 50)->nullable(true);
            $table->string("alamat_instansi")->nullable(true);
            $table->string("telepon_instansi", 25)->nullable(true);
            $table->tinyInteger("deleted")->default(0)->comment("0=Not Deleted, 1=deleted");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instansis');
    }
}
