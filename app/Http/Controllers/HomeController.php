<?php

namespace App\Http\Controllers;

use App\Models\Izins;
use App\Models\Magangs;
use Illuminate\Http\Request;
use App\Models\Absens;
use App\Models\AktifitasMagangs;
use App\Models\MagangRegistration;
use App\Models\User;
use App\Models\PemberianTugas;


class HomeController extends Controller
{
    public function index(){

        //check aktif users
//        $detailUsers = User::find(auth()->user()->id);
//        if($detailUsers->deleted == "1") {
//            return redirect('/logout')->with('loginError', 'Login Gagal, Users Deleted');
//
//
//        }
        if(auth()->user()->privileges == "ADMIN") {
            $absens = Absens::where('deleted', 0)->orderBy('id', "DESC")->take(5)->get();
            $aktifitasMagangs = AktifitasMagangs::where('deleted', 0)->orderBy('id')->take(5)->get();
            $dataIzinMagangs = Izins::where('deleted', 0)->orderBy('id')->take(5)->get();
            $pemberianTugas = PemberianTugas::where('id_users_diberi_tugas', auth()->user()->id)->orderBy('id', 'DESC')->take(5)->get();
        } else if(auth()->user()->privileges == "USERS MAGANG") {
            $absens = Absens::where('deleted', 0)->where('id_users', auth()->user()->id)->orderBy('id', "DESC")->take(5)->get();
            $aktifitasMagangs = AktifitasMagangs::where('deleted', 0)->where('id_users', auth()->user()->id)->orderBy('id')->take(5)->get();
            $dataIzinMagangs = Izins::where('deleted', 0)->where('id_users', auth()->user()->id)->orderBy('id')->take(5)->get();
            $pemberianTugas = PemberianTugas::where('id_users_diberi_tugas', auth()->user()->id)->orderBy('id', 'DESC')->take(5)->get();
        } else if(auth()->user()->privileges == "PEMBIMBING SEKOLAH") {
            $absens = Absens::leftJoin('magangs', 'magangs.id_users_magang', '=', 'absens.id_users')->select('absens.*')->where('magangs.id_pembimbing_sekolah', auth()->user()->id)->take(5)->get();
            $aktifitasMagangs = AktifitasMagangs::leftJoin('magangs', 'magangs.id_users_magang', '=', 'aktifitas_magangs.id_users')->select('aktifitas_magangs.*')->where('magangs.id_pembimbing_sekolah', auth()->user()->id)->take(5)->get();
            $dataIzinMagangs = Izins::leftJoin('magangs', 'magangs.id_users_magang', '=', 'izins.id_users')->select('izins.*')->where('magangs.id_pembimbing_sekolah', auth()->user()->id)->take(5)->get();
            $pemberianTugas = PemberianTugas::where('id_users_diberi_tugas', auth()->user()->id)->orderBy('id', 'DESC')->take(5)->get();
        } else if(auth()->user()->privileges == "PEMBIMBING MAGANG") {
            $absens = Absens::leftJoin('magangs', 'magangs.id_users_magang', '=', 'absens.id_users')->select('absens.*')->where('magangs.id_users_pembimbing_magang', auth()->user()->id)->take(5)->get();
            $aktifitasMagangs = AktifitasMagangs::leftJoin('magangs', 'magangs.id_users_magang', '=', 'aktifitas_magangs.id_users')->select('aktifitas_magangs.*')->where('magangs.id_users_pembimbing_magang', auth()->user()->id)->take(5)->get();
            $dataIzinMagangs = Izins::leftJoin('magangs', 'magangs.id_users_magang', '=', 'izins.id_users')->select('izins.*')->where('magangs.id_users_pembimbing_magang', auth()->user()->id)->take(5)->get();
            $pemberianTugas = PemberianTugas::where('id_users_diberi_tugas', auth()->user()->id)->orderBy('id', 'DESC')->take(5)->get();
        }
        return view('home.dashboard',
            [
                "absens" => $absens,
                "aktifitasMagangs" => $aktifitasMagangs,
                'dataIzinMagangs' => $dataIzinMagangs,
                'countAjuanMagang' => MagangRegistration::where('deleted', 0)->where('status','PENGAJUAN')->get()->count(),
                'countPesertaMagang' =>Magangs::where('deleted',0)->where('status','MAGANG')->get()->count(),
                'countAbsensiHariIni' => Absens::where('deleted', 0)->whereDate('created_at', date('Y-m-d'))->get()->count(),
                'countAktifitasMagans' => AktifitasMagangs::where('deleted', 0)->whereDate('created_at', date('Y-m-d'))->get()->count(),
                'pemberianTugas' => $pemberianTugas
            ]
        );
    }

}
