<?php

namespace App\Http\Controllers;

use App\Models\MagangRegistration;
use App\Models\Magangs;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailPendaftaranMagang;
use App\Models\Instansis;


class PendaftaranMagangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pendaftaran_magang.v_data_pendaftaran_magang', [
            'pendaftaranMagans' => MagangRegistration::where('deleted', 0)->orderBy('id', 'DESC')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pendaftaran_magang.v_pendaftaran_magang_detail', [
            'detailPendaftaranMagang' => MagangRegistration::find($id),
            'pembimbingMagang' => User::where('deleted', 0)->where('privileges', 'PEMBIMBING MAGANG')->get(),
            'pembimbingSekolah' => User::where('deleted', 0)->where('privileges', 'PEMBIMBING SEKOLAH')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tanggal_mulai_magang' => 'required',
            'id_users_pembimbing_magang' => 'required',
            'status' => 'required',
            'tanggal_selesai_magang' => 'required',
            'id_pembimbing_sekolah' => 'required',
            'keterangan' => 'required'
        ]);
        $pendaftaranMagangDetai = MagangRegistration::find($id);
        //update magang registration
        $update['magang_registrations']['keterangan'] = $request->keterangan;
        $update['magang_registrations']['status'] = $request->status;
        MagangRegistration::where('id', $id)->update($update['magang_registrations']);
        $pendaftaranMagangDetail = MagangRegistration::find($id);

        //check instansi
        $countinstansis = Instansis::where("nama_instansi", $pendaftaranMagangDetail->nama_instansi)->get()->count();
        if($countinstansis > 0){
            $resultInstansis = Instansis::where('nama_instansi', $pendaftaranMagangDetail->nama_instansi)->first();
            $is_instansis = $resultInstansis->id;
        } else {
            $insert['instansis']['nama_instansi'] = $pendaftaranMagangDetail->nama_instansi;
            $insert['instansis']['alamat_instansi'] = $pendaftaranMagangDetail->alamat_lengkap_instansi;
            $insert['instansis']['telepon_instansi'] = $pendaftaranMagangDetail->telepon_instansi;
            $resultInsertInstansis = Instansis::create($insert['instansis']);
            $is_instansis = $resultInsertInstansis->id;
        }
        if($request->status == "DITERIMA") {
            //SET INSERT TO TABEL magangs
            $insert['magangs']['kode_pendaftaran_magang'] = $pendaftaranMagangDetail->kode_registrasi;
            $insert['magangs']['id_users_magang'] = 0;
            $insert['magangs']['id_instansi'] = $is_instansis;
            $insert['magangs']['id_users_pembimbing_magang'] = $request->id_users_pembimbing_magang;
            $insert['magangs']['photo'] = $pendaftaranMagangDetail->fupload_berkas_foto_peserta_magang;
            $insert['magangs']['file_berkas'] = $pendaftaranMagangDetail->fupload_berkas_peserta_magang;
            $insert['magangs']['jurusan_bidang_keahlihan'] = $pendaftaranMagangDetail->bidang_keahlian_peserta_magang;
            $insert['magangs']['id_pembimbing_sekolah'] = $request->id_pembimbing_sekolah;
            $insert['magangs']['tanggal_mulai_magang'] = date('Y-m-d', strtotime($request->tanggal_mulai_magang));
            $insert['magangs']['tanggal_selesai_magang'] = date('Y-m-d', strtotime($request->tanggal_selesai_magang));
            $insert['magangs']['is_approval'] = 'Y';
            $insert['magangs']['is_approval_at'] = date('Y-m-d H:i:s');
            $insert['magangs']['keterangan'] = $request->keterangan;
            $insert['magangs']['user_approval'] = auth()->user()->nama_lengkap;
            $resultInsertMagans = Magangs::create($insert['magangs']);


//            //SET INSERT TO USERS
            $insertToUsers['users']['username'] = $pendaftaranMagangDetail->username_peserta_magang;
            $insertToUsers['users']['password'] = $pendaftaranMagangDetail->password_peserta_magang;
            $insertToUsers['users']['nama_lengkap'] = $pendaftaranMagangDetail->nama_lengkap_peserta_magang;
            $insertToUsers['users']['alamat'] = $pendaftaranMagangDetail->alamat_lengkap_peserta_magang;
            $insertToUsers['users']['gender'] = $pendaftaranMagangDetail->jenis_kelamin_peserta_magang;
            $insertToUsers['users']['email'] = $pendaftaranMagangDetail->email_peserta_magang;
            $insertToUsers['users']['telepon'] =$pendaftaranMagangDetail->telepon_peserta_magang;
            $insertToUsers['users']['photo'] = $pendaftaranMagangDetail->fupload_berkas_foto_peserta_magang;
            $insertToUsers['users']['privileges'] = 'USERS MAGANG';
            $insertToUsers['users']['is_peserta_magang'] = 'Y';
            $insertToUsers['users']['pending'] = 0;
            $insertToUsers['users']['disabled'] = 0;
            $insertToUsers['users']['kode_verifikasi'] = $pendaftaranMagangDetail->kode_registrasi;
            $insertToUsers['users']['kode_verifikasi_at'] = date('Y-m-d H:i:s');
            $resultInsertUser = User::create($insertToUsers['users']);
            $resultIdUsers = $resultInsertUser->id;

            //SET UPDATE MAGANGS
            $update['magangs']['id_users_magang'] = $resultIdUsers;
            Magangs::where('kode_pendaftaran_magang', $pendaftaranMagangDetail->kode_registrasi)->update($update['magangs']);

            //SET SOFT DELERE REGISTRASI MAGANG
            $update['magang_registrations']['deleted'] = 1;
            MagangRegistration::where('kode_registrasi', $pendaftaranMagangDetail->kode_registrasi)->update($update['magang_registrations']);


//            update data registrasi magang
            $update['magang_registrations']['deleted'] = 1;
            MagangRegistration::where('id', $id)->update($update['magang_registrations']);
        }

        if($request->status != "PENGAJUAN") {
            Mail::to($pendaftaranMagangDetai->email_peserta_magang)->send(new SendMailPendaftaranMagang($pendaftaranMagangDetail));
        }
        return redirect('/pendaftaran-magang')->with('success', 'Sukses Melakukan Verifikasi Pendaftaran Magangs');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
