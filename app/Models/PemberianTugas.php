<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\FileUploads;

class PemberianTugas extends Model
{
    use HasFactory;
    protected $table = 'pemberian_tugas';
    protected $guarded = ['id'];

    public function usersPemberiTugas(){
        return $this->belongsTo(User::class, 'id_users_pemberi_tugas', 'id');
    }
    public function userDiberiTugas(){
        return $this->belongsTo(User::class, 'id_users_diberi_tugas', 'id');
    }
    public function fileLampiran(){
        return $this->belongsTo(FileUploads::class, 'id_file_tugas', 'id');
    }
}
