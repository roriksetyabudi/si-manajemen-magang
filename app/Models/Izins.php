<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Izins extends Model
{
    use HasFactory;

    protected $table = 'izins';
    protected $guarded = ['id'];

    public function user(){
        return $this->belongsTo(User::class, 'id_users', 'id');
    }
    public function fileuploads(){
        return $this->belongsTo(FileUploads::class, 'file_bukti_izin', 'id');
    }
    public function userApproval(){
        return $this->belongsTo(User::class, 'id_users_approval', 'id');
    }

}
