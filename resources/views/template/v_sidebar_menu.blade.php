<!-- Sidebar Menu -->
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-header">MENU UTAMA</li>
        <li class="nav-item">
            <a href="/home" class="nav-link
            {{
                request()->is('home') ? 'active' :
                request()->is('/') ? 'active' : ''
            }}">
                <i class="nav-icon far fa-image"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        @if(auth()->user()->privileges == "ADMIN")
        <li class="nav-item">
            <a href="/general-settings" class="nav-link
                 {{
                    request()->is('general-settings') ? 'active' : ''

                }}">
                <i class="nav-icon fas fa-book"></i>
                <p>
                    General Settings
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="/data-users" class="nav-link
                     {{
                        request()->is('data-users') ? 'active' :
                        request()->is('data-users/create') ? 'active' :
                        request()->is('edit-users') ? 'active' : ''
                    }}">
                <i class="nav-icon far fa-user"></i>
                <p>
                    Users
                </p>
            </a>
        </li>
        @endif
        @if(auth()->user()->privileges == "ADMIN")
        <li class="nav-item">
            <a href="/pendaftaran-magang" class="nav-link
                    {{
                        request()->is('pendaftaran-magang') ? 'active' :
                        request()->is('pendaftaran-magang/{id}/edit') ? 'active' : ''
                    }}">
                <i class="nav-icon fas fa-book"></i>
                <p>
                    Pendaftaran Magang
                </p>
            </a>
        </li>
        @endif
        <li class="nav-item">
            <a href="/data-magang" class="nav-link
                    {{
                        request()->is('data-magang') ? 'active' :
                        request()->is('data-magang/create') ? 'active' :
                        request()->is('data-magang-detail') ? 'active' : ''
                    }}">
                <i class="nav-icon fas fa-file"></i>
                <p>
                    Data Magang
                </p>
            </a>
        </li>
        @if(auth()->user()->privileges == "USERS MAGANG" || auth()->user()->privileges == "ADMIN" || auth()->user()->privileges == "PEMBIMBING MAGANG" || auth()->user()->privileges == "PEMBIMBING SEKOLAH")
            <li class="nav-item">
                <a href="/absensi-magang" class="nav-link
                {{
                    request()->is('absensi-magang') ? 'active' :
                    request()->is('absensi-magang/create') ? 'active' :
                    request()->is('absensi-magang-add') ? 'active' : ''

                }}">
                    <i class="nav-icon fas fa-book"></i>
                    <p>
                        Absensi Magang
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/aktifitas-magang" class="nav-link
                    {{
                        request()->is('aktifitas-magang') ? 'active' :
                        request()->is('aktifitas-magang-add') ? 'active' : ''
                    }}">
                    <i class="nav-icon fas fa-tree"></i>
                    <p>
                        Aktifitas Magang
                    </p>
                </a>
            </li>
        @endif
        @if(auth()->user()->privileges == "USERS MAGANG" || auth()->user()->privileges == "PEMBIMBING MAGANG"  || auth()->user()->privileges == "ADMIN" || auth()->user()->privileges == "PEMBIMBING SEKOLAH")
            <li class="nav-item">
                <a href="/nilai-magang" class="nav-link
                {{
                    request()->is('nilai-magang') ? 'active' :
                    request()->is('nilai-magang-add') ? 'active' : ''


                }}">
                    <i class="nav-icon fas fa-book"></i>
                    <p>
                        Nilai Magang
                    </p>
                </a>
            </li>
        @endif

        <li class="nav-item">
            <a href="/data-izin" class="nav-link
            {{
                request()->is('data-izin') ? 'active' :
                request()->is('data-izin-add') ? 'active' :
                request()->is('data-izin-edit') ? 'active' :
                request()->is('data-izin-detail') ? 'active' : ''
            }}">
                <i class="nav-icon fas fa-book"></i>
                <p>
                    Data Izin
                </p>
            </a>
        </li>


        @if(auth()->user()->privileges == "ADMIN" || auth()->user()->privileges == "PEMBIMBING SEKOLAH" || auth()->user()->privileges == "PEMBIMBING MAGANG")
            <li class="nav-item
                        {{
                            request()->is('laporan-data-magang') ? 'menu-open' :
                            request()->is('laporan-absensi-magang') ? 'menu-open' :
                            request()->is('laporan-aktifitas-magang') ? 'menu-open' :
                            request()->is('laporan-izin-magang') ? 'menu-open' :
                            request()->is('laporan-nilai-magang') ? 'menu-open' :
                            request()->is('laporan-data-users') ? 'menu-open' : ''
                        }}">
                <a href="#" class="nav-link {{
                            request()->is('laporan-data-magang') ? 'active' :
                            request()->is('laporan-absensi-magang') ? 'active' :
                            request()->is('laporan-aktifitas-magang') ? 'active' :
                            request()->is('laporan-izin-magang') ? 'active' :
                            request()->is('laporan-nilai-magang') ? 'active' :
                            request()->is('laporan-data-users') ? 'active' : ''
                        }}">
                    <i class="nav-icon fas fa-copy"></i>
                    <p>
                        Laporan
                        <i class="fas fa-angle-left right"></i>
                        <span class="badge badge-info right">7</span>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="/laporan-data-magang" class="nav-link
                        {{request()->is('laporan-data-magang') ? 'active' : ''}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Magang</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/laporan-absensi-magang" class="nav-link {{request()->is('laporan-absensi-magang') ? 'active' : ''}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Absensi Magang</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/laporan-absensi-peserta-magang" class="nav-link {{request()->is('laporan-absensi-peserta-magang') ? 'active' : ''}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Absensi Peserta Magang</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/laporan-aktifitas-magang" class="nav-link {{request()->is('laporan-aktifitas-magang') ? 'active' : ''}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Aktifitas Magang</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/laporan-izin-magang" class="nav-link {{request()->is('laporan-izin-magang') ? 'active' : ''}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Izin Magang</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/laporan-nilai-magang" class="nav-link {{request()->is('laporan-nilai-magang') ? 'active' : ''}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Nilai Magang</p>
                        </a>
                    </li>
                    @if(auth()->user()->privileges == "ADMIN")
                        <li class="nav-item">
                            <a href="/laporan-data-users" class="nav-link {{request()->is('laporan-data-users') ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Data Users</p>
                            </a>
                        </li>
                    @endif



                </ul>
            </li>
        @endif
        @if(auth()->user()->privileges == "USERS MAGANG" || auth()->user()->privileges == "PEMBIMBING MAGANG")
            <li class="nav-item">
                <a href="/pemberian-tugas" class="nav-link {{request()->is('pemberian-tugas') ? 'active' : ''}}">
                    <i class="nav-icon fas fa-book"></i>
                    <p>Pemberian Tugas </p>
                </a>
            </li>
        @endif


        <li class="nav-header">AKUN</li>
        <li class="nav-item">
            <a href="/ubah-akun" class="nav-link {{request()->is('ubah-akun') ? 'active' : ''}}">
                <i class="nav-icon fas fa-user"></i>
                <p>
                    Ubah Akun
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="/logout" class="nav-link">
                <i class="nav-icon fas fa-trash"></i>
                <p>
                    Logout
                </p>
            </a>
        </li>
    </ul>
</nav>
<!-- /.sidebar-menu -->
