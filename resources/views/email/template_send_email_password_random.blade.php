@component('mail::message')
    # PASSWORD BARU ANDA
    Lupa Password Sukses DIlakukan, Berikut Adalah Password Anda, Password ini bersifat random, silahkan login menggunakan password di bawah ini, jika sukses login silahkan untuk mengganti password sesuai dengan keinginan anda
    PASSWORD ANDA ADALAH :
    {{ $passwordRandom['password'] }}

    Terima Kasih
    {{ config('app.name') }}
@endcomponent
