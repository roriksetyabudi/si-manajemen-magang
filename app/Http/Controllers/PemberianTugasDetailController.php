<?php

namespace App\Http\Controllers;

use App\Models\FileUploads;
use App\Models\PemberianTugasDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PemberianTugasDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_kegiatan' => 'required',
            'keterangan' => 'required',
            'target' => 'required',
            'id_file_tugas' => 'mimes:pdf,image,xlsx,csv,xls,zip,doc,docx'

        ]);
        $resultIdFileUpload = 0;
        if($request->file('id_file_tugas')) {
            $insert['file_uploads']['title'] = "Lampiran Pengerjaan Tugas ". $request->nama_kegiatan;
            $insert['file_uploads']['filename'] = $request->file('id_file_tugas')->hashName();
            $insert['file_uploads']['path'] = $request->file('id_file_tugas')->store('file-uploads/documents');
            $insert['file_uploads']['type'] = $request->file('id_file_tugas')->extension();
            $insert['file_uploads']['size'] = $request->file('id_file_tugas')->getSize();
            $queryInsertFileUploads = FileUploads::create($insert['file_uploads']);
            $resultIdFileUpload = $queryInsertFileUploads->id;
        }
        $insert['pengerjaan_tugas']['id_tugas'] = $request->id_tugas;
        $insert['pengerjaan_tugas']['id_users_created'] = auth()->user()->id;
        $insert['pengerjaan_tugas']['nama_kegiatan'] = $request->nama_kegiatan;
        $insert['pengerjaan_tugas']['keterangan'] = $request->keterangan;
        $insert['pengerjaan_tugas']['id_file_tugas'] = $resultIdFileUpload;
        $insert['pengerjaan_tugas']['target'] = $request->target;
        $insert['pengerjaan_tugas']['created_at'] = date('Y-m-d H:i:s');
        $insert['pengerjaan_tugas']['updated_at'] = date('Y-m-d H:i:s');

        PemberianTugasDetail::create($insert['pengerjaan_tugas']);
        return redirect('/pemberian-tugas-detail/'.$request->id_tugas)->with('success', 'Sukses Melakukan Pengerjaan Tugas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pemberianTugasDetail = PemberianTugasDetail::find($id);
        if($pemberianTugasDetail->id_file_tugas != 0) {
            $detailFileUploads = FileUploads::find($pemberianTugasDetail->id_file_tugas);
            Storage::delete($detailFileUploads->path);
        }
        DB::table('pemberian_tugas_detail')->where('id', '=', $id)->delete();
        return redirect('/pemberian-tugas-detail/'.$pemberianTugasDetail->id_tugas)->with('success', 'Sukses Menghapus Pengerjaan Tugas');
    }

}
