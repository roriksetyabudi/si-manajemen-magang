@extends('template.v_template')
@section('title', 'Data Absensi Magang')
@push('head-css')
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/adminlte.min.css">

    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/summernote/summernote-bs4.min.css">
    <!-- CodeMirror -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/codemirror/codemirror.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/codemirror/theme/monokai.css">
    <!-- SimpleMDE -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/simplemde/simplemde.min.css">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

@endpush
@section('content')
    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
        </symbol>
        <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
        </symbol>
        <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
        </symbol>
    </svg>
    @if(auth()->user()->privileges == "USERS MAGANG")
        <div class="card card-primary">
            @if(session()->has('success'))
                <div class="alert alert-success d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                    <div>
                        {{ session('success') }}
                    </div>
                </div>
            @endif

            @if(session()->has('fail'))
                <div class="alert alert-danger" role="alert">
                    {{ session('fail') }}
                </div>
            @endif


            <div class="card-header">
                <h3 class="card-title">Detail Tugas {{ strtoupper($pemberianTugas->nama_tugas) }}</h3>
            </div>
            <form method="post" action="/pemberian-tugas-detail/post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" class="form-control @error('id_tugas') is-invalid @enderror" id="id_tugas" name="id_tugas" placeholder="Tugas" value="{{ $pemberianTugas->id }}">

                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kegiatan</label>
                        <input type="text" class="form-control @error('nama_kegiatan') is-invalid @enderror" id="nama_kegiatan" name="nama_kegiatan" placeholder="Nama Kegiatan" value="">
                        @error('nama_kegiatan')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Keterangan Tugas Yang Diberikan</label>
                        <textarea id="keterangan_summernote" name="keterangan" class="@error('keterangan') is-invalid @enderror">

                    </textarea>
                        @error('keterangan')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">File Lampiran Tugas</label>
                        <input type="file" class="form-control @error('id_file_tugas') is-invalid @enderror" id="id_file_tugas" name="id_file_tugas" placeholder="Pemberian Tugas" value=""><br>

                        @error('id_file_tugas')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Realisasi</label>
                        <input type="number" class="form-control @error('target') is-invalid @enderror" id="target" name="target" placeholder="Realisasi" value="">
                        @error('target')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Proses</button>
                    </div>
            </form>


        </div>
    @endif


    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Detail Pengerjaan Tugas {{ strtoupper($pemberianTugas->nama_tugas) }}</h3>
        </div>
        <table class="table table-bordered table_list_data_pemberian_tugas" id="table_list_data_pemberian_tugas">
            <thead>
            <tr class="bg-info">
                <th>Nama Pengerjaan</th>
                <th>Keterangan</th>
                <th>Lampiran</th>
                <th>Tanggal Dibuat</th>
                <th>Realisasi</th>
                <th class="text-center">Aksi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($pemberianTugasDetail as $value)
                <tr>
                    <td>{{ $value->nama_kegiatan }}</td>
                    <td>{{ $value->keterangan }}</td>
                    <td>
                        @if($value->id_file_tugas == 0)

                        @else
                            <a href="{{asset('storage/'.$value->fileLampiran->path)}}" target="blank">{{ $value->fileLampiran->title }}</a>
                        @endif

                    </td>
                    <td>{{  date('d F y H:i:s', strtotime($value->created_at))}}</td>
                    <td>{{ $value->target }} %</td>

                        <td class="text-center">
                            @if(auth()->user()->privileges == "USERS MAGANG")
                                <a href="/delete-pemberian-tugas-detail/{{ $value->id }}" class="btn btn-danger">Hapus</a>
                            @endif

                        </td>

                </tr>
            @endforeach


            </tbody>
        </table>
    </div>





@endsection
@push('bottom-js')
    <script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

    <!-- bs-custom-file-input -->
    <script src="{{asset('template')}}/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

    <!-- AdminLTE App -->
    <script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>

    <!-- Summernote -->
    <script src="{{asset('template')}}/plugins/summernote/summernote-bs4.min.js"></script>
    <!-- CodeMirror -->
    <script src="{{asset('template')}}/plugins/codemirror/codemirror.js"></script>
    <script src="{{asset('template')}}/plugins/codemirror/mode/css/css.js"></script>
    <script src="{{asset('template')}}/plugins/codemirror/mode/xml/xml.js"></script>
    <script src="{{asset('template')}}/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>


    <script>
        $(function () {

            // Summernote
            $('#keterangan_summernote').summernote()

            // CodeMirror
            CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
                mode: "htmlmixed",
                theme: "monokai"
            });


        })
    </script>
@endpush

