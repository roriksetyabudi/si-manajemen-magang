<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Untitled Document</title>
    <style type="text/css">
        body {
            font-family: Arial, Helvetica, sans-serif;
        }
        .table_data {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 13px;
            height: auto;
            width: 100%;
            border: thin none #333;
            padding-top: 5px;
        }
        .header_table_data {
            width: 15%;
            height: auto;
            text-align: center;
            border: thin none #666;
        }
        .header_table_data_content {
            width: 85%;
            height: auto;
            text-align: center;
            vertical-align: top;
            border: thin none #666;
            line-height: 3px;
        }
        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-top-width: 2px;
            border-right-width: 2px;
            border-bottom-width: 2px;
            border-left-width: 2px;
            border-top-style: ridge;
            border-right-style: ridge;
            border-bottom-style: ridge;
            border-left-style: ridge;
        }
        .line_table {
            height: auto;
            width: 100%;
            border: thin none #000;
            padding-left: 5px;
        }
        .label_surat_keterangan_berobat {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 16px;
            font-weight: bold;
            color: #000;
            text-align: center;
            vertical-align: top;
            height: auto;
            width: 100%;
            text-decoration: underline;

        }
        .label_nomor_surat_keterangan_berobat {
            height: auto;
            width: 100%;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            text-align: center;
            vertical-align: top;
            padding-top: 10px;
        }

        .header_kepada {
            height: auto;
            width: 60%;
            border: thin none #333;
        }
        .footer_kepada {
            width: 40%;
            border: thin none #000;
        }
        .p_text {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            line-height: 16px;
        }
        .isi_label{
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            height: auto;
            width: 25%;
            padding-left: 25px;
            padding-top: 5px;
            padding-bottom: 5px;
            border: thin none #000;
        }
        .isi_content{
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            height: auto;
            width: 30%;
            padding-top: 5px;
            padding-bottom: 5px;
            border: thin none #000;
        }
        .table_isi_1 {
            height: auto;
            width: 100%;
            padding-top: 5px;
            padding-bottom: 5px;
        }
    </style>
</head>

<body>
<table class="table_data">
    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"><b></b></td>
    </tr>
</table>
<table class="table_data">
    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"><b></b></td>
    </tr>
</table>
<table class="table_data">
    <tr>
        <td align="centar" width="5%" class="header_logo" rowspan="4"></td>
        <td align="center" width="90%" class="header_title" style="letter-spacing: 3px;"><H2>PT Pos Indonesia (Persero) Madiun</H2></td>

        <td align="centar" width="5%" class="header_logo" rowspan="4"></td>
    </tr>
    <tr><td align="center" width="90%" class="header_title" colspan="3"><H5>Jalan Pahlawan Nomor 24 Madiun Kode Pos 63100</H5></td></tr>
    <tr><td align="center" width="90%" class="header_title" colspan="3" style="letter-spacing: 2px;"><H2></H2></td></tr>
    <tr><td align="center" width="90%" class="header_title" colspan="3" style="letter-spacing: 2px;"><H1><b><u>SERTIFIKAT</u></b></H1><br><h6>{{ strtoupper($detailNilai->no_sertifikat) }}</h6></td></tr>


</table>
<table class="table_data">
    <tr><td align="center" width="100%" class="header_title" colspan="3"><H5>DIBERIKAN KEPADA</H5></td></tr>
</table>


<table class="table_data">
    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"><b></b></td>
    </tr>

</table>
<table class="table_data">
    <tr><td align="center" width="100%" class="header_title" colspan="3"><H1>{{ strtoupper($detailNilai->user->nama_lengkap) }}</H1></td></tr>
</table>

<table class="table_data">
    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"><b></b></td>
    </tr>
</table>
<table class="table_data">
    <tr><td align="center" width="100%" class="header_title" colspan="3"><h5>Nama Instansi {{ strtoupper($detailNilai->magangs->instansis->nama_instansi) }}</h5></td></tr>
</table>
<table class="table_data">
    <tr><td align="center" width="100%" class="header_title" colspan="3"><h5>Telah Melakukan Praktek Kerja di PT POS Indonesia Cabang Madiun</h5></td></tr>
</table>
<table class="table_data">
    <tr><td align="center" width="100%" class="header_title" colspan="3"><h5>Periode {{ strtoupper(date('d F Y', strtotime($detailNilai->magangs->tanggal_mulai_magang))) }} - {{ strtoupper(date('d F Y', strtotime($detailNilai->magangs->tanggal_selesai_magang))) }}</h5></td></tr>
</table>

<table class="table_data">
    <tr><td align="center" width="100%" class="header_title" colspan="3"><h5>Sertifikat ini Diberikan Kepada Yang Bersangkutan untuk dapat digunakan sebagaimana mestinya</h5></td></tr>
</table>
<table class="table_data">
    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"><b></b></td>
    </tr>
</table>
<table class="table_data">
    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"><b></b></td>
    </tr>
</table>
<table class="table_data">

    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;">Madiun {{ strtoupper(date('d F Y', strtotime($detailNilai->magangs->tanggal_selesai_magang))) }} <br>Pembimbing Magang</td>
    </tr>
    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"></td>
    </tr>
    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"></td>
    </tr>
    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"></td>
    </tr>

    <tr>
        <td width="60%">&nbsp;</td>
        <td width="40%" style="text-align:centar;"><b><u>{{ $detailNilai->magangs->usersPembimbingmagang->nama_lengkap }}</u><br>Nip Pos : {{ $detailNilai->magangs->usersPembimbingmagang->nippos }}</b></td>
    </tr>
</table>
</body>
</html>
