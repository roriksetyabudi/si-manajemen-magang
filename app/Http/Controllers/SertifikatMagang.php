<?php

namespace App\Http\Controllers;
use PDF;
use App\Models\NilaiTrans;
use Illuminate\Http\Request;

class SertifikatMagang extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $detailNilaiCount = NilaiTrans::where('id_users', $id)->get()->count();
        if($detailNilaiCount > 0) {
            $detailNilai = NilaiTrans::where('id_users', $id)->get()->first();

            $view = view('laporan.nilai_magang.v_cetak_sertifikat', [
                "detailNilai" => $detailNilai,
            ]);
            $html_content = $view->render();
            PDF::SetAuthor('Rorik Setya Budi');
            PDF::SetTitle('Laporan Data Sistem Informasi Manajemen Magang');
            PDF::SetSubject("Laporan Nilai Magang");
            PDF::SetKeywords('sistem informasi manajemen magang');
            PDF::setPrintHeader(false);
            PDF::setPrintFooter(false);
            PDF::SetMargins(7, 7, 7, 7);
            PDF::SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

            PDF::AddPage('L', 'F4');
            PDF::SetFont('helvetica', null, 11);
            PDF::writeHTML($html_content, true, false, true, false, '');
            PDF::Output('data_nilai.pdf');
        } else {
            echo "Data Tidak Tersedia";
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
