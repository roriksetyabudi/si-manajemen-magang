<?php

namespace App\Http\Controllers;

use App\Models\GeneralSettings;
use App\Models\MagangRegistration;
use App\Models\FileUploads;
use App\Models\User;
use Illuminate\Http\Request;

class MagangRegistrasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('extras.registrasi_magang.v_registrasi_magang');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $checkCountDataMagangs = MagangRegistration::where('deleted', 0)->get()->count();
        $checkGeneralSettings = GeneralSettings::first();
        if($checkCountDataMagangs == $checkGeneralSettings->kuota_magang) {
            return redirect('/registrasi-magang/create')->with('fail', 'Kuota Magang Sudah Terpenuhi, Silahkan Mendaftar Pada Lain Waktu');

        }

        $validatedData = $request->validate([
            'username_peserta_magang' => 'required|min:3|unique:magang_registrations',
            'password_peserta_magang' => 'required',
            'ulangi_password_peserta_magang' => 'required|same:password_peserta_magang',
            'nama_lengkap_peserta_magang' => 'required|min:3',
            'alamat_lengkap_peserta_magang' => 'required|min:3',
            'jenis_kelamin_peserta_magang' => 'required',
            'telepon_peserta_magang' => 'required|min:3|unique:magang_registrations',
            'email_peserta_magang' => 'required|min:3|unique:magang_registrations|email',
            'nama_instansi' => 'required|min:3',
            'alamat_lengkap_instansi' => 'required|min:3',
            'telepon_instansi' => 'required|min:3',
            'bidang_keahlian_peserta_magang' => 'required',
            'fupload_berkas_peserta_magang' => 'required|mimes:pdf|max:2000',
            'fupload_berkas_foto_peserta_magang' => 'required|image|file|max:1000',

            'nama_pembimbing_sekolah_peserta_magang' => 'required|min:3',
            'tanggal_ajuan_mulai_magang' => 'required|min:3',
            'tanggal_ajuan_selesai_magang' => 'required|min:3',
        ]);

        //CHECK EMAIL IN USERS
        $checkUsersName = User::where("username", $request->username_peserta_magang)->get()->count();
        if($checkUsersName > 0) {
            return redirect('/registrasi-magang/create')->with('fail', 'Username '.$request->username_peserta_magang.' Yang Anda Masukan Sudah Digunakan');
        }
        $checkEmails = User::where("email", $request->email_peserta_magang)->get()->count();
        if($checkEmails > 0) {
            return redirect('/registrasi-magang/create')->with('fail', 'Email '.$request->email_peserta_magang.' Yang Anda Masukan Sudah Digunakan');
        }
        $checkNoTelepon = User::where("telepon", $request->telepon_instansi)->get()->count();
        if($checkNoTelepon > 0) {
            return redirect('/registrasi-magang/create')->with('fail', 'No Telepon '.$request->telepon_instansi.' Yang Anda Masukan Sudah Digunakan');
        }

        //SAVE TO file_uploads Berkas Magang
        $dataFileUploads['berkasMagang']['title'] = "Berkas Magang ". $request->nama_lengkap_peserta_magang;
        $dataFileUploads['berkasMagang']['filename'] = $request->file('fupload_berkas_peserta_magang')->hashName();
        $dataFileUploads['berkasMagang']['type'] = $request->file('fupload_berkas_peserta_magang')->extension();
        $dataFileUploads['berkasMagang']['size'] = $request->file('fupload_berkas_peserta_magang')->getSize();
        if($request->file('fupload_berkas_peserta_magang')) {
            $dataFileUploads['berkasMagang']['path'] = $request->file('fupload_berkas_peserta_magang')->store('file-uploads/documents');
        }
        $resultBerkasMagang = FileUploads::create($dataFileUploads['berkasMagang']);
        $resultIdBerkasMagang = $resultBerkasMagang->id;
        //SAVE TO file_uploads pas foto
        $dataFileUploads['pasPhoto']['title'] = "Foto Peserta ". $request->nama_lengkap_peserta_magang;
        $dataFileUploads['pasPhoto']['filename'] = $request->file('fupload_berkas_foto_peserta_magang')->hashName();
        $dataFileUploads['pasPhoto']['type'] = $request->file('fupload_berkas_foto_peserta_magang')->extension();
        $dataFileUploads['pasPhoto']['size'] = $request->file('fupload_berkas_foto_peserta_magang')->getSize();
        if($request->file('fupload_berkas_foto_peserta_magang')) {
            $dataFileUploads['pasPhoto']['path'] = $request->file('fupload_berkas_foto_peserta_magang')->store('file-uploads/images');
        }
        $resulrPasPhoto = FileUploads::create($dataFileUploads['pasPhoto']);
        $resultIdPasPhoto = $resulrPasPhoto->id;

        //INSERT TO magang_registrations
        $dataRegistrasi['username_peserta_magang'] = $request->username_peserta_magang;
        $dataRegistrasi['password_peserta_magang'] = bcrypt($request->password_peserta_magang);
        $dataRegistrasi['kode_registrasi'] = random_int(100000, 999999);
        $dataRegistrasi['nama_lengkap_peserta_magang'] = $request->nama_lengkap_peserta_magang;
        $dataRegistrasi['alamat_lengkap_peserta_magang'] = $request->alamat_lengkap_peserta_magang;
        $dataRegistrasi['jenis_kelamin_peserta_magang'] = $request->jenis_kelamin_peserta_magang;
        $dataRegistrasi['telepon_peserta_magang'] = $request->telepon_peserta_magang;
        $dataRegistrasi['email_peserta_magang'] = $request->email_peserta_magang;
        $dataRegistrasi['fupload_berkas_peserta_magang'] = $resultIdBerkasMagang;
        $dataRegistrasi['fupload_berkas_foto_peserta_magang'] = $resultIdPasPhoto;

        $dataRegistrasi['nama_instansi'] = $request->nama_instansi;
        $dataRegistrasi['alamat_lengkap_instansi'] = $request->alamat_lengkap_instansi;
        $dataRegistrasi['telepon_instansi'] = $request->telepon_instansi;
        $dataRegistrasi['bidang_keahlian_peserta_magang'] = $request->bidang_keahlian_peserta_magang;

        $dataRegistrasi['nama_pembimbing_sekolah_peserta_magang'] = $request->nama_pembimbing_sekolah_peserta_magang;
        $dataRegistrasi['tanggal_ajuan_mulai_magang'] = date('Y-m-d', strtotime($request->tanggal_ajuan_mulai_magang));
        $dataRegistrasi['tanggal_ajuan_selesai_magang'] = date('Y-m-d', strtotime($request->tanggal_ajuan_selesai_magang));



        $resultInsertRegistrasi = MagangRegistration::create($dataRegistrasi);
        if($resultInsertRegistrasi) {
            return redirect('/registrasi-magang/create')->with('success', 'Sukses Melakukan Registrasi Magang, Silahkan Menunggu Konfirmasi Melalui Email Terdaftar');
        } else {
            return redirect('/registrasi-magang/create')->with('fail', 'Registrasi Magang Gagal Dilakukan');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
