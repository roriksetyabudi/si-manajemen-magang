@extends('template.v_template')
@section('title', 'Dashboard')
@push('head-css')
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template/')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('template/')}}/plugins/fontawesome-free/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template/')}}/dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('template/')}}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
@endpush
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header border-transparent">
                        <h3 class="card-title">Data Absensi Peserta Magang</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table m-0">
                                <thead>
                                <tr>
                                    <th>Nama Peserta</th>
                                    <th>Masuk</th>
                                    <th>Pulang</th>
                                    <th>Status</th>
                                    <th>Updated</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($absens as $absen)
                                <tr>
                                    <td>{{ $absen->user->nama_lengkap }}</td>
                                    <td>
                                        {{ date('d F Y, H:i:s', strtotime($absen->tanggal_masuk)) }}
                                        @if(date('H:i:s', strtotime($absen->tanggal_masuk)) > '06:00:00' && date('H:i:s', strtotime($absen->tanggal_masuk)) < '08:05:00')
                                            <span class="badge badge-success">Tepat</span>
                                        @elseif(date('H:i:s', strtotime($absen->tanggal_masuk)) > '08:05:00' && date('H:i:s', strtotime($absen->tanggal_masuk)) < '09:05:00')
                                            <span class="badge badge-warning">Terlambat</span>
                                        @else
                                            <span class="badge badge-danger">Tidak Absen</span>
                                        @endif
                                    </td>
                                    <td>

                                        @if($absen->tanggal_pulang == "" || $absen->tanggal_pulang == null)
                                        @else
                                            {{ date('d F Y, H:i:s', strtotime($absen->tanggal_pulang)) }}
                                        @endif

                                        @if(date('H:i:s', strtotime($absen->tanggal_pulang)) >= '15:00:00')
                                            <span class="badge badge-success">Tepat</span>
                                        @elseif(date('H:i:s', strtotime($absen->tanggal_pulang)) > '14:00:00' && date('H:i:s', strtotime($absen->tanggal_pulang)) <= '15:00:00')
                                            <span class="badge badge-warning">Pulang Awal</span>
                                        @else
                                            <span class="badge badge-danger">Tidak Absen</span>
                                        @endif

                                    <td>
                                        @if($absen->tanggal_masuk != ""  && $absen->tanggal_pulang != "")
                                            <span class="badge badge-success">Hadir</span>
                                        @else
                                            <span class="badge badge-danger">Tidak Hadir</span>
                                        @endif

                                    </td>
                                    <td>{{ date('d F Y, H:i:s', strtotime($absen->updated_at)) }}</td>
                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">

                        <a href="/absensi-magang" class="btn btn-sm btn-secondary float-right">Lihat Semua Absensi</a>
                    </div>
                    <!-- /.card-footer -->
                </div>

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="ion ion-clipboard mr-1"></i>
                            Aktifitas Kegiatan Hari ini
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <ul class="todo-list" data-widget="todo-list">
                            @foreach($aktifitasMagangs as $aktifitasMagang)
                            <li>
                                <span class="handle">
                                  <i class="fas fa-ellipsis-v"></i>
                                  <i class="fas fa-ellipsis-v"></i>
                                </span>
                                <span class="text">{{ $aktifitasMagang->nama_kegiatan }} <small class="badge badge-info">{{ $aktifitasMagang->user->nama_lengkap }}</small></span>
                                <small class="badge badge-success"><i class="far fa-clock"></i> {{ $aktifitasMagang->interval_aktifitas }}</small>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        <a href="/aktifitas-magang" class="btn btn-primary float-right"> Lihat Lebih Banyak Aktifitas</a>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                @if(auth()->user()->privileges == "ADMIN")
                    <div class="info-box mb-3 bg-warning">
                        <span class="info-box-icon"><i class="fas fa-tag"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Ajuan Pendaftaran Magang</span>
                            <span class="info-box-number">{{ $countAjuanMagang }} Peserta</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                    <div class="info-box mb-3 bg-success">
                        <span class="info-box-icon"><i class="far fa-user"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Data Peserta Magang</span>
                            <span class="info-box-number">{{ $countPesertaMagang }} Peserta</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                    <div class="info-box mb-3 bg-info">
                        <span class="info-box-icon"><i class="fas fa-book"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Absensi Hari ini</span>
                            <span class="info-box-number">{{ $countAbsensiHariIni }} Peserta</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                    <div class="info-box mb-3 bg-info">
                        <span class="info-box-icon"><i class="fas fa-table"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Aktifitas Hari ini</span>
                            <span class="info-box-number">{{ $countAktifitasMagans }} Aktifitas</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                @endif


                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Izin Peserta Magang</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <ul class="products-list product-list-in-card pl-2 pr-2">
                            @foreach($dataIzinMagangs as $dataIzinMagang)
                                <li class="item">
                                    <div class="product-img">
                                        @if($dataIzinMagang->user->photo == "")
                                            <img src="{{asset('template')}}/dist/img/avatar.png" alt="Product Image" class="img-size-50">
                                        @else
                                            <img src="{{asset('storage/'.$dataIzinMagang->user->fileuploads->path )}}" alt="Product Image" class="img-size-50">
                                        @endif



                                    </div>
                                    <div class="product-info">
                                        <a href="/data-izin-detail/{{ $dataIzinMagang->id }}" class="product-title">{{ $dataIzinMagang->user->nama_lengkap }}
                                            <span class="badge badge-warning float-right">Detail</span></a>
                                        <span class="product-description">
                                        {{ $dataIzinMagang->alasan_izin }}
                                    </span>
                                    </div>
                                </li>
                            @endforeach



                        </ul>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer text-center">
                        <a href="/data-izin" class="uppercase">Lihat Semua Izin Peserta Magang</a>
                    </div>
                    <!-- /.card-footer -->
                </div>

                    @if(auth()->user()->privileges == "USERS MAGANG")
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Data Tugas Dari Pembimbing Magang</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table m-0">
                                        <thead>
                                        <tr>
                                            <th>Nama Tugas</th>
                                            <th>Keterangan</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($pemberianTugas as $value)
                                            <tr>
                                                <td>{{ $value->nama_tugas }}<br></td>
                                                <td>{!! $value->keterangan !!}</td>
                                                <td><a href="/pemberian-tugas-detail/{{ $value->id }}" class="btn btn-info">Detail</a></td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="card-footer text-center">
                                <a href="/pemberian-tugas" class="uppercase">Lihat Lebih Banyak Lagi</a>
                            </div>
                        </div>

                    @endif
            </div>
        </div>
    </div>


@endsection
@push('bottom-js')
    <!-- jQuery -->
    <script src="{{asset('template/')}}/plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('template/')}}/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template/')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="{{asset('template/')}}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template/')}}/dist/js/adminlte.js"></script>
@endpush

