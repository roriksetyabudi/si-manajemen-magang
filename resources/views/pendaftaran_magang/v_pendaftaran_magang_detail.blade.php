 @extends('template.v_template')
@section('title', 'Data Pendaftaran Magang')
@push('head-css')
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/adminlte.min.css">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/summernote/summernote-bs4.min.css">
@endpush
@section('content')
    <div class="container-fluid">

        <form method="post" action="/pendaftaran-magang/post/{{ $detailPendaftaranMagang->id }}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-3">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle"
                                     src="{{asset('storage/'.$detailPendaftaranMagang->fileuploadsPhotoPendaftaranMagang->path)}}"
                                     alt="User profile picture">
                            </div>
                            <h3 class="profile-username text-center">{{ $detailPendaftaranMagang->nama_lengkap_peserta_magang }}</h3>
                        </div>

                    </div>


                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Detail Instansi</h3>
                        </div>
                        <div class="card-body">
                            <strong><i class="fas fa-book mr-1"></i> Asal Instansi</strong>
                            <p class="text-muted">
                                {{ $detailPendaftaranMagang->nama_instansi }}
                            </p>
                            <hr>
                            <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat Instansi</strong>
                            <p class="text-muted">{{ $detailPendaftaranMagang->alamat_lengkap_instansi }}</p>
                            <hr><strong><i class="fas fa-pencil-alt mr-1"></i> Jurusan</strong>
                            <p class="text-muted">{{ $detailPendaftaranMagang->bidang_keahlian_peserta_magang }}</p>
                            <hr><strong><i class="fas fa-user-alt mr-1"></i> Pembimbing Sekolah</strong>
                            <p class="text-muted">{{ $detailPendaftaranMagang->nama_pembimbing_sekolah_peserta_magang }}</p>

                            <hr><strong><i class="fas fa-paperclip mr-1"></i> File Berkas</strong>
                            <p class="text-muted"><a href="{{ asset('storage/'.$detailPendaftaranMagang->fileuploads->path) }}" target="_blank">{{ $detailPendaftaranMagang->fileuploads->title }}</a></p>


                        </div>

                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card card-primary card-outline">


                        <div class="invoice p-3 mb-3">
                            <div class="row">
                                <div class="col-12">
                                    <h4>
                                        <i class="fas fa-globe"></i> VERIFIKASI PENDAFTARAN PESERTA MAGANG
                                        <small class="float-right">Tanggal: {{ date('d F y H:i:s', strtotime($detailPendaftaranMagang->created_at)) }}</small>
                                    </h4>
                                </div>
                                <!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Peserta Magang
                                    <address>
                                        <strong>{{ $detailPendaftaranMagang->nama_lengkap_peserta_magang }}</strong><br>
                                        {{ $detailPendaftaranMagang->alamat_lengkap_peserta_magang }}<br>
                                        Phone: {{ $detailPendaftaranMagang->telepon_peserta_magang }}<br>
                                        Email: {{ $detailPendaftaranMagang->email_peserta_magang }}
                                    </address>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Instansi
                                    <address>
                                        <strong>{{ $detailPendaftaranMagang->nama_instansi }}</strong><br>
                                        {{ $detailPendaftaranMagang->alamat_lengkap_instansi }}<br>
                                        Phone: {{ $detailPendaftaranMagang->telepon_instansi }}<br>
                                        <b>Pembimbing Sekolah:</b><br> {{ $detailPendaftaranMagang->nama_pembimbing_sekolah_peserta_magang }}
                                    </address>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <b>Kode Pendaftaran #{{ $detailPendaftaranMagang->kode_registrasi }}</b><br>
                                    <b>Tanggal Ajuan Mulai Magang:</b><br> {{ date('d F y', strtotime($detailPendaftaranMagang->tanggal_ajuan_mulai_magang)) }}<br>
                                    <b>Tanggal Ajuan Selesai Magang:</b><br> {{ date('d F y', strtotime($detailPendaftaranMagang->tanggal_ajuan_selesai_magang)) }}

                                </div>
                                <!-- /.col -->
                            </div>


                        </div>
                        <div class="card-header">
                            <h3 class="card-title">MANAGE DATA MAGANG</h3>
                        </div>

                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="col-md-12 row mb-2">

                                        <label for="inputPassword" class="col-sm-4 col-form-label">Mulai Magang</label>
                                        <div class="col-sm-8">
                                            <input type="date" class="form-control @error('tanggal_mulai_magang') is-invalid @enderror" id="tanggal_mulai_magang" name="tanggal_mulai_magang" value="{{ $detailPendaftaranMagang->tanggal_ajuan_mulai_magang }}">
                                            @error('tanggal_mulai_magang')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12 row mb-2">
                                        <label for="inputPassword" class="col-sm-4 col-form-label">P. Magang</label>
                                        <div class="col-sm-8">
                                            <select class="form-control @error('id_users_pembimbing_magang') is-invalid @enderror select2" name="id_users_pembimbing_magang" id="id_users_pembimbing_magang" style="width: 100%;">
                                                <option value="">Pilih</option>
                                                @foreach($pembimbingMagang as $pembimbingMagangs)
                                                    <option value="{{ $pembimbingMagangs->id }}">{{ $pembimbingMagangs->nama_lengkap }}</option>
                                                @endforeach
                                            </select>
                                            @error('id_users_pembimbing_magang')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12 row">
                                        <label for="inputPassword" class="col-sm-4 col-form-label">Status</label>
                                        <div class="col-sm-8">
                                            <select class="form-control @error('status') is-invalid @enderror select2" name="status" id="status" style="width: 100%;">
                                                <option value="">Pilih</option>
                                                <option value="PENGAJUAN" @if($detailPendaftaranMagang->status == "PENGAJUAN") selected @endif>PENGAJUAN</option>
                                                <option value="DITERIMA" @if($detailPendaftaranMagang->status == "DITERIMA") selected @endif>DITERIMA</option>
                                                <option value="DITOLAK" @if($detailPendaftaranMagang->status == "DITOLAK") selected @endif>DITOLAK</option>
                                            </select>
                                            @error('status')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12 row mb-2">
                                        <label for="inputPassword" class="col-sm-4 col-form-label">Selesai</label>
                                        <div class="col-sm-8">
                                            <input type="date" class="form-control @error('tanggal_selesai_magang') is-invalid @enderror" id="tanggal_selesai_magang" name="tanggal_selesai_magang" value="{{ $detailPendaftaranMagang->tanggal_ajuan_selesai_magang }}">
                                            @error('tanggal_selesai_magang')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12 row">
                                        <label for="inputPassword" class="col-sm-4 col-form-label">P. Sekolah</label>
                                        <div class="col-sm-8">
                                            <select class="form-control @error('id_pembimbing_sekolah') is-invalid @enderror select2" name="id_pembimbing_sekolah" id="id_pembimbing_sekolah" style="width: 100%;">
                                                <option value="">Pilih</option>
                                                @foreach($pembimbingSekolah as $pembimbingSekolahs)
                                                    <option value="{{ $pembimbingSekolahs->id }}">{{ $pembimbingSekolahs->nama_lengkap }}</option>
                                                @endforeach

                                            </select>
                                            @error('id_pembimbing_sekolah')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <hr>
                        <div class="card-header">
                            <h3 class="card-title">CATATAN KEPADA PESERTA MAGANG</h3>
                        </div>


                        <div class="card-body">
                            <div class="form-group">
                    <textarea id="compose-textarea"  name="keterangan"  class="form-control keterangan" style="height: 300px">
                        @if($detailPendaftaranMagang->keterangan == "" || $detailPendaftaranMagang->keterangan == null)
                            {{-- <h1><u>PEMBERITAHUAN PENGAJUAN MAGANG</u></h1>
                            <p>Kepada {{ $detailPendaftaranMagang->nama_lengkap_peserta_magang }} Terima Kasih Sudah Melakukan Pengajuan Magang Kepada Kami,
                            Ini adalah balasan email kami yang berisi tentang identitas diri anda, yang sudah di kirimkan kepada kami, </p> --}}
                        @else
                            {{ $detailPendaftaranMagang->keterangan }}
                        @endif


                    </textarea>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> PROSES</button>

                        </div>

                    </div>

                </div>
            </div>
        </form>


    </div>
@endsection
@push('bottom-js')
    <script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>
    <!-- Summernote -->
    <script src="{{asset('template')}}/plugins/summernote/summernote-bs4.min.js"></script>

    <script>
        $(function () {
            //Add text editor
            $('#compose-textarea').summernote()

            $('#status').change(function() {
                var value = $(this).val();

                if(value == "DITERIMA") {
                    $('#compose-textarea').summernote('reset');
                    $('#compose-textarea').summernote('pasteHTML', '<h1><u>PEMBERITAHUAN PENGAJUAN MAGANG</u></h1> <p>Kepada <b> {{ $detailPendaftaranMagang->nama_lengkap_peserta_magang }} </b> Terima Kasih Sudah Melakukan Pengajuan Magang Kepada Kami,Ini adalah balasan email kami yang berisi tentang identitas diri anda, yang sudah di kirimkan kepada kami, <b>SELAMAT ANDA DITERIMA MAGANG DI KANTOR POS</b> </p>');
                } else if(value == "DITOLAK") {
                    $('#compose-textarea').summernote('reset');
                    $('#compose-textarea').summernote('pasteHTML', '<h1><u>PEMBERITAHUAN PENGAJUAN MAGANG</u></h1> <p>Kepada <b> {{ $detailPendaftaranMagang->nama_lengkap_peserta_magang }} </b> Terima Kasih Sudah Melakukan Pengajuan Magang Kepada Kami,Ini adalah balasan email kami yang berisi tentang identitas diri anda, yang sudah di kirimkan kepada kami, <b>MOHON MAAF ANDA DITOLAK MAGANG DI KANTOR POS</b> </p>');
                } else {
                    $('#compose-textarea').summernote('reset');
                    $('#compose-textarea').summernote('pasteHTML', '<h1><u>PEMBERITAHUAN PENGAJUAN MAGANG</u></h1> <p>Kepada <b> {{ $detailPendaftaranMagang->nama_lengkap_peserta_magang }} </b> Terima Kasih Sudah Melakukan Pengajuan Magang Kepada Kami,Ini adalah balasan email kami yang berisi tentang identitas diri anda, yang sudah di kirimkan kepada kami, </p>');
                }

            });
        })
    </script>
@endpush
