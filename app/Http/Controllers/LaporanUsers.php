<?php

namespace App\Http\Controllers;

use App\Models\NilaiTrans;
use App\Models\User;
use Illuminate\Http\Request;
use PDF;

class LaporanUsers extends Controller
{
    public function index(){
        return view('laporan.data_users.v_laporan_data_users');
    }
    public function generate_pdf(Request $request) {
        $request->validate([
            'tanggal_mulai' => 'required|min:3',
            'tanggal_selesai' => 'required|min:3',
        ]);

        $users = User::where('deleted',0)
            ->whereDate('created_at', '>=', date('Y-m-d', strtotime($request->post('tanggal_mulai'))))
            ->whereDate('created_at', '<=', date('Y-m-d', strtotime($request->post('tanggal_selesai'))))->get();

        $view = view('laporan.data_users.cetak', [
            "detailUsers" => $users,
            "tanggal_mulai" => $request->tanggal_mulai,
            "tanggal_selesai" => $request->tanggal_selesai
        ]);
        $html_content = $view->render();
        PDF::SetAuthor('Rorik Setya Budi');
        PDF::SetTitle('Laporan Data Sistem Informasi Manajemen Magang');
        PDF::SetSubject("Laporan Data Users");
        PDF::SetKeywords('sistem informasi manajemen magang');
        PDF::setPrintHeader(false);
        PDF::setPrintFooter(false);
        PDF::SetMargins(7, 7, 7, 7);
        PDF::SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

        PDF::AddPage('L', 'A4');
        PDF::SetFont('helvetica', null, 11);
        PDF::writeHTML($html_content, true, false, true, false, '');
        PDF::Output('data_users.pdf');
    }
}
