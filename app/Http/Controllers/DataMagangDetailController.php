<?php

namespace App\Http\Controllers;

use App\Models\Absens;
use App\Models\AktifitasMagangs;
use App\Models\FileUploads;
use App\Models\Izins;
use App\Models\Magangs;
use App\Models\NilaiTrans;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DataMagangDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $detailMagangs = Magangs::find($id);
        return view('data_magang.v_data_magang_detail',[
            "absens" => Absens::where('deleted', 0)->where('id_users', $detailMagangs->id_users_magang)->orderBy('id', "DESC")->get(),
            "aktifitasMagangs" => AktifitasMagangs::where('deleted', 0)->where('id_users', $detailMagangs->id_users_magang)->orderBy('id', "DESC")->get(),
            'dataIzinMagangs' => Izins::where('deleted', 0)->where('id_users', $detailMagangs->id_users_magang)->orderBy('id', "DESC")->get(),
            'dataNilai' => NilaiTrans::where('deleted',0)->where('id_users', $detailMagangs->id_users_magang)->first(),
            'detailMagang' => Magangs::where('deleted', 0)->where('id', $id)->first(),
            'id' => $id

        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function upload_file_berkas_tugas_akhir_magang(Request $request){
        $request->validate([
            'id_magang' => 'required',
            'file_berkas_tugas_akhir_magang' => 'required|mimes:pdf|max:10000',
        ]);
        $countDetailDataMagangs = Magangs::where('id', $request->id_magang)->get()->count();
        if($countDetailDataMagangs > 0) {
            $DetailDataMagangs = Magangs::find($request->id_magang);
            if($request->file('file_berkas_tugas_akhir_magang')) {

                $CountDetailFileUploads = FileUploads::where('id', $DetailDataMagangs->file_berkas_tugas_akhir_magang)->get()->count();
                if($CountDetailFileUploads > 0) {
                    $DetailFileUploads = FileUploads::find($DetailDataMagangs->file_berkas_tugas_akhir_magang);
                    Storage::delete($DetailFileUploads->path);
                }
                //FILE UPLOAD
                $update['file_uploads']['title'] = "Berkas Tugas Akhir Magang". $DetailDataMagangs->user->nama_lengkap;
                $update['file_uploads']['filename'] = $request->file('file_berkas_tugas_akhir_magang')->hashName();
                $update['file_uploads']['path'] = $request->file('file_berkas_tugas_akhir_magang')->store('file-uploads/documents');
                $update['file_uploads']['type'] = $request->file('file_berkas_tugas_akhir_magang')->extension();
                $update['file_uploads']['size'] = $request->file('file_berkas_tugas_akhir_magang')->getSize();
                if($CountDetailFileUploads > 0) {
                    FileUploads::where('id', $DetailDataMagangs->file_berkas_tugas_akhir_magang)->update($update['file_uploads']);
                } else {
                    $queryInsertFileUploads = FileUploads::create($update['file_uploads']);
                    $resultIdFileUploadPhotos = $queryInsertFileUploads->id;
                    $update['magangs']['file_berkas_tugas_akhir_magang'] = $resultIdFileUploadPhotos;
                    Magangs::where('id', $request->id_magang)->update($update['magangs']);
                }
            }
            return redirect('/data-magang-detail/'.$request->id_magang)->with('success', 'File Tugas Magang Sukses Di Upload');
        } else {
            return redirect('/data-magang-detail/'.$request->id_magang)->with('fail', 'Data Magang Tidak Tersedia');
        }
    }
}
