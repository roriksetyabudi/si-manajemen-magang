<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Izins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('izins', function (Blueprint $table) {
            $table->id();
            $table->smallInteger('id_users')->index();

            $table->date("tanggal_izin")->nullable(true);
            $table->date("tanggal_selesai_izin")->nullable(true);
            $table->text("alasan_izin")->nullable(true);
            $table->smallInteger("file_bukti_izin")->default(0);
            $table->smallInteger("id_users_approval")->default(0)->index();
            $table->enum("is_approval", ["Y","N"])->default("N");
            $table->dateTime("is_approval_at")->nullable(true);
            $table->text("keterangan_approval")->nullable(true);
            $table->enum("status",["Pengajuan","Disetujui","Ditolak"])->default("Pengajuan");
            $table->tinyInteger("deleted")->default(0)->comment("0=Not Deleted, 1=deleted");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('izins');
    }
}
