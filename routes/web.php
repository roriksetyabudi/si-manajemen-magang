<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\MagangRegistrasiController;
use \App\Http\Controllers\LoginController;
use \App\Http\Controllers\HomeController;
use \App\Http\Controllers\UsersController;
use \App\Http\Controllers\PendaftaranMagangController;
use \App\Http\Controllers\RegistrasiAkunController;
use \App\Http\Controllers\DataMagangController;
use \App\Http\Controllers\DataMagangDetailController;
use \App\Http\Controllers\AbsensController;
use \App\Http\Controllers\AktifitasMagangsController;
use \App\Http\Controllers\NilaiTransController;
use \App\Http\Controllers\IzinController;
use \App\Http\Controllers\UbahAkunController;
use \App\Http\Controllers\LaporanMagangs;
use \App\Http\Controllers\LaporanAbsensi;
use \App\Http\Controllers\LaporanAktifitasMagang;
use \App\Http\Controllers\LaporanIzin;
use \App\Http\Controllers\LaporanNilai;
use \App\Http\Controllers\LaporanUsers;
use \App\Http\Controllers\GeneralSettingsController;
use \App\Http\Controllers\NilaiMagangCetak;
use \App\Http\Controllers\SertifikatMagang;
use \App\Http\Controllers\LaporanAbsensiPesertaMagang;
use App\Http\Controllers\LupaPasswordController;
use App\Http\Controllers\PemberianTugasController;
use App\Http\Controllers\PemberianTugasDetailController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'index'])->middleware('auth');
Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::get('/logout', [LoginController::class, 'logout']);
Route::resource('/registrasi-magang', MagangRegistrasiController::class);
Route::resource('/registrasi-magang/post', MagangRegistrasiController::class);
Route::get('/home', [HomeController::class, 'index'])->middleware('auth');
Route::resource('/data-users', UsersController::class)->middleware('auth');
Route::resource('/data-users/post', UsersController::class)->middleware('auth');
Route::resource('/data-users/{id}/edit', UsersController::class)->middleware('auth');
Route::get('/data-users/blokir/{id}/{blokir}', [UsersController::class, 'blokir'])->middleware('auth');

Route::resource('/pendaftaran-magang', PendaftaranMagangController::class)->middleware('auth');
Route::resource('/pendaftaran-magang/post', PendaftaranMagangController::class)->middleware('auth');
Route::resource('/pendaftaran-magang/{id}/edit', PendaftaranMagangController::class)->middleware('auth');

Route::resource('/registrasi-akun', RegistrasiAkunController::class);
Route::resource('/registrasi-akun/post', RegistrasiAkunController::class);
Route::resource('/data-magang', DataMagangController::class)->middleware('auth');
Route::resource('/data-magang/post', DataMagangController::class)->middleware('auth');
Route::resource('/data-magang-detail', DataMagangDetailController::class)->middleware('auth');

Route::get('/absensi-magang', [AbsensController::class, 'index'])->middleware('auth');
Route::get('absens', [AbsensController::class, 'index'])->name('absens.index')->middleware('auth');
Route::resource('/absensi-magang', AbsensController::class)->middleware('auth');
Route::resource('/absensi-magang/post', AbsensController::class)->middleware('auth');
Route::resource('/aktifitas-magang', AktifitasMagangsController::class)->middleware('auth');
Route::resource('/aktifitas-magang/post', AktifitasMagangsController::class)->middleware('auth');
Route::resource('/aktifitas-magang/{id}/edit', AktifitasMagangsController::class)->middleware('auth');
Route::get('aktifitas_magangs', [AktifitasMagangsController::class, 'index'])->name('aktifitas_magangs.index')->middleware('auth');
Route::get('delete-aktifitas-magang/{id}', [AktifitasMagangsController::class, 'destroy'])->middleware('auth');
Route::get('aktifitas-magang-detail/{id}', [AktifitasMagangsController::class, 'detail'])->middleware('auth');

Route::resource('/nilai-magang', NilaiTransController::class)->middleware('auth');
Route::get('nilai_trans', [NilaiTransController::class, 'index'])->name('nilai_trans.index')->middleware('auth');
Route::post('nilai_trans', [NilaiTransController::class, 'set_nilai_with_ajax'])->name('nilai_trans.set_nilai_with_ajax')->middleware('auth');

Route::resource('/data-izin', \App\Http\Controllers\IzinController::class)->middleware('auth');
Route::resource('/data-izin/post', \App\Http\Controllers\IzinController::class)->middleware('auth');
Route::resource('/data-izin/{id}/edit', \App\Http\Controllers\IzinController::class)->middleware('auth');

Route::get('/data-izin/soft_delete/{id}', [IzinController::class,'soft_delete'])->middleware('auth');

Route::get('izins', [IzinController::class, 'index'])->name('izins.index')->middleware('auth');
Route::get('/data-izin-detail/{id}', [IzinController::class, 'izins_detail'])->middleware('auth');
Route::post('/data-izin-detail/action', [IzinController::class, 'izin_action'])->middleware('auth');

Route::get('/laporan-data-magang', [LaporanMagangs::class, 'index'])->middleware('auth');
Route::post('/laporan-data-magang/generate', [LaporanMagangs::class, 'generate_pdf'])->middleware('auth');

Route::get('/laporan-absensi-magang', [LaporanAbsensi::class, 'index'])->middleware('auth');
Route::post('/laporan-absensi-magang/generate', [LaporanAbsensi::class, 'generate_pdf'])->middleware('auth');

Route::get('/laporan-absensi-peserta-magang', [LaporanAbsensiPesertaMagang::class, 'index'])->middleware('auth');
Route::post('/laporan-absensi-peserta-magang/generate', [LaporanAbsensiPesertaMagang::class, 'generate'])->middleware('auth');


Route::get('/laporan-aktifitas-magang', [LaporanAktifitasMagang::class, 'index'])->middleware('auth');
Route::post('/laporan-aktifitas-magang/generate', [LaporanAktifitasMagang::class, 'generate_pdf'])->middleware('auth');

Route::get('/laporan-izin-magang', [LaporanIzin::class, 'index'])->middleware('auth');
Route::post('/laporan-izin-magang/generate', [LaporanIzin::class, 'generate_pdf'])->middleware('auth');

Route::get('/laporan-nilai-magang', [LaporanNilai::class, 'index'])->middleware('auth');
Route::post('/laporan-nilai-magang/generate', [LaporanNilai::class, 'generate_pdf'])->middleware('auth');

Route::get('/laporan-data-users', [LaporanUsers::class, 'index'])->middleware('auth');
Route::post('/laporan-data-users/generate', [LaporanUsers::class, 'generate_pdf'])->middleware('auth');

Route::resource('/ubah-akun', UbahAkunController::class)->middleware('auth');
Route::resource('/ubah-akun/post', UbahAkunController::class)->middleware('auth');

Route::resource('/general-settings', GeneralSettingsController::class)->middleware('auth');
Route::resource('/general-settings/post', GeneralSettingsController::class)->middleware('auth');
Route::get('/absensi-magang/set-approval/{id}', [AbsensController::class,'set_approval'])->middleware('auth');

Route::get('/cetak-nilai-magang/{id}', [NilaiMagangCetak::class, 'index'])->middleware('auth');
Route::get('/cetak-sertifikat-magang/{id}', [SertifikatMagang::class, 'index'])->middleware('auth');

Route::get('/lupa-password', [LupaPasswordController::class, 'index']);
Route::resource('/lupa-password/post', LupaPasswordController::class);
Route::get('/lupa-password/reset_hash', [LupaPasswordController::class,'reset_hash']);

Route::resource('/pemberian-tugas', PemberianTugasController::class)->middleware('auth');
Route::get('pemberian_tugas', [PemberianTugasController::class, 'index'])->name('pemberian_tugas.index')->middleware('auth');
Route::resource('/pemberian-tugas/post', PemberianTugasController::class)->middleware('auth');
Route::resource('/pemberian-tugas/{id}/edit', PemberianTugasController::class)->middleware('auth');
Route::get('delete-pemberian-tugas/{id}', [PemberianTugasController::class, 'destroy'])->middleware('auth');
Route::get('/pemberian-tugas-detail/{id}', [PemberianTugasController::class,'detail'])->middleware('auth');
Route::resource('/pemberian-tugas-detail/post', PemberianTugasDetailController::class)->middleware('auth');
Route::get('delete-pemberian-tugas-detail/{id}', [PemberianTugasDetailController::class, 'destroy'])->middleware('auth');

Route::post('/upload-file-berkas-tugas-akhir-magang', [DataMagangDetailController::class, 'upload_file_berkas_tugas_akhir_magang'])->middleware('auth');
