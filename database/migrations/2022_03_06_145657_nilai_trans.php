<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NilaiTrans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai_trans', function (Blueprint $table) {
            $table->id();
            $table->smallInteger('id_users')->index()->unique();
            $table->string("no_sertifikat")->default("");
            $table->float("kedisiplinan")->default(0);
            $table->float("sikap")->default(0);
            $table->float("komunikasi")->default(0);
            $table->float("kerapian")->default(0);
            $table->float("kerjasama")->default(0);
            $table->float("motivasi")->default(0);
            $table->float("penguasaan_terhadap_pekerjaan")->default(0);
            $table->float("hasil_pekerjaan")->default(0);
            $table->float("ide_atau_gagasan")->default(0);
            $table->float("tanggung_jawab")->default(0);
            $table->float("kejujuran")->default(0);
            $table->float("rata_rata")->default(0);
            $table->tinyInteger("deleted")->default(0)->comment("0=Not Deleted, 1=deleted");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai_trans');
    }
}
