@extends('template.v_template')
@section('title', 'Laporan Data Magang')
@push('head-css')
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/adminlte.min.css">
@endpush
@section('content')
    <div class="card">
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
            </symbol>
            <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
            </symbol>
            <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
            </symbol>
        </svg>
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4>Data Magang</h4>
                    </div>
                    @if(auth()->user()->privileges == "ADMIN" || auth()->user()->privileges == "PEMBIMBING SEKOLAH")
                        <div class="col-sm-6 text-right">
                            <a href="/data-magang/create" class="btn bg-gradient-primary">Tambah Data Magang</a>
                        </div>
                    @endif
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session()->has('success'))
                <div class="alert alert-success d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                    <div>
                        {{ session('success') }}
                    </div>
                </div>
            @endif
            <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr class="bg-info">
                    <th>Nama</th>
                    <th>P.Magang</th>
                    <th>P.Sekolah</th>
                    <th>Mulai Magang</th>
                    <th>Selesai Magang</th>
                    <th>Instansi</th>
                    <th>Jurusan</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @foreach($magangs as $magang)
                    {!!
                            $tanggal_akhir = '';
                            $tgl1 = new DateTime($magang->tanggal_selesai_magang);
                            $tgl2 = new DateTime(date('Y-m-d'));
                            $jarak = $tgl2->diff($tgl1);
 !!}
                    <tr>
                        <td>
                            <p><i class="fas fa-user mr-1"></i> {{ $magang->userMagang->nama_lengkap }}</p>
                            @if(date('Y-m-d') > $magang->tanggal_selesai_magang)
                                <span class="badge bg-success float-left">Sudah Selesai</span>

                            @else
                                <span class="badge bg-info float-left">Belum Selesai</span>
                            @endif

                        </td>
                        <td>{{ $magang->usersPembimbingmagang->nama_lengkap }}</td>
                        <td>{{ $magang->usersPembimbingSekolah->nama_lengkap }}</td>

                        <td>{{ date('d F Y', strtotime($magang->tanggal_mulai_magang)) }}</td>

                        <td>{{ date('d F Y', strtotime($magang->tanggal_selesai_magang)) }}<br>
                            @if($jarak->format('%R%a') == -7) <span class="badge bg-warning float-left">Hampir Selesai</span>
                            @elseif($jarak->format('%R%a') == -6) <span class="badge bg-warning float-left">Hampir Selesai</span>
                            @elseif($jarak->format('%R%a') == -5) <span class="badge bg-warning float-left">Hampir Selesai</span>
                            @elseif($jarak->format('%R%a') == -4) <span class="badge bg-warning float-left">Hampir Selesai</span>
                            @elseif($jarak->format('%R%a') == -3) <span class="badge bg-warning float-left">Hampir Selesai</span>
                            @elseif($jarak->format('%R%a') == -2) <span class="badge bg-warning float-left">Hampir Selesai</span>
                            @elseif($jarak->format('%R%a') == -1) <span class="badge bg-warning float-left">Hampir Selesai</span>
                            @elseif($jarak->format('%R%a') == -0) <span class="badge bg-success float-left">Hampir Selesai</span>
                            @endif</td>
                        <td>{{ $magang->instansis->nama_instansi }}</td>
                        <td>{{ $magang->jurusan_bidang_keahlihan }}</td>
                        <td class="text-center">
                            <a href="/data-magang-detail/{{ $magang->id }}" class="btn btn-outline-info btn-sm">Detail</a>
                        </td>
                    </tr>
                @endforeach
                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection
@push('bottom-js')
    <script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{asset('template')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/jszip/jszip.min.js"></script>
    <script src="{{asset('template')}}/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{asset('template')}}/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endpush
