<?php

namespace App\Http\Controllers;

use App\Models\AktifitasMagangs;
use App\Models\FileUploads;
use App\Models\Magangs;
use App\Models\PemberianTugas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;
use App\Models\PemberianTugasDetail;

class PemberianTugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $dataPemberianTugas = PemberianTugas::leftJoin('users', 'pemberian_tugas.id_users_diberi_tugas', '=', 'users.id')
                ->leftJoin('file_uploads', 'pemberian_tugas.id_file_tugas', '=', 'file_uploads.id')
                ->select(['pemberian_tugas.id','pemberian_tugas.id_users_pemberi_tugas','pemberian_tugas.id_users_diberi_tugas','pemberian_tugas.nama_tugas',
                    'pemberian_tugas.keterangan','pemberian_tugas.id_file_tugas','pemberian_tugas.target','pemberian_tugas.created_at',
                    'users.nama_lengkap as users_diberi_tugas','file_uploads.title','file_uploads.filename','file_uploads.path','file_uploads.type'])
                ->where(function ($query) {
                    if(auth()->user()->privileges == "USERS MAGANG") {
                        $query->where('pemberian_tugas.id_users_diberi_tugas', auth()->user()->id);
                    } else if(auth()->user()->privileges == "PEMBIMBING MAGANG") {
                        $query->where('pemberian_tugas.id_users_pemberi_tugas', auth()->user()->id);
                    }
                })->get();

            return DataTables::of($dataPemberianTugas)
                ->addIndexColumn('pemberian_tugas')
                ->filter(function ($instance) use ($request) {
                    if(!empty($request->get('search'))){
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            if (Str::contains(Str::lower($row['nama_tugas']), Str::lower($request->get('search')))){
                                return true;
                            }
                            if (Str::contains(Str::lower($row['keterangan']), Str::lower($request->get('search')))){
                                return true;
                            }
                            return false;
                        });
                    }
                })
                ->addColumn('user', function (PemberianTugas $pemberianTugas){
                    return $pemberianTugas->users_diberi_tugas;
                })
                ->addColumn('nama_tugas', function (PemberianTugas $pemberianTugas){

                    $pemberianTugasDetail = PemberianTugasDetail::where('id_tugas', $pemberianTugas->id)->get();
                    $realisasi = 0;
                    foreach ($pemberianTugasDetail as $valuePemberianTugasDetail) {
                        $realisasi += $valuePemberianTugasDetail['target'];
                    }


                    $nama_tugas = $pemberianTugas->nama_tugas;
                    $nama_tugas .= '<br>';
                    $nama_tugas .='<div class="progress">
                                         <div class="progress-bar" role="progressbar" style="width: '.$realisasi.'%;" aria-valuenow="'.$realisasi.'" aria-valuemin="0" aria-valuemax="'.$pemberianTugas->target.'">'.$realisasi.'"%</div>
                                    </div>';
                    return $nama_tugas;
                })
                ->addColumn('keterangan', function (PemberianTugas $pemberianTugas){
                    return $pemberianTugas->keterangan;
                })
                ->addColumn('lampiran', function (PemberianTugas $pemberianTugas){
                    return '<a href="'.url('/storage/'.$pemberianTugas->path).'" target="blank">'.$pemberianTugas->title.'</a>';
                })
                ->addColumn('tanggal_dibuat', function (PemberianTugas $pemberianTugas){
                    return date('d F y H:i:s', strtotime($pemberianTugas->created_at));
                })
                ->addColumn('target', function (PemberianTugas $pemberianTugas){
                    return $pemberianTugas->target."%";
                })
                ->addColumn('realisasi', function (PemberianTugas $pemberianTugas){
                    $pemberianTugasDetail = PemberianTugasDetail::where('id_tugas', $pemberianTugas->id)->get();
                    $realisasi = 0;
                    foreach ($pemberianTugasDetail as $valuePemberianTugasDetail) {
                        $realisasi += $valuePemberianTugasDetail['target'];
                    }
                    return $realisasi."%";
                })

                ->addColumn('action', function (PemberianTugas $pemberianTugas) {
                    $id = $pemberianTugas->id;
                    if(auth()->user()->privileges == "PEMBIMBING MAGANG") {
                        $btn = '<div class="btn-group">
                                    <button type="button" class="btn btn-warning">Aksi</button>
                                    <button type="button" class="btn btn-warning dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                    <span class="sr-only"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                    <a class="dropdown-item" href="/pemberian-tugas-detail/' . $id . '">Detail</a>
                                    <a class="dropdown-item" href="/pemberian-tugas/' . $id . '/edit">Edit</a>
                                    <a class="dropdown-item" href="/delete-pemberian-tugas/' . $id . '">Hapus</a>

                                    </div>
                                    </div>';
                    } else if(auth()->user()->privileges == "USERS MAGANG") {
                        $btn = '<a href="/pemberian-tugas-detail/'.$id.'" class="btn btn-info">Updated</a>';
                    } else {
                        $btn = 'ReadOnly';
                    }
                    return $btn;
                })
                ->rawColumns(['action','user','nama_tugas','keterangan','lampiran','tanggal_dibuat'])
                ->make(true);

        }
        return view('pemberian_tugas.v_pemberian_tugas',[
            "id_users" => auth()->user()->id,
            "privileges" => auth()->user()->privileges
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pemberian_tugas.v_pemberian_tugas_add',[
            "id_users" => auth()->user()->id,
            "privileges" => auth()->user()->privileges,
            "magangs" => Magangs::where('id_users_pembimbing_magang', auth()->user()->id)->whereDate('tanggal_selesai_magang', '>=', date('Y-m-d'))->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_users_diberi_tugas' => 'required',
            'nama_tugas' => 'required',
            'keterangan' => 'required',
            'target' => 'required',
            'id_file_tugas' => 'mimes:pdf,image,xlsx,csv,xls,zip,doc,docx'
        ]);
        //FILE UPLOAD IMAGES PHOTO USER
        $resultIdFileUpload = 0;
        if($request->file('id_file_tugas')) {
            $insert['file_uploads']['title'] = "Lampiran Pemberian Tugas ". $request->nama_tugas;
            $insert['file_uploads']['filename'] = $request->file('id_file_tugas')->hashName();
            $insert['file_uploads']['path'] = $request->file('id_file_tugas')->store('file-uploads/documents');
            $insert['file_uploads']['type'] = $request->file('id_file_tugas')->extension();
            $insert['file_uploads']['size'] = $request->file('id_file_tugas')->getSize();
            $queryInsertFileUploads = FileUploads::create($insert['file_uploads']);
            $resultIdFileUpload = $queryInsertFileUploads->id;
        }
        $insert['pemberian_tugas']['id_users_pemberi_tugas'] = auth()->user()->id;
        $insert['pemberian_tugas']['id_users_diberi_tugas'] = $request->id_users_diberi_tugas;
        $insert['pemberian_tugas']['nama_tugas'] = $request->nama_tugas;
        $insert['pemberian_tugas']['keterangan'] = $request->keterangan;
        $insert['pemberian_tugas']['id_file_tugas'] = $resultIdFileUpload;
        $insert['pemberian_tugas']['target'] = $request->target;
        $insert['pemberian_tugas']['created_at'] = date('Y-m-d H:i:s');
        $insert['pemberian_tugas']['updated_at'] = date('Y-m-d H:i:s');

        PemberianTugas::create($insert['pemberian_tugas']);
        return redirect('/pemberian-tugas')->with('success', 'Sukses Membuat Tugas Baru');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pemberianTugas = PemberianTugas::find($id);
        $fileUploadsDetail = FileUploads::find($pemberianTugas->id_file_tugas);
        return view('pemberian_tugas.v_pemberian_tugas_edit',[
            'pemberianTugas' => $pemberianTugas,
            'fileUploadsDetail' => $fileUploadsDetail,
            "magangs" => Magangs::where('id_users_pembimbing_magang', auth()->user()->id)->whereDate('tanggal_selesai_magang', '>=', date('Y-m-d'))->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'id_users_diberi_tugas' => 'required',
            'nama_tugas' => 'required',
            'keterangan' => 'required',
            'target' => 'required',
            'id_file_tugas' => 'mimes:pdf,image,xlsx,csv,xls,zip,doc,docx'
        ]);
        $pemberianTugas = PemberianTugas::find($id);
        if($pemberianTugas->id_file_tugas != 0) {
            $detailFileUploads = FileUploads::find($pemberianTugas->id_file_tugas);
        }
        if($request->file('id_file_tugas')) {

            if($pemberianTugas->id_file_tugas == 0) {
                $resultIdFileUpload = 0;
                if($request->file('id_file_tugas')) {
                    $insert['file_uploads']['title'] = "Lampiran Pemberian Tugas ". $request->nama_tugas;
                    $insert['file_uploads']['filename'] = $request->file('id_file_tugas')->hashName();
                    $insert['file_uploads']['path'] = $request->file('id_file_tugas')->store('file-uploads/documents');
                    $insert['file_uploads']['type'] = $request->file('id_file_tugas')->extension();
                    $insert['file_uploads']['size'] = $request->file('id_file_tugas')->getSize();
                    $queryInsertFileUploads = FileUploads::create($insert['file_uploads']);
                    $resultIdFileUpload = $queryInsertFileUploads->id;
                    $insert['pemberian_tugas']['id_file_tugas'] = $resultIdFileUpload;
                }
            } else {
                //delete image old
                Storage::delete($detailFileUploads->path);
                $update['file_uploads']['title'] = "Lampiran Pemberian Tugas ". $request->nama_tugas;
                $update['file_uploads']['filename'] = $request->file('id_file_tugas')->hashName();
                $update['file_uploads']['path'] = $request->file('id_file_tugas')->store('file-uploads/documents');
                $update['file_uploads']['type'] = $request->file('id_file_tugas')->extension();
                $update['file_uploads']['size'] = $request->file('id_file_tugas')->getSize();
                FileUploads::where('id', $pemberianTugas->id_file_tugas)->update($update['file_uploads']);
            }


        }
        $insert['pemberian_tugas']['id_users_pemberi_tugas'] = auth()->user()->id;
        $insert['pemberian_tugas']['id_users_diberi_tugas'] = $request->id_users_diberi_tugas;
        $insert['pemberian_tugas']['nama_tugas'] = $request->nama_tugas;
        $insert['pemberian_tugas']['keterangan'] = $request->keterangan;
        $insert['pemberian_tugas']['target'] = $request->target;
        $insert['pemberian_tugas']['updated_at'] = date('Y-m-d H:i:s');

        PemberianTugas::where('id', $id)->update($insert['pemberian_tugas']);
        return redirect('/pemberian-tugas')->with('success', 'Sukses Memperbaharui Tugas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pemberianTugas = PemberianTugas::find($id);
        if($pemberianTugas->id_file_tugas != 0) {
            $detailFileUploads = FileUploads::find($pemberianTugas->id_file_tugas);
            Storage::delete($detailFileUploads->path);
        }

        DB::table('pemberian_tugas_detail')->where('id_tugas', '=', $id)->delete();
        DB::table('pemberian_tugas')->where('id', '=', $id)->delete();


        return redirect('/pemberian-tugas')->with('success', 'Sukses Menghapus Data Pemberian Tugas');
    }

    public function detail($id) {
        $pemberianTugas = PemberianTugas::find($id);
        $fileUploadsDetail = FileUploads::find($pemberianTugas->id_file_tugas);

        if(auth()->user()->privileges == "PEMBIMBING MAGANG") {
            $pemberianTugasDetail = PemberianTugasDetail::where('id_tugas', $id)->orderBy('id', 'DESC')->get();
        } else if(auth()->user()->privileges == "USERS MAGANG") {
            $pemberianTugasDetail = PemberianTugasDetail::where('id_tugas', $id)->where('id_users_created', auth()->user()->id)->orderBy('id', 'DESC')->get();
        }

        return view('pemberian_tugas.v_pemberian_tugas_detail',[
            'pemberianTugas' => $pemberianTugas,
            'fileUploadsDetail' => $fileUploadsDetail,
            'pemberianTugasDetail' =>$pemberianTugasDetail
        ]);
    }
}
