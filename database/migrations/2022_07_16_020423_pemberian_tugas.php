<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PemberianTugas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemberian_tugas', function (Blueprint $table) {
           $table->id();
            $table->integer('id_users_pemberi_tugas')->nullable(false)->default(0)->comment('id users pemberi tugas');
            $table->integer('id_users_diberi_tugas')->nullable(false)->default(0)->comment('id users yang diberi tugas');
            $table->string('nama_tugas', 100)->nullable(true)->comment('nama tugas diberikan');
            $table->text('keterangan')->nullable(true)->comment('keterangan atau deskripsi tugas');
            $table->integer('id_file_tugas')->nullable(false)->default(0)->comment('id file  tugas dari pemberi tugas');
            $table->tinyInteger('target')->nullable(false)->default(0)->comment('target persentase yang diharapkan oleh pemberi tugas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemberian_tugas');
    }
}
