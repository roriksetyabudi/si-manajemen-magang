<?php

namespace App\Http\Controllers;

use App\Models\Magangs;
use Illuminate\Http\Request;
use PDF;
use App\Models\Absens;
class LaporanAbsensi extends Controller
{
    public function index(){
        return view('laporan.absensi_magang.v_laporan_absensi_magang');
    }
    public function generate_pdf(Request $request) {
        $request->validate([
            'tanggal_mulai' => 'required|min:3',
            'tanggal_selesai' => 'required|min:3',
        ]);
        if(auth()->user()->privileges == "PEMBIMBING SEKOLAH") {
            $absens = Absens::leftJoin('magangs', 'magangs.id_users_magang', '=', 'absens.id_users')
                ->leftJoin('users', 'absens.id_users', '=', 'users.id')
                ->select(['absens.id as id_absen','absens.id_users','users.nama_lengkap','absens.tanggal_masuk','absens.tanggal_pulang','absens.status',
                    'absens.file_bukti_absensi','absens.deleted','absens.updated_at as updated_at_absens'])
                ->whereNotNull('magangs.id_users_magang')
                ->where('id_pembimbing_sekolah', auth()->user()->id)
                ->whereDate('absens.created_at', '>=', date('Y-m-d', strtotime($request->post('tanggal_mulai'))))
                ->whereDate('absens.created_at', '<=', date('Y-m-d', strtotime($request->post('tanggal_selesai'))))->get();
        } else if(auth()->user()->privileges == "PEMBIMBING MAGANG") {
            $absens = Absens::leftJoin('magangs', 'magangs.id_users_magang', '=', 'absens.id_users')
                ->leftJoin('users', 'absens.id_users', '=', 'users.id')
                ->select(['absens.id as id_absen','absens.id_users','users.nama_lengkap','absens.tanggal_masuk','absens.tanggal_pulang','absens.status',
                    'absens.file_bukti_absensi','absens.deleted','absens.updated_at as updated_at_absens'])
                ->whereNotNull('magangs.id_users_magang')
                ->where('id_users_pembimbing_magang', auth()->user()->id)
                ->whereDate('absens.created_at', '>=', date('Y-m-d', strtotime($request->post('tanggal_mulai'))))
                ->whereDate('absens.created_at', '<=', date('Y-m-d', strtotime($request->post('tanggal_selesai'))))->get();
        } else if(auth()->user()->privileges == "ADMIN") {
            $absens = Absens::leftJoin('magangs', 'magangs.id_users_magang', '=', 'absens.id_users')
                ->leftJoin('users', 'absens.id_users', '=', 'users.id')
                ->select(['absens.id as id_absen','absens.id_users','users.nama_lengkap','absens.tanggal_masuk','absens.tanggal_pulang','absens.status',
                    'absens.file_bukti_absensi','absens.deleted','absens.updated_at as updated_at_absens'])
                ->whereNotNull('magangs.id_users_magang')
                ->whereDate('absens.created_at', '>=', date('Y-m-d', strtotime($request->post('tanggal_mulai'))))
                ->whereDate('absens.created_at', '<=', date('Y-m-d', strtotime($request->post('tanggal_selesai'))))->get();
        }
        $view = view('laporan.absensi_magang.cetak', [
            "detailAbsens" => $absens,
            "tanggal_mulai" => $request->tanggal_mulai,
            "tanggal_selesai" => $request->tanggal_selesai
        ]);
        $html_content = $view->render();
        PDF::SetAuthor('Rorik Setya Budi');
        PDF::SetTitle('Laporan Data Sistem Informasi Manajemen Magang');
        PDF::SetSubject("Laporan Absensi");
        PDF::SetKeywords('sistem informasi manajemen magang');
        PDF::setPrintHeader(false);
        PDF::setPrintFooter(false);
        PDF::SetMargins(7, 7, 7, 7);
        PDF::SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

        PDF::AddPage('L', 'A4');
        PDF::SetFont('helvetica', null, 11);
        PDF::writeHTML($html_content, true, false, true, false, '');
        PDF::Output('data_absensi.pdf');
    }

}
