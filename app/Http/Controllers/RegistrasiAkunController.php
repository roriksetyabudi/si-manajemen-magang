<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MagangRegistration;
use App\Models\User;
use App\Models\Magangs;

class RegistrasiAkunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('extras.registrasi_akun.v_registrasi_akun');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_verifikasi' => 'required|min:3',
//            'username' => 'required|min:3|unique:users',
//            'password' => 'required|same:ulangi_password',
//            'ulangi_password' => 'required',
            'email' => 'required',
//            'telepon' => 'required|unique:users',
        ]);

        $resultCountKodeVerifikasi = MagangRegistration::where('kode_registrasi', $request->kode_verifikasi)->where('email_peserta_magang', $request->email)->get()->count();
        //$resultCountEmail = MagangRegistration::where('email_peserta_magang', $request->email)->get()->count();
        if($resultCountKodeVerifikasi > 0) {
            $resultKodeVerifikasi = MagangRegistration::where('kode_registrasi', $request->kode_verifikasi)->first();
            //SET INSERT DATA USERS
//            $insert['users']['username'] = $request->username;
//            $insert['users']['password'] = bcrypt($request->password);
//            $insert['users']['nama_lengkap'] = $resultKodeVerifikasi->nama_lengkap_peserta_magang;
//            $insert['users']['alamat'] = $resultKodeVerifikasi->alamat_lengkap_peserta_magang;
//            $insert['users']['gender'] = $resultKodeVerifikasi->jenis_kelamin_peserta_magang;
//            $insert['users']['email'] = $request->email;
//            $insert['users']['telepon'] =$request->telepon;
//            $insert['users']['photo'] = $resultKodeVerifikasi->fupload_berkas_foto_peserta_magang;
//            $insert['users']['privileges'] = 'USERS MAGANG';
//            $insert['users']['is_peserta_magang'] = 'Y';
            $insert['users']['pending'] = 1;
            $insert['users']['disabled'] = 1;
//            $insert['users']['kode_verifikasi'] = $request->kode_verifikasi;
//            $insert['users']['kode_verifikasi_at'] = date('Y-m-d H:i:s');
//            $resultInsertUser = User::create($insert['users']);
            User::where('email', $request->email)->update($insert['users']);
//            $resultIdUsers = $resultInsertUser->id;
//            //SET UPDATE MAGANGS
//            $update['magangs']['id_users_magang'] = $resultIdUsers;
//            Magangs::where('kode_pendaftaran_magang', $request->kode_verifikasi)->update($update['magangs']);
//            //SET SOFT DELERE REGISTRASI MAGANG
//            $update['magang_registrations']['deleted'] = 1;
//            MagangRegistration::where('kode_registrasi', $request->kode_verifikasi)->update($update['magang_registrations']);
            return redirect('/registrasi-akun')->with('success', 'Aktivasi Sukses, Silahkan Masuk Login menggunakan Username dan Password Anda');
        } else {
            return redirect('/registrasi-akun')->with('fail', 'Aktivasi Gagal Dilakukan, Silahkan Periksa Kembali Kode Verfikasi atau Email Anda');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
