<?php

namespace App\Http\Controllers;

use App\Models\Absens;
use App\Models\AktifitasMagangs;
use App\Models\FileUploads;
use App\Models\Izins;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;


class IzinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $dataIzin = Izins::leftJoin('users', 'izins.id_users', '=', 'users.id')
                ->leftJoin('magangs', 'magangs.id_users_magang', '=', 'izins.id_users')
                ->leftJoin('instansis', 'instansis.id', '=', 'magangs.id_instansi')

                ->select(['izins.id as id_izins','izins.id_users','izins.tanggal_izin','izins.alasan_izin','izins.file_bukti_izin',
                    'izins.id_users_approval','izins.is_approval','izins.is_approval_at','izins.keterangan_approval','izins.status',
                    'izins.created_at as created_at_izins', 'instansis.nama_instansi','users.nama_lengkap','izins.tanggal_selesai_izin'])
                ->whereNotNull('magangs.id_users_magang')->orderBy('izins.id', "DESC")
                ->whereDate('izins.created_at', '>=', date('Y-m-d', strtotime($request->get('tanggal_mulai'))))
                ->whereDate('izins.created_at', '<=', date('Y-m-d', strtotime($request->get('tanggal_selesai'))))
                ->where(function ($query) {
                    if(auth()->user()->privileges == "USERS MAGANG") {
                        $query->where('izins.id_users', auth()->user()->id);
                    } else if(auth()->user()->privileges == "PEMBIMBING SEKOLAH") {
                        $query->where('magangs.id_pembimbing_sekolah', auth()->user()->id);
                    } else if(auth()->user()->privileges == "PEMBIMBING MAGANG") {
                        $query->where('magangs.id_users_pembimbing_magang', auth()->user()->id);
                    }
                })
                ->where('izins.deleted', 0)->get();

            return DataTables::of($dataIzin)
                ->addIndexColumn()
                ->filter(function ($instance) use ($request) {

                    if(!empty($request->get('nama_peserta'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains(Str::lower($row['nama_lengkap']), Str::lower($request->get('nama_peserta'))) ? true : false;
                        });
                    }
                    if(!empty($request->get('status'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['status'], $request->get('status')) ? true : false;
                        });
                    }
                    if(!empty($request->get('search'))){
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            if (Str::contains(Str::lower($row['nama_lengkap']), Str::lower($request->get('search')))){
                                return true;
                            }

                            return false;
                        });
                    }

                })
                ->addColumn('nama_user', function (Izins $izins){
                    return '<b>'.$izins->nama_lengkap.'</b><br><small class="badge badge-primary" >'.$izins->nama_instansi.'</small>';
                })
                ->addColumn('alasan_izin', function ($row){
                    return $row->alasan_izin;
                })
                ->addColumn('tanggal_izin', function ($row){

                    return date('d F y', strtotime($row->tanggal_izin));

                })
                ->addColumn('tanggal_selesai_izin', function ($row){

                    return date('d F y', strtotime($row->tanggal_selesai_izin));

                })
                ->addColumn('status', function ($row){
                    if($row->status == "Pengajuan") {
                        return '<small class="badge badge-info" >'.$row->status.'</small>';
                    } else if($row->status == "Disetujui") {
                        return '<small class="badge badge-success" >'.$row->status.'</small>';
                    } else if($row->status == "Ditolak") {
                        return '<small class="badge badge-danger" >'.$row->status.'</small>';
                    }

                })
                ->addColumn('file_bukti', function ($row){
                    return '<a href="/storage/'.$row->fileuploads->path.'" target="_blank">'.$row->fileuploads->title.'</a>';
                })
                ->addColumn('action', function ($row){
                    if(auth()->user()->privileges == "ADMIN" || auth()->user()->privileges == "PEMBIMBING MAGANG" || auth()->user()->privileges == "PEMBIMBING SEKOLAH") {
                        return '<a href="/data-izin-detail/'.$row->id_izins.'" class="btn btn-outline-info btn-sm">Detail</a>';
                    } else if(auth()->user()->privileges == "USERS MAGANG") {
                        if($row->is_approval == "Y") {
                            return '<a href="/data-izin-detail/'.$row->id_izins.'" class="btn btn-outline-info btn-sm">Detail</a>';
                        } else if($row->is_approval == "N") {
                            return '<a href="/data-izin-detail/'.$row->id_izins.'" class="btn btn-outline-info btn-sm">Detail</a> <a href="/data-izin/'.$row->id_izins.'/edit" class="btn btn-outline-primary btn-sm">Edit</a> <a href="/data-izin/soft_delete/'.$row->id_izins.'" class="btn btn-outline-primary btn-sm">Hapus</a>';
                        }

                    }

                })
                ->rawColumns(['nama_user','alasan_izin','tanggal_izin','status','action','file_bukti'])
                ->make(true);
        }

        return view('data_izin.v_data_izin',[
            "id_users" => auth()->user()->id,
            "privileges" => auth()->user()->privileges,
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $detailUsers = User::find(auth()->user()->id);
        return view('data_izin.v_data_izin_add',[
            "id_users" => auth()->user()->id,
            "privileges" => auth()->user()->privileges,
            'detailUsers' => $detailUsers,
            'id' => auth()->user()->id,
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_user' => 'required',
            'nama_peserta_magang' => 'required',
            'tanggal_izin' => 'required',
            'tanggal_selesai_izin' => 'required',
            'file_bukti_izin' => 'required|mimes:pdf,jpeg,png,jpg|file|max:1000',
        ]);

        $insert['file_uploads']['title'] = "Buakti Izin ". $request->nama_lengkap;
        $insert['file_uploads']['filename'] = $request->file('file_bukti_izin')->hashName();
        $insert['file_uploads']['path'] = $request->file('file_bukti_izin')->store('file-uploads/document');
        $insert['file_uploads']['type'] = $request->file('file_bukti_izin')->extension();
        $insert['file_uploads']['size'] = $request->file('file_bukti_izin')->getSize();
        $queryInsertBerkasUploads = FileUploads::create($insert['file_uploads']);
        $resultIdBerkasUpload = $queryInsertBerkasUploads->id;

        $insert['izins']['id_users'] = $request->id_user;
        $insert['izins']['tanggal_izin'] = date('Y-m-d', strtotime($request->tanggal_izin));
        $insert['izins']['tanggal_selesai_izin'] = date('Y-m-d', strtotime($request->tanggal_selesai_izin));
        $insert['izins']['alasan_izin'] = $request->alasan_izin;
        $insert['izins']['file_bukti_izin'] = $resultIdBerkasUpload;
        $resultInsertIzins = Izins::create($insert['izins']);
        if($resultInsertIzins) {
            return redirect('/data-izin')->with('success', 'Sukses Mengajukan Izin');
        } else {
            return redirect('/data-izin')->with('fail', 'Gagal Mengajukan Izin');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detailIzinsCount = Izins::where('id', $id)->get()->count();
        if($detailIzinsCount > 0) {
            $detailIzins = Izins::find($id);
            return view('data_izin.v_data_izin_edit', [
                "id_users" => auth()->user()->id,
                "privileges" => auth()->user()->privileges,
                "detailIzins" => $detailIzins,
                'id' => auth()->user()->id,
            ]);
        }  else {
            return redirect('/data-izin')->with('fail', 'Data Izin Tidak Tersedia');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $detailIzinsCount = Izins::where('id', $id)->get()->count();
        if($detailIzinsCount > 0) {
            $detailIzins = Izins::find($id);
            $request->validate([
                'id_user' => 'required',
                'nama_peserta_magang' => 'required',
                'tanggal_izin' => 'required',
                'tanggal_selesai_izin' => 'required',
            ]);

            if($request->file('file_bukti_izin')) {
                $request->validate([
                    'file_bukti_izin' => 'required|mimes:pdf,jpeg,png,jpg|file|max:1000'
                ]);
                //delete image old
                $resultIdBerkasUpload = 0;
                if($detailIzins->file_bukti_izin != 0) {
                    $detailFileUploads = FileUploads::find($detailIzins->file_bukti_izin);
                    Storage::delete($detailFileUploads->path);

                    $insert['file_uploads']['title'] = "Buakti Izin ". $request->nama_lengkap;
                    $insert['file_uploads']['filename'] = $request->file('file_bukti_izin')->hashName();
                    $insert['file_uploads']['path'] = $request->file('file_bukti_izin')->store('file-uploads/document');
                    $insert['file_uploads']['type'] = $request->file('file_bukti_izin')->extension();
                    $insert['file_uploads']['size'] = $request->file('file_bukti_izin')->getSize();
                    $queryInsertBerkasUploads = FileUploads::create($insert['file_uploads']);
                    $resultIdBerkasUpload = $queryInsertBerkasUploads->id;
                    $update['izins']['file_bukti_izin'] = $resultIdBerkasUpload;
                }
            }
            $update['izins']['id_users'] = $request->id_user;
            $update['izins']['tanggal_izin'] = date('Y-m-d', strtotime($request->tanggal_izin));
            $update['izins']['tanggal_selesai_izin'] = date('Y-m-d', strtotime($request->tanggal_selesai_izin));
            $update['izins']['alasan_izin'] = $request->alasan_izin;

            $resultInsertIzins = Izins::where('id', $id)->update($update['izins']);
            if($resultInsertIzins) {
                return redirect('/data-izin')->with('success', 'Sukses Melakukan Edit Izin');
            } else {
                return redirect('/data-izin')->with('fail', 'Gagal Melakukan Edit Izin');
            }
        }  else {
            return redirect('/data-izin')->with('fail', 'Data Izin Tidak Tersedia');
        }




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function izins_detail($id) {
        $detailCountIzins = Izins::where('id', $id)->get()->count();
        if($detailCountIzins > 0) {
            $detailIzins = Izins::find($id);
            return view('data_izin.v_data_izin_detail',[
                'detailIzins' => $detailIzins
            ]);
        } else {
            return redirect('/data-izin')->with('fail', 'Data Izin Tidak Tersedia');
        }
    }
    public function izin_action(Request $request){
        $request->validate([
            'id_user' => 'required',
            'id' => 'required',
            'keterangan_approval' => 'required',
            'status' => 'required',
        ]);


        $update['izins']['id_users_approval'] = auth()->user()->id;
        $update['izins']['is_approval'] = "Y";
        $update['izins']['is_approval_at'] = date('Y-m-d H:i:s');
        $update['izins']['keterangan_approval'] = $request->keterangan_approval;
        $update['izins']['status'] = $request->status;
        $resultUpdate = Izins::where('id', $request->id)->update($update['izins']);
        if($resultUpdate) {
            return redirect('/data-izin')->with('success', 'Sukses Melakukan Approve Aizin');
        } else {
            return redirect('/data-izin')->with('fail', 'Gagal ');
        }

    }

    public function soft_delete($id) {
        $detailCountIzins = Izins::where('id', $id)->get()->count();
        if($detailCountIzins > 0) {
            $detailIzins = Izins::find($id);
            $update['izins']['deleted'] = 1;
            Izins::where('id', $id)->update($update['izins']);
            return redirect('/data-izin')->with('success', 'Data Izin Sukses Dihapus');
        } else {
            return redirect('/data-izin')->with('fail', 'Data Izin Tidak Tersedia');
        }
    }

}
