<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailLinkResetPassword extends Mailable
{
    public $linkResetPassword;
    use Queueable, SerializesModels;

    public function __construct($linkResetPassword)
    {
        $this->linkResetPassword = $linkResetPassword;
    }

    public function build()
    {
        return $this->from('pt.pos.madiun@gmail.com')
            ->subject('Permintaan Lupa Password')
            ->markdown('email.template_send_email_lupa_password');
    }

}
