@extends('template.v_template')
@section('title', 'Data Absensi Magang')
@push('head-css')
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <!-- Theme style -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />


    <link rel="stylesheet" href="{{asset('template')}}/dist/css/adminlte.min.css">
@endpush
@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Data Pengajuan Izin</h3>
            @if(auth()->user()->privileges == "USERS MAGANG")
                <div class="card-tools">
                    <a href="/data-izin/create" class="btn btn-info btn-sm">Tambah Pengajuan Izin</a>
                </div>
            @endif

        </div>
        <div class="card-body">
            <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                </symbol>
                <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
                </symbol>
                <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                </symbol>
            </svg>
            @if(session()->has('success'))
                <div class="alert alert-success d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                    <div>
                        {{ session('success') }}
                    </div>
                </div>

            @elseif(session()->has('fail'))
                <div class="alert alert-danger" role="alert">
                    {{ session('fail') }}
                </div>
            @endif
            <form class="form-horizontal">

                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Mulai</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="tanggal_mulai" placeholder="Tangga Jam Masuk" name="tanggal_mulai" value="{{ date('m/d/Y', strtotime(date('Y-m-d'))) }}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nama_peserta" name="nama_peserta" placeholder="Nama Peserta Magang">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Selesai</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="tanggal_selesai" name="tanggal_selesai" placeholder="Tanggal Jam Pulang" value="{{ date('m/d/Y', strtotime(date('Y-m-d'))) }}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Status</label>
                                <div class="col-sm-10">
                                    <select class="form-control select2" id="status" name="status" style="width: 100%;">
                                        <option value="" selected="selected">Semua</option>
                                        <option value="Disetujui">Disetujui</option>
                                        <option value="Pengajuan">Pengajuan</option>
                                        <option value="Ditolak">Ditolak</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
            <div class="table-responsive">
                <table class="table table-bordered table_data_izins" id="table_data_izins">
                    <thead>
                    <tr class="bg-info">
                        <th>NAMA PESERTA</th>
                        <th>ALASAN IZIN</th>
                        <th>MULAI IZIN</th>
                        <th>SELESAI IZIN</th>
                        <th>STATUS</th>
                        <th>BUKTI</th>
                        <th class="text-center">AKSI</th>
                    </tr>
                    </thead>
                    <tbody>
{{--                    <tr>--}}
{{--                        <td>Soleh Muhammad Baitullah<br>--}}
{{--                            <span class="badge badge-info">SMK Negeri 1 Madiun</span>--}}
{{--                        </td>--}}
{{--                        <td>Karena SAkit Gigi Parah</td>--}}
{{--                        <td>--}}
{{--                            <p>--}}
{{--                                <i class="fas fa-calendar-alt"></i> 01 Maret 2022<br>--}}
{{--                                <i class="fas fa-calendar-alt"></i> 02 Maret 2022<br>--}}
{{--                                <i class="fas fa-calendar-alt"></i> 03 Maret 2022<br>--}}
{{--                                <i class="fas fa-calendar-alt"></i> 04 Maret 2022<br>--}}
{{--                                <i class="fas fa-calendar-alt"></i> 05 Maret 2022<br>--}}
{{--                            </p>--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            <small class="badge badge-warning">PENGAJUAN</small><br>--}}
{{--                        </td>--}}
{{--                        <td><p><i class="fas fa-paperclip mr-1"></i>12345678974122.pdf </p></td>--}}
{{--                        <td class="text-center">--}}
{{--                            <a href="/data-izin-detail" class="btn btn-outline-info btn-sm">Detail</a>--}}
{{--                            <a href="data-izin-edit" class="btn btn-outline-primary btn-sm">Edit</a>--}}
{{--                        </td>--}}
{{--                    </tr>--}}



                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Detail Kegiatan</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Diisi dengan deskripsi yang sudah di tuliskan pada form add kegiatan / aktifitas magang yang sudah dilakukan
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!-- /.card-footer-->


    </div>
    <!-- Modal -->


@endsection
@push('bottom-js')
    <script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{asset('template')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/jszip/jszip.min.js"></script>
    <script src="{{asset('template')}}/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{asset('template')}}/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->

    <script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function () {
            $('#tanggal_mulai').datepicker({
                uiLibrary: 'bootstrap4'
            });
            $('#tanggal_selesai').datepicker({
                uiLibrary: 'bootstrap4'
            });
            var table = $('.table_data_izins').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('izins.index') }}",
                    data: function (d){
                        d.tanggal_mulai = $("#tanggal_mulai").val(),
                            d.nama_peserta = $("#nama_peserta").val(),
                            d.search = $('input[type="search"]').val(),
                            d.tanggal_selesai = $("#tanggal_selesai").val()
                            d.status = $("#status").val()
                    },
                },
                columns: [
                    {data: 'nama_user', name: 'nama_user'},
                    {data: 'alasan_izin', name: 'alasan_izin'},
                    {data: 'tanggal_izin', name: 'tanggal_izin'},
                    {data: 'tanggal_selesai_izin', name: 'tanggal_selesai_izin'},
                    {data: 'status', name: 'status'},
                    {data: 'file_bukti', name: 'file_bukti'},
                    {data: 'action', name: 'action', className: "text-center"},
                ]
            });
            $("#tanggal_mulai, #nama_peserta, #tanggal_selesai, #status").change(function(e){
                table.draw();
                e.preventDefault();
            });



        });

    </script>


@endpush

