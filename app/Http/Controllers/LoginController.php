<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        return view('extras.login.v_login');
    }
    public function authenticate(Request $request) {
        $credentials =  $request->validate([
            'username' => 'required|min:3',
            'password' => 'required|min:3',

        ]);

        if(Auth::attempt(['username' => $request->username, 'password' => $request->password, 'deleted' => 0, 'disabled' => 1, 'pending' => 1])) {
            $request->session()->regenerate();
            return redirect()->intended('/home');
        }
        return back()->with('loginError', 'Login Gagal, Check User dan Password');

    }
    public function logout(){
        Auth::logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();
        return redirect('/');
    }
}
