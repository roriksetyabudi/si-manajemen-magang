<?php

namespace App\Http\Controllers;

use App\Models\Absens;
use Illuminate\Http\Request;
use PDF;
use App\Models\AktifitasMagangs;
class LaporanAktifitasMagang extends Controller
{
    public function index(){
        return view('laporan.aktifitas_magang.v_laporan_aktifitas_magang');

    }
    public function generate_pdf(Request $request) {
        $request->validate([
            'tanggal_mulai' => 'required|min:3',
            'tanggal_selesai' => 'required|min:3',
        ]);
        if(auth()->user()->privileges == "PEMBIMBING SEKOLAH") {
            $aktifitas_magangs = AktifitasMagangs::leftJoin('magangs', 'magangs.id_users_magang', '=', 'aktifitas_magangs.id_users')
                ->leftJoin('users', 'aktifitas_magangs.id_users', '=', 'users.id')
                ->select(['aktifitas_magangs.id as id_aktifitas_magang','aktifitas_magangs.id_users','aktifitas_magangs.nama_kegiatan','aktifitas_magangs.tanggal_mulai','aktifitas_magangs.tanggal_selesai',
                    'aktifitas_magangs.interval_aktifitas','aktifitas_magangs.volume_kegiatan','aktifitas_magangs.keterangan_kegiatan','aktifitas_magangs.deleted','aktifitas_magangs.created_at','aktifitas_magangs.updated_at',
                    'users.nama_lengkap'])
                ->whereNotNull('magangs.id_users_magang')
                ->where('id_pembimbing_sekolah', auth()->user()->id)
                ->whereDate('aktifitas_magangs.tanggal_selesai', '>=', date('Y-m-d', strtotime($request->post('tanggal_mulai'))))
                ->whereDate('aktifitas_magangs.tanggal_selesai', '<=', date('Y-m-d', strtotime($request->post('tanggal_selesai'))))->get();
        } else if(auth()->user()->privileges == "PEMBIMBING MAGANG") {
            $aktifitas_magangs = AktifitasMagangs::leftJoin('magangs', 'magangs.id_users_magang', '=', 'aktifitas_magangs.id_users')
                ->leftJoin('users', 'aktifitas_magangs.id_users', '=', 'users.id')
                ->select(['aktifitas_magangs.id as id_aktifitas_magang','aktifitas_magangs.id_users','aktifitas_magangs.nama_kegiatan','aktifitas_magangs.tanggal_mulai','aktifitas_magangs.tanggal_selesai',
                    'aktifitas_magangs.interval_aktifitas','aktifitas_magangs.volume_kegiatan','aktifitas_magangs.keterangan_kegiatan','aktifitas_magangs.deleted','aktifitas_magangs.created_at','aktifitas_magangs.updated_at',
                    'users.nama_lengkap'])
                ->whereNotNull('magangs.id_users_magang')
                ->where('id_users_pembimbing_magang', auth()->user()->id)
                ->whereDate('aktifitas_magangs.tanggal_selesai', '>=', date('Y-m-d', strtotime($request->post('tanggal_mulai'))))
                ->whereDate('aktifitas_magangs.tanggal_selesai', '<=', date('Y-m-d', strtotime($request->post('tanggal_selesai'))))->get();
        } else if(auth()->user()->privileges == "ADMIN") {
            $aktifitas_magangs = AktifitasMagangs::leftJoin('magangs', 'magangs.id_users_magang', '=', 'aktifitas_magangs.id_users')
                ->leftJoin('users', 'aktifitas_magangs.id_users', '=', 'users.id')
                ->select(['aktifitas_magangs.id as id_aktifitas_magang','aktifitas_magangs.id_users','aktifitas_magangs.nama_kegiatan','aktifitas_magangs.tanggal_mulai','aktifitas_magangs.tanggal_selesai',
                    'aktifitas_magangs.interval_aktifitas','aktifitas_magangs.volume_kegiatan','aktifitas_magangs.keterangan_kegiatan','aktifitas_magangs.deleted','aktifitas_magangs.created_at','aktifitas_magangs.updated_at',
                    'users.nama_lengkap'])
                ->whereNotNull('magangs.id_users_magang')
                ->whereDate('aktifitas_magangs.tanggal_selesai', '>=', date('Y-m-d', strtotime($request->post('tanggal_mulai'))))
                ->whereDate('aktifitas_magangs.tanggal_selesai', '<=', date('Y-m-d', strtotime($request->post('tanggal_selesai'))))->get();
        }
        $view = view('laporan.aktifitas_magang.cetak', [
            "detailAktifitasMagang" => $aktifitas_magangs,
            "tanggal_mulai" => $request->tanggal_mulai,
            "tanggal_selesai" => $request->tanggal_selesai
        ]);
        $html_content = $view->render();
        PDF::SetAuthor('Rorik Setya Budi');
        PDF::SetTitle('Laporan Data Sistem Informasi Manajemen Magang');
        PDF::SetSubject("Laporan Aktifitas Magang");
        PDF::SetKeywords('sistem informasi manajemen magang');
        PDF::setPrintHeader(false);
        PDF::setPrintFooter(false);
        PDF::SetMargins(7, 7, 7, 7);
        PDF::SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

        PDF::AddPage('L', 'A4');
        PDF::SetFont('helvetica', null, 11);
        PDF::writeHTML($html_content, true, false, true, false, '');
        PDF::Output('data_aktifitas_magangs.pdf');
    }
}
