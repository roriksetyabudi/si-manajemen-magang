<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagangRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magang_registrations', function (Blueprint $table) {
            $table->id();
            $table->string('username_peserta_magang', 50)->nullable(true);
            $table->string('password_peserta_magang', 255)->nullable(true);
            $table->integer('kode_registrasi')->unique()->nullable(false);
            $table->string('nama_lengkap_peserta_magang', 50)->nullable(true);
            $table->string("alamat_lengkap_peserta_magang")->nullable(true);
            $table->string("jenis_kelamin_peserta_magang", 2)->nullable(true);
            $table->string("telepon_peserta_magang", 20)->nullable(true);
            $table->string("email_peserta_magang", 50)->nullable(true);
            $table->integer("fupload_berkas_peserta_magang")->default(0);
            $table->integer("fupload_berkas_foto_peserta_magang")->default(0);
            $table->string("nama_instansi", 50)->nullable(true);
            $table->string("alamat_lengkap_instansi")->nullable(true);
            $table->string("telepon_instansi", 20)->nullable(true);
            $table->string("bidang_keahlian_peserta_magang", 50)->nullable(true);

            $table->string("nama_pembimbing_sekolah_peserta_magang", 50)->nullable(true);
            $table->date("tanggal_ajuan_mulai_magang")->nullable(false);
            $table->date("tanggal_ajuan_selesai_magang")->nullable(false);
            $table->text("keterangan")->nullable(true);

            $table->enum("status", ["PENGAJUAN","DITOLAK","DITERIMA"])->default("PENGAJUAN");
            $table->tinyInteger("deleted")->default(0)->comment("0=Not Deleted, 1=deleted");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('magang_registrations');
    }
}
