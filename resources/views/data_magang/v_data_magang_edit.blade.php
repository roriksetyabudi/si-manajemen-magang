@extends('template.v_template')
@section('title', 'Tambah User Baru')
@push('head-css')
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/daterangepicker/daterangepicker.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
    <!-- BS Stepper -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/bs-stepper/css/bs-stepper.min.css">
    <!-- dropzonejs -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/dropzone/min/dropzone.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/adminlte.min.css">
@endpush
@section('content')
    <div class="container-fluid">
        <form method="post" action="/data-magang/post" enctype="multipart/form-data">
            @csrf
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Tambah Data Magang</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool">
                            <a href="/data-users"><i class="fas fa-arrow-alt-circle-left"></i></a>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" id="nama_lengkap" data-inputmask-alias="datetime" placeholder="Nama Lengkap">
                                @error('nama_lengkap')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                                <label>Telepon</label>
                                <input type="text" class="form-control @error('telepon') is-invalid @enderror" name="telepon" id="telepon" placeholder="Telepon">
                                @error('telepon')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <input type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat" id="alamat" placeholder="Alamat">
                                @error('alamat')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Jenis Kelamin</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input @error('gender') is-invalid @enderror" type="radio" name="gender" id="genderL" value="L">
                                    <label class="form-check-label" for="inlineRadio1">Laki - Laki</label>

                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input @error('gender') is-invalid @enderror" type="radio" name="gender" id="genderP" value="P">
                                    <label class="form-check-label" for="inlineRadio2">Perempuan</label>

                                </div>
                            </div>
                            <div class="form-group">
                                <label>Peserta Magang</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input @error('is_peserta_magang') is-invalid @enderror" type="radio" name="is_peserta_magang" id="is_peserta_magangY" value="Y" checked>
                                    <label class="form-check-label" for="inlineRadio1">Ya Peserta Magang</label>

                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input @error('is_peserta_magang') is-invalid @enderror" type="radio" name="is_peserta_magang" id="is_peserta_magangN" value="N">
                                    <label class="form-check-label" for="inlineRadio2">Bukan Peserta Magang</label>

                                </div>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Privileges</label>
                                <select class="form-control @error('privileges') is-invalid @enderror select2" name="privileges" id="privileges" style="width: 100%;">
                                    <option value="USERS MAGANG">USERS MAGANG</option>
                                </select>
                                @error('privileges')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="" placeholder="Username">
                                @error('username')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                                @error('password')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Email">
                                @error('email')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Foto Peserta</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input @error('photo') is-invalid @enderror" id="photo" name="photo" onchange="previewImage()">
                                    @error('photo')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                    <label class="custom-file-label" for="exampleInputFile" required>Upload Foto</label>
                                </div>
                                <hr>

                                <img class="image-preview img-fluid col-sm-6">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-default layout_add_magang">
                <div class="card-header">
                    <h3 class="card-title">Data Magang</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Mulai Magang</label>
                                <input type="date" class="form-control @error('tanggal_mulai_magang') is-invalid @enderror" name="tanggal_mulai_magang" id="tanggal_mulai_magang" placeholder="Tanggal Mulai Magang">
                            </div>
                            <div class="form-group">
                                <label>Tanggal Selesai Magang</label>
                                <input type="date" class="form-control @error('tanggal_selesai_magang') is-invalid @enderror" name="tanggal_selesai_magang" id="tanggal_selesai_magang" placeholder="Tanggal Selesai Magang">
                            </div>
                            <div class="form-group">
                                <label>Jurusan Bidang Keahliah</label>
                                <input type="text" class="form-control @error('jurusan_bidang_keahlihan') is-invalid @enderror" name="jurusan_bidang_keahlihan" id="jurusan_bidang_keahlihan" placeholder="Jurusan Bidang Keahlian">
                            </div>
                            <div class="form-group">
                                <label>Pembimbing Magang</label>
                                <select class="form-control select2 @error('id_users_pembimbing_magang') is-invalid @enderror" name="id_users_pembimbing_magang" id="id_users_pembimbing_magang" style="width: 100%;">
                                    <option value="" selected="selected">Pilih</option>
                                    @foreach($pembimbingMagang as $valpembimbingMagang)
                                        <option value="{{ $valpembimbingMagang->id }}">{{ $valpembimbingMagang->nama_lengkap }}</option>
                                    @endforeach



                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nama Instansi</label>
                                <select class="form-control select2 @error('id_instansi') is-invalid @enderror" name="id_instansi" id="id_instansi" style="width: 100%;">
                                    <option value="" selected="selected">Pilih</option>
                                    @foreach($instansis as $instansi)
                                        <option value="{{ $instansi->id }}" nama_instansi="{{ $instansi->nama_instansi }}" alamat_instansi="{{ $instansi->alamat_instansi }}" telepon_instansi="{{ $instansi->telepon_instansi }}">{{ $instansi->nama_instansi }}</option>
                                    @endforeach

                                    <option value="lainya">Lainya</option>
                                </select>
                                <input type="text" class="form-control" name="nama_instansi" id="nama_instansi" placeholder="Nama Instansi">
                            </div>

                            <!-- /.form-group -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                            <!-- /.form-group -->
                            <div class="form-group">
                                <label>Pembimbing Sekolah</label>
                                <select class="form-control select2 @error('id_pembimbing_sekolah') is-invalid @enderror" name="id_pembimbing_sekolah" id="id_pembimbing_sekolah" style="width: 100%;">
                                    <option value="" selected="selected">Pilih</option>
                                    @foreach($pembimbingSekolah as $valpembimbingSekolah)
                                        <option value="{{ $valpembimbingSekolah->id }}">{{ $valpembimbingSekolah->nama_lengkap }}</option>
                                    @endforeach


                                </select>
                            </div>

                            <div class="form-group">
                                <label>Alamat Instansi</label>
                                <input type="text" class="form-control @error('alamat_instansi') is-invalid @enderror" name="alamat_instansi" id="alamat_instansi" placeholder="Alamat Instansi">
                            </div>
                            <div class="form-group">
                                <label>Telepon Instansi</label>
                                <input type="text" class="form-control @error('telepon_instansi') is-invalid @enderror" name="telepon_instansi" id="telepon_instansi" placeholder="Nomor Telepon">
                            </div>
                            <div class="form-group">
                                <label>Upload Berkas Magang</label>
                                <p class="text-muted h6">Kartu Identitas dan Surat Keterangan Magang dari Instansi, Digabung Jadi 1 File .pdf maksimal 2Mb</p>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input @error('file_berkas') is-invalid @enderror" id="file_berkas" name="file_berkas">
                                    @error('file_berkas')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                    <label class="custom-file-label" for="exampleInputFile" required>Upload Berkas</label>

                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn bg-gradient-primary">Proses</button>
            </div>
        </form>

    </div>
@endsection
@push('bottom-js')
    <!-- jQuery -->
    <script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Select2 -->
    <script src="{{asset('template')}}/plugins/select2/js/select2.full.min.js"></script>
    <!-- Bootstrap4 Duallistbox -->
    <script src="{{asset('template')}}/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
    <!-- InputMask -->
    <script src="{{asset('template')}}/plugins/moment/moment.min.js"></script>
    <script src="{{asset('template')}}/plugins/inputmask/jquery.inputmask.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{asset('template')}}/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="{{asset('template')}}/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{asset('template')}}/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Bootstrap Switch -->
    <script src="{{asset('template')}}/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <!-- BS-Stepper -->
    <script src="{{asset('template')}}/plugins/bs-stepper/js/bs-stepper.min.js"></script>
    <!-- dropzonejs -->
    <script src="{{asset('template')}}/plugins/dropzone/min/dropzone.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->

    <script type="text/javascript">
        $(document).ready(function() {

            $("#nama_instansi").hide();

            $('#id_instansi').change(function () {
                var value = $(this).val();
                if(value == "lainya") {
                    $("#nama_instansi").show();
                    $("#id_instansi").hide();
                } else {
                    var optionSelected = $(this).find("option:selected");
                    $("#nama_instansi").hide();
                    $("#id_instansi").show();

                    $("#alamat_instansi").val(optionSelected.attr('alamat_instansi'));
                    $("#telepon_instansi").val(optionSelected.attr('telepon_instansi'));


                }
            });

        });
        function previewImage(){
            const image = document.querySelector('#photo');
            const imagePreview = document.querySelector('.image-preview');

            imagePreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function (oFREvent) {
                imagePreview.src = oFREvent.target.result;
            }
        }
    </script>
@endpush
