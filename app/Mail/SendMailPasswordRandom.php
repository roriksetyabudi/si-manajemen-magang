<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
class SendMailPasswordRandom extends Mailable
{
    public $passwordRandom;
    use Queueable, SerializesModels;

    public function __construct($passwordRandom)
    {
        $this->passwordRandom = $passwordRandom;
    }
    public function build()
    {
        return $this->from('pt.pos.madiun@gmail.com')
            ->subject('Password Baru Anda Dari Hasil Lupa Password')
            ->markdown('email.template_send_email_password_random');
    }

}
