<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GeneralSettings;
class GeneralSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $detailGeneralSettings = GeneralSettings::first();
        return view('general_settings.v_general_settings_edit', [
            'detailGeneralSettings' => $detailGeneralSettings
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $checkCountGeneralSettings = GeneralSettings::where('id', $id)->get()->count();
        if($checkCountGeneralSettings > 0) {
            $update['general_settings']['name'] = $request->name;
            $update['general_settings']['description'] = $request->description;
            $update['general_settings']['kuota_magang'] = $request->kuota_magang;
            $result = GeneralSettings::where('id', $id)->update($update['general_settings']);
            if($result) {
                return redirect('/general-settings')->with('success', 'Sukses Memperbahaui General Settings');
            } else {
                return redirect('/general-settings')->with('fail', 'Gagal Memperbaharui General Settings');
            }


        } else {
            return redirect('/general-settings')->with('fail', 'Data Tidak Tersedia');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
