<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absens extends Model
{
    use HasFactory;

    protected $table = 'absens';
    protected $guarded = ['id'];

    public function user(){
        return $this->belongsTo(User::class, 'id_users','id');
    }
    public function fileuploads(){
        return$this->belongsTo(FileUploads::class, 'file_bukti_absensi', 'id');
    }
    public function fileuploadsPulang(){
        return$this->belongsTo(FileUploads::class, 'file_bukti_absensi_pulang', 'id');
    }
    public function get_magangs_users_aktif(){
        $dataUSersAktifMagang = DB::select("SELECT a.id_users_magang, b.nama_lengkap FROM management_magang.magangs a
	                                LEFT JOIN users b ON a.id_users_magang=b.id WHERE CURDATE() < a.tanggal_selesai_magang");
        return $dataUSersAktifMagang;
    }
}
