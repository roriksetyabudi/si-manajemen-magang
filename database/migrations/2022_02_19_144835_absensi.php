<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Absensi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absens', function (Blueprint $table) {
            $table->id();
            $table->smallInteger('id_users')->index();
            $table->dateTime('tanggal_masuk')->nullable(true);
            $table->dateTime('tanggal_pulang')->nullable(true);
            $table->enum('status', ["Masuk","Pulang"])->default("Masuk");
            $table->smallInteger("file_bukti_absensi")->default(0);
            $table->smallInteger("file_bukti_absensi_pulang")->default(0);
            $table->enum("approval", ['Y','N'])->default('N');
            $table->tinyInteger("deleted")->default(0)->comment("0=Not Deleted, 1=deleted");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absens');
    }
}
