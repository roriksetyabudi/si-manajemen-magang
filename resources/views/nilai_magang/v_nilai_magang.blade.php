@extends('template.v_template')
@section('title', 'Nilai Magang')
@push('head-css')
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template/')}}/plugins/fontawesome-free/css/all.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">


    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template/')}}/dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('template/')}}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

@endpush
@section('content')

    <style>
        .scrollme {
            overflow-x: auto;
        }
    </style>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Data Nilai Magang</h3>

        </div>
        <div class="card-body">

                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nama_peserta" name="nama_peserta" placeholder="Nama Peserta Magang">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Instansi</label>
                                <div class="col-sm-10">
                                    <select class="form-control select2" name="id_instansis" id="id_instansis" style="width: 100%;">
                                        <option value="">Semua</option>
                                        @foreach($instansis as $instansi)
                                            <option value="{{ $instansi->id }}">{{ $instansi->nama_instansi }}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            <div class="table-responsive">
                <table class="table table-bordered table_data_nilai_magang" id="table_data_nilai_magang">
                    <thead>
                    <tr class="bg-info">
{{--                        <td class="align-middle text-center">No</td>--}}
                        <th class="align-middle text-center">Peserta</th>
                        <th class="align-middle text-center">Kedisiplinan</th>
                        <th class="align-middle text-center">Sikap</th>
                        <th class="align-middle text-center">Komunikasi</th>
                        <th class="align-middle text-center">Kerapian</th>
                        <th class="align-middle text-center">Kerjasama</th>
                        <th class="align-middle text-center">Motivasi</th>
                        <th class="align-middle text-center">Pend.Pekerjaan</th>
                        <th class="align-middle text-center">H.Pekerjaan</th>
                        <th class="align-middle text-center">Ide Gagasan</th>

                        <th class="align-middle text-center">T.JAWAB</th>
                        <th class="align-middle text-center">JUJUR</th>
                        <th class="align-middle text-center">RATA RATA</th>
                        <th class="align-middle text-center">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>

        <div class="modal fade" id="dialog_modifikasi_nilai">
            <div class="modal-dialog modal-dialog-scrollable modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title title_name"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                            <div class="row">

                                <div class="col-md-12">

                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">Input Atau Ubah Nilai</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <!-- form start -->
                                        <form>
                                            <input type="text" class="form-control" id="id_user" name="id_user" placeholder="0" value="" hidden>
                                            <input type="text" class="form-control" id="id_nilai_magangs" name="id_nilai_magangs" placeholder="0" value="" hidden>

                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">NO Sertifikat</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="no_sertifikat" name="no_sertifikat" value="" placeholder="Tuliskan Nomor Sertifikat">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Kedisiplinan</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="kedisiplinan" name="kedisiplinan" placeholder="0" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Sikap</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="sikap" name="sikap" placeholder="0" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Komunikasi</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="komunikasi" name="komunikasi" placeholder="0" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Kerapian</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="kerapian" name="kerapian" placeholder="0" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Kerjasama</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="kerjasama" name="kerjasama" placeholder="0" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Motivasi</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="motivasi" name="motivasi" placeholder="0" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Penguasaan Pekerjaan</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="penguasaan_terhadap_pekerjaan" name="penguasaan_terhadap_pekerjaan" placeholder="0" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Hasil Pekerjaan</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="hasil_pekerjaan" name="hasil_pekerjaan" placeholder="0" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Ide Gagasan</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="ide_atau_gagasan" name="ide_atau_gagasan" placeholder="0" value="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Tanggung Jawab</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="tanggung_jawab" name="tanggung_jawab" placeholder="0" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Kejujuran</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="kejujuran" name="kejujuran" placeholder="0" value="">
                                                    </div>
                                                </div>


                                            </div>

                                        </form>
                                    </div>
                                </div>

                            </div>



                    </div>
                    <div class="modal-footer justify-content-between">
                        @if(auth()->user()->privileges == "PEMBIMBING MAGANG")
                        <button type="button" id="proses_set_nilai_peserta_magang" class="btn btn-primary proses_set_nilai_peserta_magang">PROSES</button>
                        @endif
                        <button type="button" class="btn btn-success btn_cetak_nilai">CETAK NILAI</button>
                        <button type="button" class="btn btn-info btn_cetak_sertifikat">CETAK SERTIFIKAT</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
@endsection
@push('bottom-js')
    <!-- jQuery -->
    <script src="{{asset('template/')}}/plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('template/')}}/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template/')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- DataTables  & Plugins --><script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="{{asset('template')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{asset('template')}}/plugins/jszip/jszip.min.js"></script>
    <script src="{{asset('template')}}/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{asset('template')}}/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>

    <!-- overlayScrollbars -->
    <script src="{{asset('template/')}}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template/')}}/dist/js/adminlte.js"></script>

    <script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });




        $(function () {
            $('#kedisiplinan').keyup(function (e) {
                $(this).val($(this).val().replace(/[A-Za-z-,*+?^${}()|[\]\/\\]+$/, ""));
            });
            $('#sikap').keyup(function (e) {
                $(this).val($(this).val().replace(/[A-Za-z-,*+?^${}()|[\]\/\\]+$/, ""));
            });
            $('#komunikasi').keyup(function (e) {
                $(this).val($(this).val().replace(/[A-Za-z-,*+?^${}()|[\]\/\\]+$/, ""));
            });
            $('#kerapian').keyup(function (e) {
                $(this).val($(this).val().replace(/[A-Za-z-,*+?^${}()|[\]\/\\]+$/, ""));
            });
            $('#kerjasama').keyup(function (e) {
                $(this).val($(this).val().replace(/[A-Za-z-,*+?^${}()|[\]\/\\]+$/, ""));
            });
            $('#motivasi').keyup(function (e) {
                $(this).val($(this).val().replace(/[A-Za-z-,*+?^${}()|[\]\/\\]+$/, ""));
            });
            $('#penguasaan_terhadap_pekerjaan').keyup(function (e) {
                $(this).val($(this).val().replace(/[A-Za-z-,*+?^${}()|[\]\/\\]+$/, ""));
            });
            $('#hasil_pekerjaan').keyup(function (e) {
                $(this).val($(this).val().replace(/[A-Za-z-,*+?^${}()|[\]\/\\]+$/, ""));
            });
            $('#ide_atau_gagasan').keyup(function (e) {
                $(this).val($(this).val().replace(/[A-Za-z-,*+?^${}()|[\]\/\\]+$/, ""));
            });

            $('#tanggung_jawab').keyup(function (e) {
                $(this).val($(this).val().replace(/[A-Za-z-,*+?^${}()|[\]\/\\]+$/, ""));
            });
            $('#kejujuran').keyup(function (e) {
                $(this).val($(this).val().replace(/[A-Za-z-,*+?^${}()|[\]\/\\]+$/, ""));
            });
            var table = $('.table_data_nilai_magang').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('nilai_trans.index') }}",
                    data: function (d){
                        d.status = $("#status").val(),
                            d.nama_peserta = $("#nama_peserta").val(),
                            d.search = $('input[type="search"]').val(),
                            d.id_instansis = $("#id_instansis").val()
                    },
                },

                columns: [
                    {data: 'user', name: 'user.nama_lengkap'},
                    {data: 'kedisiplinan', name: 'nilai_trans.kedisiplinan', className: "text-center"},
                    {data: 'sikap', name: 'nilai_trans.sikap', className: "text-center"},
                    {data: 'komunikasi', name: 'nilai_trans.komunikasi', className: "text-center"},
                    {data: 'kerapian', name: 'nilai_trans.kerapian', className: "text-center"},
                    {data: 'kerjasama', name: 'nilai_trans.kerjasama', className: "text-center"},
                    {data: 'motivasi', name: 'nilai_trans.motivasi', className: "text-center"},
                    {data: 'penguasaan_terhadap_pekerjaan', name: 'nilai_trans.penguasaan_terhadap_pekerjaan', className: "text-center"},
                    {data: 'hasil_pekerjaan', name: 'nilai_trans.hasil_pekerjaan', className: "text-center"},
                    {data: 'ide_atau_gagasan', name: 'nilai_trans.ide_atau_gagasan', className: "text-center"},

                    {data: 'tanggung_jawab', name: 'nilai_trans.tanggung_jawab', className: "text-center"},
                    {data: 'kejujuran', name: 'nilai_trans.kejujuran', className: "text-center"},
                    {data: 'rata_rata', name: 'nilai_trans.rata_rata', className: "text-center"},
                    {data: 'action', name: 'action', orderable: false, searchable: false, className: "text-center"},
                ]
            });
            $("#nama_peserta, #id_instansis").change(function(e){
                //table.column($(this).data('column')).search($(this).val()).draw();
                table.draw();
                e.preventDefault();
            });

            $('#table_data_nilai_magang').on('click', '.click_pilih_nilai_magangs', function () {
                var id_nilai_trans = $(this).attr('id');
                var id_users = $(this).attr('id_users');
                var no_sertifikat = $(this).attr('no_sertifikat');
                var nama_lengkap = $(this).attr('nama_lengkap');
                var kedisiplinan = $(this).attr('kedisiplinan');
                var sikap = $(this).attr('sikap');
                var komunikasi = $(this).attr('komunikasi');
                var kerapian = $(this).attr('kerapian');
                var kerjasama = $(this).attr('kerjasama');
                var motivasi = $(this).attr('motivasi');
                var penguasaan_terhadap_pekerjaan = $(this).attr('penguasaan_terhadap_pekerjaan');
                var ide_atau_gagasan = $(this).attr('ide_atau_gagasan');
                var hasil_pekerjaan = $(this).attr('hasil_pekerjaan');

                var tanggung_jawab = $(this).attr('tanggung_jawab');
                var kejujuran = $(this).attr('kejujuran');

                $("#dialog_modifikasi_nilai").modal('show');
                $(".title_name").text("Detail Nilai " + nama_lengkap);

                $("#id_user").val(id_users);
                $("#id_nilai_magangs").val(id_nilai_trans);

                $("#no_sertifikat").val(no_sertifikat);
                $("#kedisiplinan").val(kedisiplinan);
                $("#sikap").val(sikap);
                $("#komunikasi").val(komunikasi);
                $("#kerapian").val(kerapian);
                $("#kerjasama").val(kerjasama);
                $("#motivasi").val(motivasi);
                $("#penguasaan_terhadap_pekerjaan").val(penguasaan_terhadap_pekerjaan);
                $("#hasil_pekerjaan").val(hasil_pekerjaan);
                $("#ide_atau_gagasan").val(ide_atau_gagasan);

                $("#tanggung_jawab").val(tanggung_jawab);
                $("#kejujuran").val(kejujuran);



            });

            $(".proses_set_nilai_peserta_magang").click(function(e){
                $.ajax({
                    type:'POST',
                    url:"{{ route('nilai_trans.set_nilai_with_ajax') }}",
                    data: {
                        id_users: $("#id_user").val(),
                        id_nilai_magangs: $("#id_nilai_magangs").val(),
                        no_sertifikat: $("#no_sertifikat").val(),
                        kedisiplinan: $("#kedisiplinan").val(),
                        sikap: $("#sikap").val(),
                        komunikasi: $("#komunikasi").val(),
                        kerapian: $("#kerapian").val(),
                        kerjasama: $("#kerjasama").val(),
                        motivasi: $("#motivasi").val(),
                        penguasaan_terhadap_pekerjaan: $("#penguasaan_terhadap_pekerjaan").val(),
                        hasil_pekerjaan: $("#hasil_pekerjaan").val(),
                        ide_atau_gagasan: $("#ide_atau_gagasan").val(),

                        tanggung_jawab: $("#tanggung_jawab").val(),
                        kejujuran: $("#kejujuran").val()
                    },
                    success: function (data) {
                        if(data.metaData.code == 200) {
                            alert(data.metaData.message);
                            table.draw();
                            $("#dialog_modifikasi_nilai").modal('hide');
                        } else if(data.metaData.code == 201) {
                            alert(data.metaData.message);
                            table.draw();
                            $("#dialog_modifikasi_nilai").modal('show');
                        }
                    }
                });
            });
            $(".btn_cetak_nilai").click(function(e){
                window.open("/cetak-nilai-magang/" + $('#id_user').val(), '_blank');
            });
            $(".btn_cetak_sertifikat").click(function(e){
                window.open("/cetak-sertifikat-magang/" + $('#id_user').val(), '_blank');
            });


        });

    </script>

@endpush
