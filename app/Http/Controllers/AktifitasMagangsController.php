<?php

namespace App\Http\Controllers;
use App\Models\Absens;
use Illuminate\Http\Request;
use App\Models\AktifitasMagangs;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;

class AktifitasMagangsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $dataAktifitasMagang = AktifitasMagangs::leftJoin('users','aktifitas_magangs.id_users', '=', 'users.id')
                ->leftJoin('magangs', 'magangs.id_users_magang', '=', 'aktifitas_magangs.id_users')
                ->leftJoin('instansis', 'instansis.id', '=', 'magangs.id_instansi')
                ->select(['aktifitas_magangs.id as id_aktifitas_magangs','aktifitas_magangs.id_users as id_users_aktifitas','aktifitas_magangs.nama_kegiatan',
                    'aktifitas_magangs.tanggal_mulai','aktifitas_magangs.tanggal_selesai','aktifitas_magangs.interval_aktifitas',
                    'aktifitas_magangs.volume_kegiatan','aktifitas_magangs.keterangan_kegiatan','aktifitas_magangs.deleted',
                    'users.nama_lengkap'])
                ->whereNotNull('magangs.id_users_magang')
                ->whereDate('aktifitas_magangs.created_at', '>=', date('Y-m-d', strtotime($request->get('tanggal_mulai'))))
                ->whereDate('aktifitas_magangs.created_at', '<=', date('Y-m-d', strtotime($request->get('tanggal_selesai'))))
                ->where(function ($query) {
                    if(auth()->user()->privileges == "USERS MAGANG") {
                        $query->where('aktifitas_magangs.id_users', auth()->user()->id);
                    } else if(auth()->user()->privileges == "PEMBIMBING SEKOLAH") {
                        $query->where('magangs.id_pembimbing_sekolah', auth()->user()->id);
                    } else if(auth()->user()->privileges == "PEMBIMBING MAGANG") {
                        $query->where('magangs.id_users_pembimbing_magang', auth()->user()->id);
                    }
                })
                ->where('aktifitas_magangs.deleted',0)->get();

            return DataTables::of($dataAktifitasMagang)
                ->addIndexColumn('aktifitas_magangs')
                ->filter(function ($instance) use ($request) {

                    if(!empty($request->get('nama_kegiatan'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains(Str::lower($row['nama_kegiatan']), Str::lower($request->get('nama_kegiatan'))) ? true : false;
                        });
                    }

                    if(!empty($request->get('user'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains(Str::lower($row['nama_lengkap']), Str::lower($request->get('user'))) ? true : false;
                        });
                    }
                    if(!empty($request->get('search'))){
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            if (Str::contains(Str::lower($row['nama_lengkap']), Str::lower($request->get('search')))){
                                return true;
                            }

                            return false;
                        });
                    }


                })
                    ->addColumn('user', function (AktifitasMagangs $aktifitasMagangs){
                        return $aktifitasMagangs->nama_lengkap;
                    })
                    ->addColumn('nama_kegiatan', function (AktifitasMagangs $aktifitasMagangs){
                        return $aktifitasMagangs->nama_kegiatan;
                    })
                    ->addColumn('tanggal_mulai', function (AktifitasMagangs $aktifitasMagangs) {
                        return date('d F y H:i:s', strtotime($aktifitasMagangs->tanggal_mulai));
                    })
                    ->addColumn('tanggal_selesai', function (AktifitasMagangs $aktifitasMagangs) {
                        return date('d F y H:i:s', strtotime($aktifitasMagangs->tanggal_selesai));
                    })
                    ->addColumn('interval_aktifitas', function (AktifitasMagangs $aktifitasMagangs) {
                        return '<small class="badge badge-success"><i class="far fa-clock"></i> '.$aktifitasMagangs->interval_aktifitas.'</small>';
                    })
                    ->addColumn('volume_kegiatan', function (AktifitasMagangs $aktifitasMagangs) {
                        return $aktifitasMagangs->volume_kegiatan;
                    })
                    ->addColumn('action', function (AktifitasMagangs $aktifitasMagangs) {
                        $id = $aktifitasMagangs->id_aktifitas_magangs;
                        if(auth()->user()->privileges == "USERS MAGANG") {
                            $btn = '<div class="btn-group">
                                    <button type="button" class="btn btn-warning">Aksi</button>
                                    <button type="button" class="btn btn-warning dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                    <span class="sr-only"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                    <a class="dropdown-item" href="/aktifitas-magang-detail/'.$id.'">Detail</a>
                                    <a class="dropdown-item" href="/aktifitas-magang/'.$id.'/edit">Edit</a>
                                    <a class="dropdown-item" href="/delete-aktifitas-magang/'.$id.'">Hapus</a>

                                    </div>
                                    </div>';
                        } else {
                            $btn = 'ReadOnly';
                        }



                        return $btn;
                    })
                    ->rawColumns(['action','user','tanggal_mulai','tanggal_selesai','interval_aktifitas','volume_kegiatan'])
                    ->make(true);

        }
        return view('aktifitas_magang.v_aktifitas_magang', [
            "id_users" => auth()->user()->id,
            "privileges" => auth()->user()->privileges
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('aktifitas_magang.v_aktifitas_magang_add',[
            "id_users" => auth()->user()->id,
            "privileges" => auth()->user()->privileges,
            "aktifitasMagangs" => AktifitasMagangs::where('id_users', auth()->user()->id)->whereDate('created_at', date('Y-m-d'))->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_kegiatan' => 'required',
            'tanggal_mulai' => 'required',
            'tanggal_selesai' => 'required',
            'interval_aktifitas' => 'required',
            'volume_kegiatan' => 'required',
            'keterangan_kegiatan' => 'required',
        ]);

        $insert['aktifitas_magangs']['id_users'] = auth()->user()->id;
        $insert['aktifitas_magangs']['nama_kegiatan'] = $request->nama_kegiatan;
        $insert['aktifitas_magangs']['tanggal_mulai'] = date('Y-m-d H:i:s', strtotime($request->tanggal_mulai. ' '.date('H:i:s')));
        $insert['aktifitas_magangs']['tanggal_selesai'] = date('Y-m-d H:i:s', strtotime($request->tanggal_selesai. ' '.date('H:i:s')));
        $insert['aktifitas_magangs']['interval_aktifitas'] = $request->interval_aktifitas;
        $insert['aktifitas_magangs']['volume_kegiatan'] = $request->volume_kegiatan;
        $insert['aktifitas_magangs']['keterangan_kegiatan'] = $request->keterangan_kegiatan;

        AktifitasMagangs::create($insert['aktifitas_magangs']);
        return redirect('/aktifitas-magang')->with('success', 'Sukses Menambah Data Aktifitas Kegiatan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('aktifitas_magang.v_aktifitas_magang_edit', [
            "id_users" => auth()->user()->id,
            "privileges" => auth()->user()->privileges,
            "aktifitasMagangDetail" => AktifitasMagangs::find($id),
            "id" => $id
        ]);
    }
    public function detail($id) {
        return view('aktifitas_magang.v_aktifitas_magang_detail', [
            "id_users" => auth()->user()->id,
            "privileges" => auth()->user()->privileges,
            "aktifitasMagangDetail" => AktifitasMagangs::find($id),
            "id" => $id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_kegiatan' => 'required',
            'tanggal_mulai' => 'required',
            'tanggal_selesai' => 'required',
            'interval_aktifitas' => 'required',
            'volume_kegiatan' => 'required',
            'keterangan_kegiatan' => 'required',
        ]);

        $update['aktifitas_magangs']['id_users'] = auth()->user()->id;
        $update['aktifitas_magangs']['nama_kegiatan'] = $request->nama_kegiatan;
        $update['aktifitas_magangs']['tanggal_mulai'] = date('Y-m-d H:i:s', strtotime($request->tanggal_mulai. ' '.date('H:i:s')));
        $update['aktifitas_magangs']['tanggal_selesai'] = date('Y-m-d H:i:s', strtotime($request->tanggal_selesai. ' '.date('H:i:s')));
        $update['aktifitas_magangs']['interval_aktifitas'] = $request->interval_aktifitas;
        $update['aktifitas_magangs']['volume_kegiatan'] = $request->volume_kegiatan;
        $update['aktifitas_magangs']['keterangan_kegiatan'] = $request->keterangan_kegiatan;

        AktifitasMagangs::where('id', $id)->update($update['aktifitas_magangs']);
        return redirect('/aktifitas-magang')->with('success', 'Sukses Melakukan Update Data Aktifitas Kegiatan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detail = AktifitasMagangs::find($id);
        if($detail) {
            AktifitasMagangs::where('id', $id)->update(['deleted' => 1]);
            return redirect('/aktifitas-magang')->with('success', 'Sukses Melakukan Hapus Data Aktifitas Kegiatan');
        } else {
            return redirect('/aktifitas-magang')->with('fail', 'Gagal');
        }
    }
}
