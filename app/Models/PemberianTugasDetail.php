<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PemberianTugasDetail extends Model
{
    use HasFactory;
    use HasFactory;
    protected $table = 'pemberian_tugas_detail';
    protected $guarded = ['id'];

    public function userCreated(){
        return $this->belongsTo(User::class, 'id_users_created', 'id');
    }

    public function fileLampiran(){
        return $this->belongsTo(FileUploads::class, 'id_file_tugas', 'id');
    }
}
