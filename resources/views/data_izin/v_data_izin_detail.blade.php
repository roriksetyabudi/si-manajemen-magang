@extends('template.v_template')
@section('title', 'Data Absensi Magang')
@push('head-css')
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/adminlte.min.css">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/summernote/summernote-bs4.min.css">
@endpush
@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Detail Data Izin</h3>
        </div>
        <form method="post" action="/data-izin-detail/action" enctype="multipart/form-data">
            @csrf
            <input type="text" class="form-control @error('id_user') is-invalid @enderror" id="id_user" name="id_user" placeholder="" value="{{ $detailIzins->id_users }}" hidden>
            <input type="text" class="form-control @error('id') is-invalid @enderror" id="id" name="id" placeholder="" value="{{ $detailIzins->id }}" hidden>
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Peserta Magang</label>
                    <input type="email" class="form-control @error('nama_peserta_magang') is-invalid @enderror" id="nama_peserta_magang" name="nama_peserta_magang" placeholder="" value="{{$detailIzins->user->nama_lengkap}}" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tanggal Mulai Izin</label>
                    <input type="email" class="form-control @error('tanggal_izin') is-invalid @enderror" id="tanggal_izin" name="tanggal_izin" placeholder="" value="{{ date('d F y', strtotime($detailIzins->tanggal_izin)) }}" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tanggal Selesai Izin</label>
                    <input type="email" class="form-control @error('tanggal_selesai_izin') is-invalid @enderror" id="tanggal_selesai_izin" name="tanggal_selesai_izin" placeholder="" value="{{ date('d F y', strtotime($detailIzins->tanggal_selesai_izin)) }}" readonly>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Alasan Izin</label>
                    <textarea id="summernote" name="alasan_izin">
                        {{ $detailIzins->alasan_izin }}
                    </textarea>
                </div>
                @if(auth()->user()->privileges == "ADMIN")
                    <div class="form-group">
                        <label for="exampleInputEmail1">Alasan Approval</label>
                        <textarea id="keterangan_approval" name="keterangan_approval" class="@error('keterangan_approval') is-invalid @enderror">
                        {{ $detailIzins->keterangan_approval }}
                    </textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Status Izin</label>
                        <select class="form-control select2 @error('status') is-invalid @enderror" name="status" id="status" style="width: 100%;">
                            <option value="">Pilih</option>
                            <option value="Disetujui" @if($detailIzins->status == "Disetujui") selected @endif>Disetujui</option>
                            <option value="Ditolak" @if($detailIzins->status == "Ditolak") selected @endif>Ditolak</option>
                        </select>
                    </div>
                @endif

                <div class="form-group">
                    <label for="exampleInputFile">Bukti Izin</label>
                </div>
            </div>
            <div class="card-footer bg-white">
                <ul class="mailbox-attachments d-flex align-items-stretch clearfix">
                    <li>
                        <span class="mailbox-attachment-icon"><i class="far fa-file-pdf"></i></span>

                        <div class="mailbox-attachment-info">
                            <a href="#" class="mailbox-attachment-name"><i class="fas fa-paperclip"></i> {{ $detailIzins->fileuploads->filename }}</a>
                            <span class="mailbox-attachment-size clearfix mt-1">
                          <span>{{ $detailIzins->fileuploads->size }}</span>
                          <a href="/storage/{{ $detailIzins->fileuploads->path }}" target="_blank" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                        </div>
                    </li>


                </ul>
            </div>
            <div class="card-footer">
                @if(auth()->user()->privileges == "ADMIN")
                    <button type="submit" class="btn btn-success">PROSES</button>
                @endif

                <a href="/data-izin" class="btn btn-danger">KEMBALI</a>
            </div>
        </form>
    </div>




@endsection
@push('bottom-js')
    <script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- bs-custom-file-input -->
    <script src="{{asset('template')}}/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->

    <!-- Summernote -->
    <script src="{{asset('template')}}/plugins/summernote/summernote-bs4.min.js"></script>

    <script>
        $(function () {
            // Summernote
            $('#summernote').summernote()
            $('#keterangan_approval').summernote()

            // CodeMirror
            CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
                mode: "htmlmixed",
                theme: "monokai"
            });
        })
    </script>
@endpush

