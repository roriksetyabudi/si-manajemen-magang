<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Magangs;

class NilaiTrans extends Model
{
    use HasFactory;

    protected $table = 'nilai_trans';
    protected $guarded = ['id'];

    public function user(){
        return $this->belongsTo(User::class, 'id_users', 'id');
    }
    public function magangs(){
        return $this->belongsTo(Magangs::class, 'id_users', 'id_users_magang');
    }

}
