<?php

namespace App\Http\Controllers;

use App\Models\FileUploads;
use App\Models\Instansis;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Magangs;

class DataMagangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->privileges == "ADMIN") {
            $magangs = Magangs::where('deleted', 0)->whereNotIn('id_users_magang',['0'])->get();
        } else if(auth()->user()->privileges == "USERS MAGANG") {
            $magangs = Magangs::where('deleted', 0)->where('id_users_magang', auth()->user()->id)->get();
        } else if(auth()->user()->privileges == "PEMBIMBING SEKOLAH") {
            $magangs = Magangs::where('deleted', 0)->where('id_pembimbing_sekolah', auth()->user()->id)->whereNotIn('id_users_magang',['0'])->get();
        } else if(auth()->user()->privileges == "PEMBIMBING MAGANG") {
            $magangs = Magangs::where('deleted', 0)->where('id_users_pembimbing_magang', auth()->user()->id)->whereNotIn('id_users_magang',['0'])->get();
        }
        return view('data_magang.v_data_magang', [
            'magangs' => $magangs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data_magang.v_data_magang_add', [
            'pembimbingMagang' => User::where('deleted', 0)->where('privileges','PEMBIMBING MAGANG')->orderBy('nama_lengkap', "ASC")->get(),
            'instansis' => Instansis::where('deleted', 0)->get(),
            'pembimbingSekolah' => User::where('deleted', 0)->where('privileges','PEMBIMBING SEKOLAH')->orderBy('nama_lengkap', "ASC")->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->is_peserta_magang == "N" || $request->is_peserta_magang == "") {
            $validatedData = $request->validate([
                'nama_lengkap' => 'required|min:3',
                'telepon' => 'required|min:3',
                'alamat' => 'required',
                'gender' => 'required',
                'is_peserta_magang' => 'required',
                'privileges' => 'required',
                'username' => 'required|min:3|unique:users',
                'email' => 'required|min:3|unique:users|email',
                'password' => 'required|min:3',
                'photo' => 'required|image|file|max:1000',
            ]);
        } else {
            $validatedData = $request->validate([
                'nama_lengkap' => 'required|min:3',
                'telepon' => 'required|min:3',
                'alamat' => 'required',
                'gender' => 'required',
                'is_peserta_magang' => 'required',
                'privileges' => 'required',
                'username' => 'required|min:3|unique:users',
                'email' => 'required|min:3|unique:users|email',
                'password' => 'required|min:3',
                'photo' => 'required|image|file|max:1000',
                'tanggal_mulai_magang' => 'required',
                'tanggal_selesai_magang' => 'required',
                'jurusan_bidang_keahlihan' => 'required',
                'id_users_pembimbing_magang' => 'required',
                'id_instansi' => 'required',
                'id_pembimbing_sekolah' => 'required',
                'alamat_instansi' => 'required',
                'telepon_instansi' => 'required',
                'file_berkas' => 'required|mimes:pdf|max:2000',
            ]);
        }

        //FILE UPLOAD IMAGES PHOTO USER
        $insert['file_uploads']['title'] = "Photo Users ". $request->nama_lengkap;
        $insert['file_uploads']['filename'] = $request->file('photo')->hashName();
        $insert['file_uploads']['path'] = $request->file('photo')->store('file-uploads/images');
        $insert['file_uploads']['type'] = $request->file('photo')->extension();
        $insert['file_uploads']['size'] = $request->file('photo')->getSize();
        $queryInsertFileUploads = FileUploads::create($insert['file_uploads']);
        $resultIdFileUploadPhotos = $queryInsertFileUploads->id;

        //INSERT USERS
        $kode_verifikasi = random_int(100000, 999999);
        $insert['users']['username'] = $request->username;
        $insert['users']['password'] = bcrypt($request->password);
        $insert['users']['nama_lengkap'] = $request->nama_lengkap;
        $insert['users']['alamat'] = $request->alamat;
        $insert['users']['gender'] = $request->gender;
        $insert['users']['email'] = $request->email;
        $insert['users']['telepon'] = $request->telepon;
        $insert['users']['photo'] = $resultIdFileUploadPhotos;
        $insert['users']['privileges'] = $request->privileges;
        $insert['users']['is_peserta_magang'] = $request->is_peserta_magang;
        $insert['users']['pending'] = 1;
        $insert['users']['disabled'] = 1;
        $insert['users']['kode_verifikasi'] = $kode_verifikasi;
        $insert['users']['kode_verifikasi_at'] = date("Y-m-d H:i:s");
        $queryInsertUsers = User::create($insert['users']);
        $resultIdUsers = $queryInsertUsers->id;

        if($request->is_peserta_magang == "Y" && $request->privileges == "USERS MAGANG") {
            //FILE UPLOAD BERKAS MAGANG USER
            $insert['file_uploads_berkas']['title'] = "Berkas Magang Users ". $request->nama_lengkap;
            $insert['file_uploads_berkas']['filename'] = $request->file('file_berkas')->hashName();
            $insert['file_uploads_berkas']['path'] = $request->file('file_berkas')->store('file-uploads/documents');
            $insert['file_uploads_berkas']['type'] = $request->file('file_berkas')->extension();
            $insert['file_uploads_berkas']['size'] = $request->file('file_berkas')->getSize();
            $queryInsertFileUploadsBekas = FileUploads::create($insert['file_uploads_berkas']);
            $resultIdFileUploadPhotosBerkas = $queryInsertFileUploadsBekas->id;

            if($request->id_instansi == "lainya") {
                //SET ADD INSTANSI
                $insert['instansis']['nama_instansi'] = $request->nama_instansi;
                $insert['instansis']['alamat_instansi'] = $request->alamat_instansi;
                $insert['instansis']['telepon_instansi'] = $request->telepon_instansi;
                $queryInsertInstansis = Instansis::create($insert['instansis']);
                $resultIdInstansis = $queryInsertInstansis->id;
            }

            //INSERT DATA MAGANGS
            $insert['magangs']['kode_pendaftaran_magang'] = $kode_verifikasi;
            $insert['magangs']['id_users_magang'] = $resultIdUsers;
            if($request->id_instansi == "lainya") {
                $insert['magangs']['id_instansi'] = $resultIdInstansis;
            } else {
                $insert['magangs']['id_instansi'] = $request->id_instansi;
            }
            $insert['magangs']['id_users_pembimbing_magang'] = $request->id_users_pembimbing_magang;
            $insert['magangs']['photo'] = $resultIdFileUploadPhotos;
            $insert['magangs']['file_berkas'] = $resultIdFileUploadPhotosBerkas;
            $insert['magangs']['jurusan_bidang_keahlihan'] = $request->jurusan_bidang_keahlihan;
            $insert['magangs']['id_pembimbing_sekolah'] = $request->id_pembimbing_sekolah;
            $insert['magangs']['tanggal_mulai_magang'] = date('Y-m-d', strtotime($request->tanggal_mulai_magang));
            $insert['magangs']['tanggal_selesai_magang'] = date('Y-m-d', strtotime($request->tanggal_selesai_magang));
            $insert['magangs']['is_approval'] = "Y";
            $insert['magangs']['is_approval_at'] = date("Y-m-d H:i:s");
            $insert['magangs']['keterangan'] = "Insert By User : ".auth()->user()->nama_lengkap;
            $insert['magangs']['user_approval'] = auth()->user()->nama_lengkap;
            $queryInsertmagangs = Magangs::create($insert['magangs']);
        }
        return redirect('/data-magang')->with('success', 'Sukses Menambahkan User Baru, dan Data Magang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
