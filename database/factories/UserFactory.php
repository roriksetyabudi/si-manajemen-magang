<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return
            [
            'username' => "admin",
            'password' => bcrypt('123456'),
            'nama_lengkap' => 'Administrator',
            'alamat' => 'Jl. Raya Melati Punung',
            'gender' => 'L',
            'email' => 'roriksetya@gmail.com',
            'telepon' => '087751948827',
            'photo' => 0,
            'privileges' => 'ADMIN',
            'pending' => 1,
            'disabled' => 1,
            'kode_verifikasi' => 123456,
            'kode_verifikasi_at' => now(),
            'deleted' => 0
        ];

    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
//    public function unverified()
//    {
//        return $this->state(function (array $attributes) {
//            return [
//                'email_verified_at' => null,
//            ];
//        });
//    }
}
