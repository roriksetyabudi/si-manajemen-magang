@component('mail::message')
# Konfirmasi Pendaftaran Peserta Magang
{!! $pendaftaranMagangDetai->keterangan !!}
@if($pendaftaranMagangDetai->status == "DITERIMA")
<br>
Kode Verifikasi Anda : <b>{{ $pendaftaranMagangDetai->kode_registrasi }}</b>
@endif
@if($pendaftaranMagangDetai->status == "DITERIMA")
{{--    @component('mail::button', ['url' => '/registrasi-akun'])--}}
{{--    KUNJUNGI--}}
{{--    @endcomponent--}}
    Silahkan Login Menggunakan Username dan Password Yang Sudah Pernah Anda Buat Sebelumnya Ketika Melakukan Pendaftaran Magang
@endif
Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent
