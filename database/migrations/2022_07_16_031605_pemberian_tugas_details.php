<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PemberianTugasDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemberian_tugas_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('id_tugas')->nullable(false)->default(0)->comment('id  tugas');
            $table->integer('id_users_created')->nullable(false)->default(0)->comment('is users yang mengerjakan tugas');
            $table->string('nama_kegiatan', 100)->nullable(true)->comment('nama kegiaatan pengerjaan tugas');
            $table->text('keterangan')->nullable(true)->comment('keterangan pengerjaan tugas');
            $table->integer('id_file_tugas')->nullable(false)->default(0)->comment('lampiran pengerjaan tugas jik ada');
            $table->tinyInteger('target')->nullable(false)->default(0)->comment('target persentase pengerjaan tugas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemberian_tugas_detail');
    }
}
