@extends('template.v_template')
@section('title', 'Data Absensi Magang')
@push('head-css')
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/fonts.googleapis.com.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('template')}}/dist/css/adminlte.min.css">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('template')}}/plugins/summernote/summernote-bs4.min.css">

    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endpush
@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Tambah Pengajuan Izin Baru</h3>
        </div>
        <form method="post" action="/data-izin/post" enctype="multipart/form-data">
            @csrf
            <input type="text" class="form-control" id="id_user" name="id_user" placeholder="" value="{{ $detailUsers->id }}" hidden>
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Peserta Magang</label>
                    <input type="text" class="form-control @error('nama_peserta_magang') is-invalid @enderror" id="nama_peserta_magang" name="nama_peserta_magang" placeholder="" value="{{ $detailUsers->nama_lengkap }}" readonly>
                    @error('nama_peserta_magang')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tanggal Izin</label>
                     <input type="text" class="form-control @error('tanggal_izin') is-invalid @enderror" id="tanggal_izin" name="tanggal_izin" placeholder="" value="{{ date('m/d/Y', strtotime(date('Y-m-d'))) }}" readonly>
                    @error('tanggal_izin')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tanggal Selesai Izin</label>
                    <input type="text" class="form-control @error('tanggal_selesai_izin') is-invalid @enderror" id="tanggal_selesai_izin" name="tanggal_selesai_izin" placeholder="" value="{{ date('m/d/Y', strtotime(date('Y-m-d'))) }}" readonly>
                    @error('tanggal_selesai_izin')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Alasan Izin</label>
                    <textarea id="summernote" name="alasan_izin" class="@error('alasan_izin') is-invalid @enderror">
                        Silahkan Memasukan Alasan Izin Anda
              </textarea>
                    @error('alasan_izin')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">Bukti Izin</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input @error('file_bukti_izin') is-invalid @enderror" id="file_bukti_izin" name="file_bukti_izin">
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Upload</span>
                        </div>
                        @error('file_bukti_izin')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Proses</button>
            </div>
        </form>
    </div>




@endsection
@push('bottom-js')
    <script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- bs-custom-file-input -->
    <script src="{{asset('template')}}/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('template')}}/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->

    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

    <!-- Summernote -->
    <script src="{{asset('template')}}/plugins/summernote/summernote-bs4.min.js"></script>

    <script>
        $(function () {
            // Summernote
            $('#tanggal_izin').datepicker({
                uiLibrary: 'bootstrap4'
            });
            $('#tanggal_selesai_izin').datepicker({
                uiLibrary: 'bootstrap4'
            });

            $('#summernote').summernote()

            // CodeMirror
            CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
                mode: "htmlmixed",
                theme: "monokai"
            });


        })
    </script>
@endpush

