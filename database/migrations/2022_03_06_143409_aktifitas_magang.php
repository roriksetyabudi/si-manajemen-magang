<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AktifitasMagang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aktifitas_magangs', function (Blueprint $table) {
            $table->id();
            $table->smallInteger('id_users')->index();
            $table->string("nama_kegiatan", 100);
            $table->dateTime("tanggal_mulai");
            $table->dateTime("tanggal_selesai");
            $table->string("interval_aktifitas")->nullable(true);
            $table->string("volume_kegiatan", 100);
            $table->text("keterangan_kegiatan")->nullable(true);
            $table->tinyInteger("deleted")->default(0)->comment("0=Not Deleted, 1=deleted");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aktifitas_magangs');
    }
}
