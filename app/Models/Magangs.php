<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Magangs extends Model
{
    use HasFactory;

    protected $table = 'magangs';
    protected $guarded = ['id'];

    public function user(){
        return $this->belongsTo(User::class, 'id_users_magang', 'id');
    }
    public function userMagang(){
        return $this->belongsTo(User::class, 'id_users_magang', 'id');
    }
    public function instansis(){
        return $this->belongsTo(Instansis::class, 'id_instansi', 'id');
    }
    public function usersPembimbingmagang(){
        return $this->belongsTo(User::class, 'id_users_pembimbing_magang', 'id');
    }
    public function usersPembimbingSekolah(){
        return $this->belongsTo(User::class, 'id_pembimbing_sekolah', 'id');
    }
    public function fileUploadsFoto(){
        return $this->belongsTo(FileUploads::class, 'photo', 'id');
    }
    public function fileUploadsBerkas(){
        return $this->belongsTo(FileUploads::class, 'file_berkas', 'id');
    }
    public function fileUploadsBerkasTugasAkhirMagang(){
        return $this->belongsTo(FileUploads::class, 'file_berkas_tugas_akhir_magang', 'id');
    }
}
